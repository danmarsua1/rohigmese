<%--
 * edit.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<script>

$(document).ready(function(){
	var errors = $("[class='error']");
	
	if(errors.length>0){
		$("input[class='form-control']").addClass("is-valid");
		
		for (var i = 0; i < errors.length; i++) {
			var e = errors[i].id.split(".");
		  	e = e[0];
		  	$("input[id="+e+"]").removeClass("is-valid").addClass("is-invalid");
		  	
		}
	
	}
	 
	
});

</script>

<!-- ALERTS -->
<jstl:if test="${errorsAlert!=null}">
	<div id="alert" class="row justify-content-center">
		<div class="col-12">
			<div class="alert alert-danger text-center" role="alert">
				${errorsAlert}
			</div>
		</div>
	</div>
</jstl:if>

<div class="container justify-content-center py-1">
	
	
	<form:form class="needs-validation" action="user/edit.do" modelAttribute="user" novalidate="novalidate">
	
		<form:hidden path="id" />
		<form:hidden path="version" />
		<form:hidden path="userAccount" />
	
		<fieldset>
		
		<legend>
				<spring:message code="user.personal" />
		</legend>
			
		  <div class="form-row">
		    <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6 mb-3">
		      <form:label path="name" for="name">
		      	* <spring:message code="user.name" />
		      </form:label>
		      <form:input path="name" type="text" class="form-control" id="name" />
		      <div class="invalid-feedback">
        		<form:errors class="error" path="name" />
      		  </div>
		    </div>
		    <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6 mb-3">
		      <form:label path="surname" for="surname">
		      	* <spring:message code="user.surname" />
		      </form:label>
		      <form:input path="surname" type="text" class="form-control" id="surname" />
		      <div class="invalid-feedback">
        		<form:errors class="error" path="surname" />
      		  </div>
		    </div>
		  </div>
		  
		  <div class="form-row">
		    <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-4 mb-4">
		      <form:label path="email" for="email">
		      	* <spring:message code="user.email" />
		      </form:label>
		      <form:input path="email" type="text" class="form-control" id="email" />
		      <div class="invalid-feedback">
        		<form:errors class="error" path="email" />
      		  </div>
		    </div>
		  </div>
		  
	  </fieldset>

	  <div class="form-row">
	  	<div class="col-8 col-sm-6 col-md-4 col-lg-6 col-xl-6 mb-3">
		  	<input class="btn bg-dark text-white" type="submit" name="save"
				value="<spring:message code="user.save" />" />
		
			<input class="btn bg-dark text-white" type="button" name="cancel"
				value="<spring:message code="user.cancel" />"
				onclick="javascript: relativeRedir('welcome/index.do');" />
		</div>
	  </div>
	  
	  <hr class="hr-style">
	  
	  <div class="form-row mb-3 pb-1 text-center justify-content-center">
					<button style="background-color: #cc0000" class="btn text-white"
						name="delete"
						onclick="javascript: relativeRedir('user/edit.do');">
						<i class="fa fa-times"></i>&nbsp;
						<spring:message code="user.delete" />
					</button>
				</div>
	</form:form>

</div>
