<%--
 * list.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<div class="container justify-content-center py-1">

	
	<jstl:choose>
		<jstl:when test="${fn:length(actors)>0}">
			<table class="table">
				<thead>
					<tr>
						<th scope="col">#</th>
						<th scope="col"><spring:message code="user.type" /></th>
						<th scope="col"><spring:message code="user.name" /></th>
						<th scope="col"><spring:message code="user.surname" /></th>
						<th scope="col"><spring:message code="user.email" /></th>
						<th scope="col"><spring:message code="user.entity" /></th>
						<th scope="col"><spring:message code="user.phone" /></th>
					</tr>
				</thead>
				<tbody>
					<jstl:set var="cont" value="0" />
					<jstl:forEach var="usr" items="${actors}">
					<jstl:set var="cont" value="${cont + 1}" />
					<jstl:choose>
						<jstl:when test="${usr['class'].name=='domain.Researcher'}">
							<tr style="color:#663300;font-weight:bold">
								<th style="color:#212529" scope="row">${cont}</th>
								<td><b><jstl:out value="${usr.userAccount.authorities[0].authority}" /></b></td>
								<td><jstl:out value="${usr.name}" /></td>
								<td><jstl:out value="${usr.surname}" /></td>
								<td><jstl:out value="${usr.email}" /></td>
								<td><jstl:out value="${usr.entity}" /></td> 
								<td><jstl:out value="${usr.phone}" /></td>	
							</tr>
						</jstl:when>
						<jstl:otherwise>
							<tr>
								<th scope="row">${cont}</th>
								<td><b><jstl:out value="${usr.userAccount.authorities[0].authority}" /></b></td>
								<td><jstl:out value="${usr.name}" /></td>
								<td><jstl:out value="${usr.surname}" /></td>
								<td><jstl:out value="${usr.email}" /></td>
								<td><jstl:out value="-" /></td> 
								<td><jstl:out value="-" /></td> 
							</tr>
						</jstl:otherwise>
					</jstl:choose>
					</jstl:forEach>
				</tbody>
			</table>
		</jstl:when>
		<jstl:otherwise>
			<br/>
			<div class="form-row justify-content-center">
				<p class="text-reset text-center"><spring:message code="user.empty" /></p>
			</div>
		</jstl:otherwise>
	</jstl:choose>

</div>




