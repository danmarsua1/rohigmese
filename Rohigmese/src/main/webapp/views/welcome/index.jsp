<%--
 * index.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<jstl:if test="${invitationSuccess!=''}">
	<div id="alert-success" class="row justify-content-center">
		<div class="col-12">
			<div class="alert alert-success text-center" role="alert">
				${invitationSuccess}</div>
		</div>
	</div>
</jstl:if>

<div class="container-fluid">

	<div class="row">
		<div class="welcome-user col-12 pt-3">
			<p class="lead">
				<b><spring:message code="welcome.greeting.prefix" />
					${name.name} ${name.surname}.</b>
			</p>
		</div>
	</div>

	<div class="row">
		<div class="welcome-user col-12">
			<p class="text-reset text-justify">
				<spring:message code="welcome.introduction" />
			</p>
		</div>
	</div>

	<hr class="hr-style">

	<div id="carouselImages" class="carousel carousel-dark slide mb-3"
		data-bs-ride="carousel">
		<ol class="carousel-indicators">
		</ol>
		<div class="carousel-inner">
			<div class="carousel-item active">
				<img src="images/lisimetro2.jpg" class="d-block w-100" alt="...">
				<div class="carousel-caption">
					<h5>Meteo-Lis�metro</h5>
					<p><spring:message code="welcome.carrousel.lisimetro" /></p>
				</div>
			</div>
			<div class="carousel-item">
				<img src="images/palacio2.jpg" class="d-block w-100" alt="...">
				<div class="carousel-caption">
					<h5>Marismas de Do�ana</h5>
					<p><spring:message code="welcome.carrousel.marismillas" /></p>
				</div>
			</div>
			<div class="carousel-item">
				<img src="images/laguna.jpg" class="d-block w-100" alt="...">
				<div class="carousel-caption">
					<h5>Laguna Santa Olalla</h5>
					<p><spring:message code="welcome.carrousel.laguna" /></p>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		$(document).ready(function() {
			
			var myCarousel = document.querySelector('#carouselImages');
			
			var carousel = new bootstrap.Carousel(myCarousel, {
				interval : 6500,
				wrap : true
			});

		});
	</script>

</div>
