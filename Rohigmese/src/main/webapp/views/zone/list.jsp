<%--
 * list.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<script>
	
	function redirect(zoneId){
		var str = "station/zone/list.do?zoneId="+zoneId;
		relativeRedir(str)
	}

</script>

<div class="container justify-content-center py-1">
		
	<jstl:choose>
		<jstl:when test="${fn:length(zones)>0}">
			<table class="table">
				<thead>
					<tr>
						<th scope="col">#</th>
						<th scope="col"><spring:message code="zone.data1" /></th>
						<th scope="col"><spring:message code="zone.name" /></th>
						<th scope="col"><spring:message code="zone.type" /></th>
						<th scope="col"><spring:message code="zone.localization" /></th>
						<th scope="col"><spring:message code="zone.comments" /></th>
					</tr>
				</thead>
				<tbody>
					<jstl:set var="cont" value="0" />
					<jstl:forEach var="zone" items="${zones}">
					<jstl:set var="cont" value="${cont + 1}" />
						<tr>
							<th scope="row">${cont}</th>
							
							<td><i onclick='redirect(<jstl:out value='${zone.id}' />);' style='color:#007bff;cursor:pointer' class='fa fa-search fa-lg'><span style='color:#007bff;font: 14px sans-serif;font-weight:bolder'>&nbsp;&nbsp;<spring:message code="zone.viewData" /></span></i></td>
							<td><jstl:out value="${zone.name}" /></td>
							<td><jstl:out value="${zone.type}" /></td>
							<td><jstl:out value="${zone.localization.latitude}, ${zone.localization.longitude}" /></td>
							<jstl:choose>
								<jstl:when test="${zone.comments=='' || zone.comments==null}">
									<td><spring:message code="zone.noComments" /></td> 
								</jstl:when>
								<jstl:otherwise>
									<td><jstl:out value="${zone.comments}" /></td> 
								</jstl:otherwise>
							</jstl:choose>
						</tr>
					</jstl:forEach>
				</tbody>
			</table>
		</jstl:when>
		<jstl:otherwise>
			<br/>
			<div class="form-row justify-content-center">
				<p class="text-reset text-center"><spring:message code="zone.empty" /></p>
			</div>
		</jstl:otherwise>
	</jstl:choose>

</div>




