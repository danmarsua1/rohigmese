<%--
 * new_researcher.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<script>
	$(document).ready(function() {

		$("#chartLoading").hide();
		$("#chartImg").show();

		$("button[name='graph']").click(function() {
			$("#chartButton").hide();
			$("#chartImg").hide();
			$("#chartLoading").show();
		});

	});
</script>

<!-- ALERTS -->
<jstl:if test="${fileSuccess!=null && fileSuccess!=''}">
	<div id="alert-success" class="row justify-content-center">
		<div class="col-12">
			<div class="alert alert-success text-center" role="alert">
				${fileSuccess}</div>
		</div>
	</div>
</jstl:if>

<jstl:if test="${fileError!=null && fileError!=''}">
	<div id="alert" class="row justify-content-center">
		<div class="col-12">
			<div class="alert alert-danger text-center" role="alert">
				${fileError}</div>
		</div>
	</div>
</jstl:if>

<jstl:if test="${deleteSuccess!=null && deleteSuccess!=''}">
	<div id="alert-success" class="row justify-content-center">
		<div class="col-12">
			<div class="alert alert-success text-center" role="alert">
				${deleteSuccess}</div>
		</div>
	</div>
</jstl:if>

<jstl:if test="${deleteError!=null && deleteError!=''}">
	<div id="alert" class="row justify-content-center">
		<div class="col-12">
			<div class="alert alert-danger text-center" role="alert">
				${deleteError}</div>
		</div>
	</div>
</jstl:if>

<!-- CODE -->
<div class="container-fluid justify-content-center py-1">

	<br />
	<div class="form-row justify-content-center">
		<p class="text-reset text-center">
			<spring:message code="device.dataDevice" />
			<jstl:choose>
				<jstl:when
					test="${device.brand!=null and !fn:contains(device.name,'Sensores')}">
					<b><jstl:out value=" ${device.name} [${device.brand}]"></jstl:out></b>
				</jstl:when>
				<jstl:otherwise>
					<b><jstl:out value=" ${device.name}"></jstl:out></b>
				</jstl:otherwise>
			</jstl:choose>

		</p>
	</div>

	<hr class="hr-style">

	<security:authorize access="hasRole('RESEARCHER')">
		<div class="form-row pt-3 text-center">
			<div class="col-12">
				<button class="btn bg-dark text-white" name="newData"
					onclick="javascript: relativeRedir('device/researcher/selectDevice.do?stationId=${station.id}');">
					<i class="fa fa-plus"></i>&nbsp;
					<spring:message code="device.newData" />
				</button>
			</div>
		</div>
	</security:authorize>


	<fieldset id="fieldsetParameters" class="mt-2 mb-4">
		<legend id="legendStatus">
			<spring:message code="device.parametersLegend" />
		</legend>

		<div id="accordion">
			<div class="card">
				<div class="card-header" id="headingOne">
					<h5 class="mb-0">
						<button class="btn btn-link" data-toggle="collapse"
							data-target="#collapseOne" aria-expanded="true"
							aria-controls="collapseOne">
							<spring:message code="device.legend.lisimeter" />
						</button>
					</h5>
				</div>

				<div id="collapseOne" class="collapse" aria-labelledby="headingOne"
					data-parent="#accordion">
					<table style="border: 0; margin: 0 0 10px 0 !important;"
						class="table table-borderless col-12">
						<tbody>
							<tr>
								<td><span class="align-middle"><b>Pot
											weight/Peso Olla -</b> Cantidad de agua drenada (Kg)</span></td>
							</tr>
							<tr>
								<td><span class="align-middle"><b>Lisimeter
											weight/Peso Lis�metro -</b> Cantidad de agua recogida (Kg)</span></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div class="card">
				<div class="card-header" id="headingTwo">
					<h5 class="mb-0">
						<button class="btn btn-link collapsed" data-toggle="collapse"
							data-target="#collapseTwo" aria-expanded="false"
							aria-controls="collapseTwo">
							<spring:message code="device.legend.meteolisimeter" />
						</button>
					</h5>
				</div>
				<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo"
					data-parent="#accordion">
					<table style="border: 0; margin: 0 0 10px 0 !important;"
						class="table table-borderless col-12">
						<tbody>
							<tr>
								<td><span class="align-middle"><b>Tens.
											Lisimeter/Tens. Lis�metro -</b> Tensiometer Lisimeter/Tensi�metro
										del lis�metro (hPa)</span></td>
							</tr>
							<tr>
								<td><span class="align-middle"><b>Tens.
											Field/Tens. Campo -</b> Tensiometer field/Tensi�metro de campo
										(hPa)</span></td>
							</tr>
							<tr>
								<td><span class="align-middle"><b>T.+/-XXX -</b>
										Temperature measured at X meters/Temperatura medida a X metros
										(C�)</span></td>
							</tr>
							<tr>
								<td><span class="align-middle"><b>Rain -</b>
										Cantidad de lluvia (mm)</span></td>
							</tr>
							<tr>
								<td><span class="align-middle"><b>Wind Speed -</b>
										Velocidad del viento (m/s)</span></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div class="card">
				<div class="card-header" id="headingThree">
					<h5 class="mb-0">
						<button class="btn btn-link collapsed" data-toggle="collapse"
							data-target="#collapseThree" aria-expanded="false"
							aria-controls="collapseThree">
							<spring:message code="device.legend.campbelldecagon" />
						</button>
					</h5>
				</div>
				<div id="collapseThree" class="collapse"
					aria-labelledby="headingThree" data-parent="#accordion">
					<table style="border: 0; margin: 0 0 10px 0 !important;"
						class="table table-borderless col-12">
						<tbody>
							<tr>
								<td><span class="align-middle"><b>VWC -</b> Volume
										Water Content/Humedad/Contenido de agua en volumen (m<sup>3</sup>/m<sup>3</sup>)</span></td>
							</tr>
							<tr>
								<td><span class="align-middle"><b>EC -</b>
										Electroconductivity/Conductividad el�ctrica (dS*m<sup>-1</sup>)</span></td>
							</tr>
							<tr>
								<td><span class="align-middle"><b>TEMP -</b>
										Temperature/Temperatura (C�)</span></td>
							</tr>
							<tr>
								<td><span class="align-middle"><b>P1,P2,...,P6 -</b>
										Port/Puerto (Different depths/Diferentes profundidades)</span></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div class="card">
				<div class="card-header" id="headingFour">
					<h5 class="mb-0">
						<button class="btn btn-link collapsed" data-toggle="collapse"
							data-target="#collapseFour" aria-expanded="false"
							aria-controls="collapseFour">
							<spring:message code="device.legend.davis" />
						</button>
					</h5>
				</div>
				<div id="collapseFour" class="collapse"
					aria-labelledby="headingFour" data-parent="#accordion">
					<table style="border: 0; margin: 0 0 10px 0 !important;"
						class="table table-borderless col-12">
						<tbody>
							<tr>
								<td><span class="align-middle"><b>Temperature -</b>
										Temperatura (C�)</span></td>
							</tr>
							<tr>
								<td><span class="align-middle"><b>Humidity -</b>
										Humedad (%)</span></td>
							</tr>
							<tr>
								<td><span class="align-middle"><b>Wind Speed -</b>
										Velocidad del viento (m/s)</span></td>
							</tr>
							<tr>
								<td><span class="align-middle"><b>Bar. Pressure
											-</b> Presi�n barom�trica (mb)</span></td>
							</tr>
							<tr>
								<td><span class="align-middle"><b>Rain -</b>
										Cantidad de lluvia (mm)</span></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>

	</fieldset>

	<jstl:choose>
		<jstl:when test="${fn:length(deviceData)>0}">
			<!-- METEO LISIMETRO -->
			<jstl:if test="${fn:contains(requestURI,'deviceId=59')}">
				<display:table name="deviceData" id="row" requestURI="${requestURI}"
					pagesize="10" class="col-12 justify-content-center displaytag">
					<display:column property="moment"
						titleKey="device.meteolisimeter.moment" />
					<display:column property="tLisi"
						titleKey="device.meteolisimeter.tlisi" />
					<display:column property="tField"
						titleKey="device.meteolisimeter.tfield" />
					<display:column property="temp_m140"
						titleKey="device.meteolisimeter.tempm140" />
					<display:column property="temp_m020"
						titleKey="device.meteolisimeter.tempm020" />
					<display:column property="temp_m010"
						titleKey="device.meteolisimeter.tempm010" />
					<display:column property="temp_m005"
						titleKey="device.meteolisimeter.tempm005" />
					<display:column property="temp_005"
						titleKey="device.meteolisimeter.temp005" />
					<display:column property="temp_050"
						titleKey="device.meteolisimeter.temp050" />
					<display:column property="temp_200"
						titleKey="device.meteolisimeter.temp200" />
					<display:column property="rain"
						titleKey="device.meteolisimeter.rain" />
					<display:column property="windSpeed"
						titleKey="device.meteolisimeter.windspeed" />
				</display:table>
			</jstl:if>

			<!-- PESOS LISIMETRO -->
			<jstl:if test="${fn:contains(requestURI,'deviceId=60')}">
				<display:table name="deviceData" id="row" requestURI="${requestURI}"
					pagesize="10" class="col-12 justify-content-center displaytag">
					<display:column property="moment"
						titleKey="device.weightslisimeter.moment" />
					<display:column property="wPot"
						titleKey="device.weightslisimeter.wpot" />
					<display:column property="wLisi"
						titleKey="device.weightslisimeter.wlisi" />
				</display:table>
			</jstl:if>

			<!-- CAMPBELL LISIMETRO -->
			<jstl:if test="${fn:contains(requestURI,'deviceId=61')}">
				<display:table name="deviceData" id="row" requestURI="${requestURI}"
					pagesize="10" class="col-12 justify-content-center displaytag">
					<display:column property="moment"
						titleKey="device.campbelllisimeter.moment" />
					<display:column property="vwc_p1"
						titleKey="device.campbelllisimeter.vwcp1" />
					<display:column property="ec_p1"
						titleKey="device.campbelllisimeter.ecp1" />
					<display:column property="temp_p1"
						titleKey="device.campbelllisimeter.tempp1" />
					<display:column property="vwc_p2"
						titleKey="device.campbelllisimeter.vwcp2" />
					<display:column property="ec_p2"
						titleKey="device.campbelllisimeter.ecp2" />
					<display:column property="temp_p2"
						titleKey="device.campbelllisimeter.tempp2" />
					<display:column property="vwc_p3"
						titleKey="device.campbelllisimeter.vwcp3" />
					<display:column property="ec_p3"
						titleKey="device.campbelllisimeter.ecp3" />
					<display:column property="temp_p3"
						titleKey="device.campbelllisimeter.tempp3" />
					<display:column property="vwc_p4"
						titleKey="device.campbelllisimeter.vwcp4" />
					<display:column property="ec_p4"
						titleKey="device.campbelllisimeter.ecp4" />
					<display:column property="temp_p4"
						titleKey="device.campbelllisimeter.tempp4" />
					<display:column property="vwc_p5"
						titleKey="device.campbelllisimeter.vwcp5" />
					<display:column property="ec_p5"
						titleKey="device.campbelllisimeter.ecp5" />
					<display:column property="temp_p5"
						titleKey="device.campbelllisimeter.tempp5" />
					<display:column property="vwc_p6"
						titleKey="device.campbelllisimeter.vwcp6" />
					<display:column property="ec_p6"
						titleKey="device.campbelllisimeter.ecp6" />
					<display:column property="temp_p6"
						titleKey="device.campbelllisimeter.tempp6" />
				</display:table>
			</jstl:if>

			<!-- DAVIS LISIMETRO -->
			<jstl:if test="${fn:contains(requestURI,'deviceId=62')}">
				<display:table name="deviceData" id="row" requestURI="${requestURI}"
					pagesize="10" class="col-12 justify-content-center displaytag">
					<display:column property="moment"
						titleKey="device.davislisimeter.moment" />
					<display:column property="temp"
						titleKey="device.davislisimeter.temp" />
					<display:column property="humidity"
						titleKey="device.davislisimeter.humidity" />
					<display:column property="windSpeed"
						titleKey="device.davislisimeter.windspeed" />
					<display:column property="bar" titleKey="device.davislisimeter.bar" />
					<display:column property="rain"
						titleKey="device.davislisimeter.rain" />
				</display:table>
			</jstl:if>

			<!-- HELMAN LISIMETRO -->
			<jstl:if test="${fn:contains(requestURI,'deviceId=63')}">
				<display:table name="deviceData" id="row" requestURI="${requestURI}"
					pagesize="10" class="col-12 justify-content-center displaytag">
					<display:column property="moment"
						titleKey="device.helmanlisimeter.moment" />
					<display:column property="rain"
						titleKey="device.helmanlisimeter.rain" />
				</display:table>
			</jstl:if>

			<!-- CAMPBELL C7 DT1 -->
			<jstl:if test="${fn:contains(requestURI,'deviceId=64')}">
				<display:table name="deviceData" id="row" requestURI="${requestURI}"
					pagesize="10" class="col-12 justify-content-center displaytag">
					<display:column property="moment"
						titleKey="device.campbellc7dt1.moment" />
					<display:column property="vwc_p1"
						titleKey="device.campbellc7dt1.vwcp1" />
					<display:column property="ec_p1"
						titleKey="device.campbellc7dt1.ecp1" />
					<display:column property="temp_p1"
						titleKey="device.campbellc7dt1.tempp1" />
					<display:column property="vwc_p2"
						titleKey="device.campbellc7dt1.vwcp2" />
					<display:column property="ec_p2"
						titleKey="device.campbellc7dt1.ecp2" />
					<display:column property="temp_p2"
						titleKey="device.campbellc7dt1.tempp2" />
					<display:column property="vwc_p3"
						titleKey="device.campbellc7dt1.vwcp3" />
					<display:column property="ec_p3"
						titleKey="device.campbellc7dt1.ecp3" />
					<display:column property="temp_p3"
						titleKey="device.campbellc7dt1.tempp3" />
					<display:column property="vwc_p4"
						titleKey="device.campbellc7dt1.vwcp4" />
					<display:column property="ec_p4"
						titleKey="device.campbellc7dt1.ecp4" />
					<display:column property="temp_p4"
						titleKey="device.campbellc7dt1.tempp4" />
					<display:column property="vwc_p5"
						titleKey="device.campbellc7dt1.vwcp5" />
					<display:column property="ec_p5"
						titleKey="device.campbellc7dt1.ecp5" />
					<display:column property="temp_p5"
						titleKey="device.campbellc7dt1.tempp5" />
					<display:column property="vwc_p6"
						titleKey="device.campbellc7dt1.vwcp6" />
					<display:column property="ec_p6"
						titleKey="device.campbellc7dt1.ecp6" />
					<display:column property="temp_p6"
						titleKey="device.campbellc7dt1.tempp6" />
				</display:table>
			</jstl:if>

			<!-- CAMPBELL C7 DT2 -->
			<jstl:if test="${fn:contains(requestURI,'deviceId=65')}">
				<display:table name="deviceData" id="row" requestURI="${requestURI}"
					pagesize="10" class="col-12 justify-content-center displaytag">
					<display:column property="moment"
						titleKey="device.campbellc7dt2.moment" />
					<display:column property="vwc_p1"
						titleKey="device.campbellc7dt2.vwcp1" />
					<display:column property="ec_p1"
						titleKey="device.campbellc7dt2.ecp1" />
					<display:column property="temp_p1"
						titleKey="device.campbellc7dt2.tempp1" />
					<display:column property="vwc_p2"
						titleKey="device.campbellc7dt2.vwcp2" />
					<display:column property="ec_p2"
						titleKey="device.campbellc7dt2.ecp2" />
					<display:column property="temp_p2"
						titleKey="device.campbellc7dt2.tempp2" />
					<display:column property="vwc_p3"
						titleKey="device.campbellc7dt2.vwcp3" />
					<display:column property="ec_p3"
						titleKey="device.campbellc7dt2.ecp3" />
					<display:column property="temp_p3"
						titleKey="device.campbellc7dt2.tempp3" />
					<display:column property="vwc_p4"
						titleKey="device.campbellc7dt2.vwcp4" />
					<display:column property="ec_p4"
						titleKey="device.campbellc7dt2.ecp4" />
					<display:column property="temp_p4"
						titleKey="device.campbellc7dt2.tempp4" />
					<display:column property="vwc_p5"
						titleKey="device.campbellc7dt2.vwcp5" />
					<display:column property="ec_p5"
						titleKey="device.campbellc7dt2.ecp5" />
					<display:column property="temp_p5"
						titleKey="device.campbellc7dt2.tempp5" />
					<display:column property="vwc_p6"
						titleKey="device.campbellc7dt2.vwcp6" />
					<display:column property="ec_p6"
						titleKey="device.campbellc7dt2.ecp6" />
					<display:column property="temp_p6"
						titleKey="device.campbellc7dt2.tempp6" />
				</display:table>
			</jstl:if>

			<!-- DAVIS C7 -->
			<jstl:if test="${fn:contains(requestURI,'deviceId=66')}">
				<display:table name="deviceData" id="row" requestURI="${requestURI}"
					pagesize="10" class="col-12 justify-content-center displaytag">
					<display:column property="moment" titleKey="device.davisc7.moment" />
					<display:column property="temp" titleKey="device.davisc7.temp" />
					<display:column property="humidity"
						titleKey="device.davisc7.humidity" />
					<display:column property="windSpeed"
						titleKey="device.davisc7.windspeed" />
					<display:column property="bar" titleKey="device.davisc7.bar" />
					<display:column property="rain" titleKey="device.davisc7.rain" />
				</display:table>
			</jstl:if>

			<!-- DAVIS LLANOS -->
			<jstl:if test="${fn:contains(requestURI,'deviceId=67')}">
				<display:table name="deviceData" id="row" requestURI="${requestURI}"
					pagesize="10" class="col-12 justify-content-center displaytag">
					<display:column property="moment"
						titleKey="device.davisllanos.moment" />
					<display:column property="temp" titleKey="device.davisllanos.temp" />
					<display:column property="humidity"
						titleKey="device.davisllanos.humidity" />
					<display:column property="windSpeed"
						titleKey="device.davisllanos.windspeed" />
					<display:column property="bar" titleKey="device.davisllanos.bar" />
					<display:column property="rain" titleKey="device.davisllanos.rain" />
				</display:table>
			</jstl:if>

			<!-- DECAGON LLANOS -->
			<jstl:if test="${fn:contains(requestURI,'deviceId=68')}">
				<display:table name="deviceData" id="row" requestURI="${requestURI}"
					pagesize="10" class="col-12 justify-content-center displaytag">
					<display:column property="moment"
						titleKey="device.decagonllanos.moment" />
					<display:column property="vwc_p1"
						titleKey="device.decagonllanos.vwcp1" />
					<display:column property="ec_p1"
						titleKey="device.decagonllanos.ecp1" />
					<display:column property="temp_p1"
						titleKey="device.decagonllanos.tempp1" />
					<display:column property="vwc_p2"
						titleKey="device.decagonllanos.vwcp2" />
					<display:column property="ec_p2"
						titleKey="device.decagonllanos.ecp2" />
					<display:column property="temp_p2"
						titleKey="device.decagonllanos.tempp2" />
					<display:column property="vwc_p3"
						titleKey="device.decagonllanos.vwcp3" />
					<display:column property="ec_p3"
						titleKey="device.decagonllanos.ecp3" />
					<display:column property="temp_p3"
						titleKey="device.decagonllanos.tempp3" />
				</display:table>
			</jstl:if>

			<!-- DECAGON LAGUNA -->
			<jstl:if test="${fn:contains(requestURI,'deviceId=69')}">
				<display:table name="deviceData" id="row" requestURI="${requestURI}"
					pagesize="10" class="col-12 justify-content-center displaytag">
					<display:column property="moment"
						titleKey="device.decagonlaguna.moment" />
					<display:column property="vwc_p1"
						titleKey="device.decagonlaguna.vwcp1" />
					<display:column property="ec_p1"
						titleKey="device.decagonlaguna.ecp1" />
					<display:column property="temp_p1"
						titleKey="device.decagonlaguna.tempp1" />
					<display:column property="vwc_p2"
						titleKey="device.decagonlaguna.vwcp2" />
					<display:column property="ec_p2"
						titleKey="device.decagonlaguna.ecp2" />
					<display:column property="temp_p2"
						titleKey="device.decagonlaguna.tempp2" />
				</display:table>
			</jstl:if>

			<!-- DECAGON PALACIO -->
			<jstl:if test="${fn:contains(requestURI,'deviceId=70')}">
				<display:table name="deviceData" id="row" requestURI="${requestURI}"
					pagesize="10" class="col-12 justify-content-center displaytag">
					<display:column property="moment"
						titleKey="device.decagonpalacio.moment" />
					<display:column property="vwc_p1"
						titleKey="device.decagonpalacio.vwcp1" />
					<display:column property="ec_p1"
						titleKey="device.decagonpalacio.ecp1" />
					<display:column property="temp_p1"
						titleKey="device.decagonpalacio.tempp1" />
					<display:column property="vwc_p2"
						titleKey="device.decagonpalacio.vwcp2" />
					<display:column property="ec_p2"
						titleKey="device.decagonpalacio.ecp2" />
					<display:column property="temp_p2"
						titleKey="device.decagonpalacio.tempp2" />
					<display:column property="vwc_p3"
						titleKey="device.decagonpalacio.vwcp3" />
					<display:column property="ec_p3"
						titleKey="device.decagonpalacio.ecp3" />
					<display:column property="temp_p3"
						titleKey="device.decagonpalacio.tempp3" />
					<display:column property="vwc_p4"
						titleKey="device.decagonpalacio.vwcp4" />
					<display:column property="ec_p4"
						titleKey="device.decagonpalacio.ecp4" />
					<display:column property="temp_p4"
						titleKey="device.decagonpalacio.tempp4" />
				</display:table>
			</jstl:if>

			<!-- DAVIS SO-IGME -->
			<jstl:if test="${fn:contains(requestURI,'deviceId=71')}">
				<display:table name="deviceData" id="row" requestURI="${requestURI}"
					pagesize="10" class="col-12 justify-content-center displaytag">
					<display:column property="moment" titleKey="device.davisso.moment" />
					<display:column property="temp" titleKey="device.davisso.temp" />
					<display:column property="humidity"
						titleKey="device.davisso.humidity" />
					<display:column property="windSpeed"
						titleKey="device.davisso.windspeed" />
					<display:column property="bar" titleKey="device.davisso.bar" />
					<display:column property="rain" titleKey="device.davisso.rain" />
				</display:table>
			</jstl:if>

			<!-- CAMPBELL SO-P2 -->
			<jstl:if test="${fn:contains(requestURI,'deviceId=72')}">
				<display:table name="deviceData" id="row" requestURI="${requestURI}"
					pagesize="10" class="col-12 justify-content-center displaytag">
					<display:column property="moment"
						titleKey="device.campbellsop2.moment" />
					<display:column property="vwc_p1"
						titleKey="device.campbellsop2.vwcp1" />
					<display:column property="ec_p1"
						titleKey="device.campbellsop2.ecp1" />
					<display:column property="temp_p1"
						titleKey="device.campbellsop2.tempp1" />
					<display:column property="vwc_p2"
						titleKey="device.campbellsop2.vwcp2" />
					<display:column property="ec_p2"
						titleKey="device.campbellsop2.ecp2" />
					<display:column property="temp_p2"
						titleKey="device.campbellsop2.tempp2" />
					<display:column property="vwc_p3"
						titleKey="device.campbellsop2.vwcp3" />
					<display:column property="ec_p3"
						titleKey="device.campbellsop2.ecp3" />
					<display:column property="temp_p3"
						titleKey="device.campbellsop2.tempp3" />
					<display:column property="vwc_p4"
						titleKey="device.campbellsop2.vwcp4" />
					<display:column property="ec_p4"
						titleKey="device.campbellsop2.ecp4" />
					<display:column property="temp_p4"
						titleKey="device.campbellsop2.tempp4" />
				</display:table>
			</jstl:if>

			<!-- CAMPBELL SO-P3 -->
			<jstl:if test="${fn:contains(requestURI,'deviceId=73')}">
				<display:table name="deviceData" id="row" requestURI="${requestURI}"
					pagesize="10" class="col-12 justify-content-center displaytag">
					<display:column property="moment"
						titleKey="device.campbellsop3.moment" />
					<display:column property="vwc_p1"
						titleKey="device.campbellsop3.vwcp1" />
					<display:column property="ec_p1"
						titleKey="device.campbellsop3.ecp1" />
					<display:column property="temp_p1"
						titleKey="device.campbellsop3.tempp1" />
					<display:column property="vwc_p2"
						titleKey="device.campbellsop3.vwcp2" />
					<display:column property="ec_p2"
						titleKey="device.campbellsop3.ecp2" />
					<display:column property="temp_p2"
						titleKey="device.campbellsop3.tempp2" />
					<display:column property="vwc_p3"
						titleKey="device.campbellsop3.vwcp3" />
					<display:column property="ec_p3"
						titleKey="device.campbellsop3.ecp3" />
					<display:column property="temp_p3"
						titleKey="device.campbellsop3.tempp3" />
				</display:table>
			</jstl:if>

			<!-- NIVEL SO-P1 -->
			<jstl:if test="${fn:contains(requestURI,'deviceId=74')}">
				<display:table name="deviceData" id="row" requestURI="${requestURI}"
					pagesize="10" class="col-12 justify-content-center displaytag">
					<display:column property="moment"
						titleKey="device.manualsop1.moment" />
					<display:column property="level" titleKey="device.manualsop1.level" />
				</display:table>
			</jstl:if>

			<div class="form-row text-center mt-3 mb-4">
				<div class="col-12">
					<jstl:if test="${!fn:contains(requestURI,'deviceId=74')}">
						<button class="btn bg-dark text-white" name="downloadData"
							onclick="javascript: relativeRedir('device/downloadData.do?deviceId=${device.id}');">
							<i class="fa fa-angle-double-down"></i>&nbsp;
							<spring:message code="device.download" />
							&nbsp;<i class="fa fa-angle-double-down"></i>
						</button>
					</jstl:if>
					<input class="btn bg-dark text-white ml-2" type="button"
						name="cancel" value="<spring:message code="device.back" />"
						onclick="javascript: relativeRedir('device/station/list.do?stationId=${station.id}');" />
				</div>
			</div>

			<hr class="hr-style">

			<div class="form-row text-center mt-3 mb-4">
				<div class="col-12">
					<h3 class="text-reset">
						<spring:message code="device.graphics" />
					</h3>
				</div>
				<div class="col-12">
					<button id="chartButton" class="btn bg-dark text-white"
						name="graph"
						onclick="javascript: relativeRedir('device/graphData.do?deviceId=${device.id}');">
						<spring:message code="device.graphData1" />
						<br /> <i class="fa fa-signal"></i><br />
						<spring:message code="device.graphData2" />
					</button>
					<div id="chartImg" class="mt-2">
						<jstl:if test="${imgPath!=''}">
							<jstl:choose>
								<jstl:when test="${fn:contains(imgPath,'lisimeterMeteo')}">
									<img src="images/plots/lisimeterMeteo_1.png"
										alt='<spring:message code="device.finished" />'><br/><br/>
									<img src="images/plots/lisimeterMeteo_2.png"
										alt='<spring:message code="device.finished" />'><br/><br/>
									<img src="images/plots/lisimeterMeteo_3.png"
										alt='<spring:message code="device.finished" />'><br/><br/>
									<img src="images/plots/lisimeterMeteo_4.png"
										alt='<spring:message code="device.finished" />'>
								</jstl:when>
								<jstl:otherwise>
									<img src="${imgPath}"
										alt='<spring:message code="device.finished" />'>
										<br/><br/>
										<span style="padding:10px;border:5px outset #444"><i>${imgPath}</i></span>
								</jstl:otherwise>

							</jstl:choose>

						</jstl:if>

					</div>
					<div id="chartLoading">
						<img src="images/loading.gif" width="60em"><br />
						<spring:message code="device.pleaseWait" />
					</div>
				</div>
			</div>
		</jstl:when>
		<jstl:otherwise>
			<br />
			<div class="col-12 justify-content-center">
				<p class="text-reset text-center">
					<spring:message code="device.dataEmpty" />
				</p>
			</div>
			<div class="form-row text-center mt-3 mb-4">
				<div class="col-12">
					<input class="btn bg-dark text-white ml-2" type="button"
						name="cancel" value="<spring:message code="device.back" />"
						onclick="javascript: relativeRedir('device/station/list.do?stationId=${station.id}');" />
				</div>
			</div>
		</jstl:otherwise>
	</jstl:choose>



	<jstl:choose>
		<jstl:when test="${fn:length(deviceData)>0}">

			<hr class="hr-style">

			<security:authorize access="hasRole('RESEARCHER')">
				<div class="form-row mb-3 pb-1 text-center justify-content-center">
					<button style="background-color: #cc0000" class="btn text-white"
						name="deleteData"
						onclick="javascript: relativeRedir('device/researcher/deleteData.do?stationId=${station.id}&deviceId=${device.id}');">
						<i class="fa fa-times"></i>&nbsp;
						<spring:message code="device.deleteData" />
					</button>
				</div>
			</security:authorize>
		</jstl:when>
	</jstl:choose>

</div>

