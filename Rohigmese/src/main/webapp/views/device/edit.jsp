<%--
 * edit.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<div class="container justify-content-center py-1">

	<div class="form-row justify-content-center py-2">
		<p class="text-reset text-center">
			<spring:message code="device.editOperative" />
		</p>
	</div>

	<hr class="hr-style">

	<form:form action="device/researcher/edit.do" modelAttribute="device"
		novalidate="novalidate">

		<form:hidden path="id" />
		<form:hidden path="version" />
		<form:hidden path="name" />
		<form:hidden path="localization.latitude" />
		<form:hidden path="localization.longitude" />
		<form:hidden path="description" />
		<form:hidden path="brand" />
		<form:hidden path="price" />
		<form:hidden path="comments" />
		<form:hidden path="station" />

		<jstl:choose>
			<jstl:when test="${device.operative==true}">
				<div class="form-row justify-content-center py-1">
					<div class="form-check col-12 text-center">
						<input class="form-check-input" type="radio" name="operative"
							id="yes" value="yes" checked> <label
							class="form-check-label" for="yes"><spring:message
								code="device.operative.yes" /></label>
					</div>
					<div class="form-check col-12 text-center">
						<input class="form-check-input" type="radio" name="operative"
							id="no" value="no"> <label class="form-check-label"
							for="no"><spring:message code="device.operative.no" /></label>
					</div>
				</div>
			</jstl:when>
			<jstl:otherwise>
				<div class="form-row justify-content-center py-1">
					<div class="form-check col-12 text-center">
						<input class="form-check-input" type="radio" name="operative"
							id="yes" value="yes"> <label
							class="form-check-label" for="yes"><spring:message
								code="device.operative.yes" /></label>
					</div>
					<div class="form-check col-12 text-center">
						<input class="form-check-input" type="radio" name="operative"
							id="no" value="no" checked> <label class="form-check-label"
							for="no"><spring:message code="device.operative.no" /></label>
					</div>
				</div>
			</jstl:otherwise>
		</jstl:choose>

		<div class="row py-3 text-center">
			<div class="col-12">
				<input class="btn bg-dark text-white" type="submit" name="save"
					value="<spring:message code="user.save" />" /> <input
					class="btn bg-dark text-white" type="button" name="cancel"
					value="<spring:message code="user.cancel" />"
					onclick="javascript: relativeRedir('device/station/list.do?stationId=${device.station.id}');" />
			</div>
		</div>


	</form:form>

</div>
