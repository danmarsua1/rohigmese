<%--
 * list.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<script>
	function loadMap() {

		// Initialize the platform object:
		var platform = new H.service.Platform({
			'apikey' : '8YfcRUCCSmjqi5Fyd9Awv0mbyEZFijL3nqfQuVnHd1A'
		});

		// Obtain the default map types from the platform object
		var defaultLayers = platform.createDefaultLayers();

		// Instantiate (and display) a map object:
		var map = new H.Map(document.getElementById('mapContainer'),
				defaultLayers.raster.terrain.map, {
					zoom : 10,
					center : {
						lat : 37.0427289,
						lng : -6.4344467
					}
				});

		//Enable map events on current map
		var mapEvents = new H.mapevents.MapEvents(map);

		// Instantiate the default behavior, providing the mapEvents object:
		var behavior = new H.mapevents.Behavior(mapEvents);

		// Create the default UI:
		var ui = H.ui.UI.createDefault(map, defaultLayers);
		var zoom = ui.getControl('zoom');
		var scalebar = ui.getControl('scalebar');
		
		zoom.setAlignment('top-left');
		scalebar.setAlignment('top-left');
		
		
		//Markers
		<jstl:forEach var="device" items="${devices}">
			var dlat = <jstl:out value="${device.localization.latitude}"/>;
			var dlng = <jstl:out value="${device.localization.longitude}"/>;
			var content = "<b><jstl:out value='${device.name}' /></b>"
			+ "<br/><jstl:out value='${device.brand}' />"
			<jstl:choose>
				<jstl:when test="${device.operative==true}">
				+ "<br/><spring:message code='device.operative' />: <img style='vertical-align:middle' src='images/operative.png' alt='Operative' width='20px' />"
				</jstl:when>
				<jstl:otherwise>
				+ "<br/><spring:message code='device.operative' />: <img style='vertical-align:middle' src='images/no-operative.png' alt='Not operative' width='20px' />"
				</jstl:otherwise>
			</jstl:choose>
			+ "<br/>(<jstl:out value='${device.localization.latitude}' />, <jstl:out value='${device.localization.longitude}' />)"
			+ "<br/><i style='color:#007bff;cursor:pointer' class='fa fa-database fa-lg'><span style='color:#007bff;font: 14px sans-serif;font-weight:bolder' class='mapLink' onclick='redirect(<jstl:out value='${device.id}' />);'>&nbsp;<spring:message code='device.viewData' /></span></i>";
			var customMarker = newMarker(dlat,dlng, map);
			addEventsMarker(customMarker,ui,content);
		</jstl:forEach>

	}
	
	function redirect(deviceId){
		var str = "device/viewData.do?deviceId="+deviceId;
		relativeRedir(str)
	}
	
	function newMarker(clat,clng, map){
		
		// Create a marker icon from an image
		var icon = new H.map.Icon("images/marker2.png", {size: {w: 35, h: 35}});
		
		// Create a marker
		var marker = new H.map.Marker({
			lat : clat,
			lng : clng
		}, {
			icon : icon
		});
		
		// Add the marker to the map:
		map.addObject(marker);
		
		return marker;
		
	}
	
	function addEventsMarker(marker,ui,contentString){
		
		marker.addEventListener('pointerenter', function(evt) {
			$("#mapContainer").css("cursor", "pointer"); 
		}, false);
		
		marker.addEventListener('pointerleave', function(evt) {
			$("#mapContainer").css("cursor", "default"); 
		}, false);
		
		marker.addEventListener('tap', function(evt) {
			
			ui.addBubble(new H.ui.InfoBubble({
				lat : marker.b.lat,
				lng : marker.b.lng
			}, {
				content : contentString
			}));
		}, false);
		
	}

	$(document).ready(function() {
		loadMap();
	});
</script>

<!-- ALERTS -->
<jstl:if test="${notFoundError!=null && notFoundError!=''}">
	<div id="alert" class="row justify-content-center">
		<div class="col-12">
			<div class="alert alert-danger text-center" role="alert">
				${notFoundError}</div>
		</div>
	</div>
</jstl:if>

<div class="container-fluid justify-content-center py-1">

	<security:authorize access="hasRole('RESEARCHER')">
		<div class="form-row pt-3 text-center">
			<div class="col-12">
				<button class="btn bg-dark text-white" name="newData"
					onclick="javascript: relativeRedir('device/researcher/selectDevice.do?stationId=${station.id}');">
					<i class="fa fa-plus"></i>&nbsp;
					<spring:message code="device.newData" />
				</button>
			</div>
		</div>
	</security:authorize>

	<div class="form-row text-center mt-3 mb-4">
		<div class="col-12">
			<input class="btn bg-dark text-white ml-2" type="button"
				name="cancel" value="<spring:message code="device.back" />"
				onclick="javascript: relativeRedir('station/zone/list.do?zoneId=${station.zone.id}');" />
		</div>
	</div>

	<fieldset id="fieldsetLegend">
	<legend id="legendStatus">
		<spring:message code="device.legend" />
	</legend>
		<table style="border:0;margin:0 0 10px 0!important;" class="table table-borderless col-5">
			<tbody>
				<tr>
					<td><img src="images/operative.png" alt="Operative"
						width="24px" />
						<span class="align-middle">
							<i><spring:message code="device.trackingAvailable" /></i>
						</span></td>
					<td><img src="images/no-operative.png" alt="No Operative"
						width="24px" />
						<span class="align-middle">
							<i><spring:message code="device.trackingNotAvailable" /></i>
						</span></td>
				</tr>
			</tbody>
		</table>
	</fieldset>



	<jstl:choose>
		<jstl:when test="${fn:length(devices)>0}">
			<table class="table">
				<thead>
					<tr>
						<th scope="col">#</th>
						<th scope="col"><spring:message code="device.data1" /></th>
						<jstl:if test="${!fn:contains(requestURI,'stationId=58')}">
						<th class="text-center" scope="col"><spring:message
								code="device.dataDownload" /></th>
						</jstl:if>
						<th scope="col"><spring:message code="device.name" /></th>
						<th class="text-center" scope="col"><spring:message
								code="device.operative" /></th>
						<th scope="col"><spring:message code="device.description" /></th>
						<jstl:if test="${!fn:contains(requestURI,'stationId=58')}">
							<th scope="col"><spring:message code="device.brand" /></th>
						</jstl:if>
						<security:authorize access="hasRole('RESEARCHER')">
							<th scope="col"><spring:message code="device.price" /></th>
						</security:authorize>
						<th scope="col"><spring:message code="device.localization" /></th>
						<th scope="col"><spring:message code="device.comments" /></th>
					</tr>
				</thead>
				<tbody>
					<jstl:set var="cont" value="0" />
					<jstl:forEach var="device" items="${devices}">
						<jstl:set var="cont" value="${cont + 1}" />
						<tr>
							<th scope="row">${cont}</th>
							<td><i
								onclick='redirect(<jstl:out value='${device.id}' />);'
								style='color: #007bff; cursor: pointer'
								class='fa fa-database fa-lg'><span
									style='color: #007bff; font: 14px sans-serif; font-weight: bolder'>&nbsp;&nbsp;<spring:message
											code="device.viewData" /></span></i></td>
							<jstl:if test="${!fn:contains(requestURI,'stationId=58')}">				
							<td class="text-center"><i
								onclick="javascript: relativeRedir('device/downloadData.do?deviceId=${device.id}')"
								style='color: #007bff; cursor: pointer'
								class="fa fa-angle-double-down fa-lg"><span
									style='color: #007bff; font: 14px sans-serif; font-weight: bolder'>&nbsp;<spring:message
											code="device.dataDownload1" /></span></i></td>
											</jstl:if>
							<td><jstl:out value="${device.name}" /></td>
							<jstl:choose>
								<jstl:when test="${device.operative==true}">
									<td class="text-center"><img src="images/operative.png"
										alt="Operative" width="24px" /> <security:authorize
											access="hasRole('RESEARCHER')">
											<i
												onclick="javascript: relativeRedir('device/researcher/edit.do?deviceId=${device.id}')"
												style='color: #007bff; cursor: pointer' class="fa fa-pencil"><span
												style='color: #007bff; font: 14px sans-serif; font-weight: bolder'>&nbsp;<spring:message
														code="device.operative.change" /></span></i>
										</security:authorize></td>
								</jstl:when>
								<jstl:otherwise>
									<td class="text-center"><img src="images/no-operative.png"
										alt="Not operative" width="24px" /> <security:authorize
											access="hasRole('RESEARCHER')">
											<i
												onclick="javascript: relativeRedir('device/researcher/edit.do?deviceId=${device.id}')"
												style='color: #007bff; cursor: pointer' class="fa fa-pencil"><span
												style='color: #007bff; font: 14px sans-serif; font-weight: bolder'>&nbsp;<spring:message
														code="device.operative.change" /></span></i>
										</security:authorize></td>
								</jstl:otherwise>
							</jstl:choose>
							<jstl:choose>
								<jstl:when
									test="${device.description=='' || device.description==null}">
									<td><spring:message code="device.noDescription" /></td>
								</jstl:when>
								<jstl:otherwise>
									<td><jstl:out value="${device.description}" /></td>
								</jstl:otherwise>
							</jstl:choose>
							<jstl:if test="${!fn:contains(requestURI,'stationId=58')}">
								<td><jstl:out value="${device.brand}" /></td>
							</jstl:if>
							<security:authorize access="hasRole('RESEARCHER')">
								<td><jstl:out value="${device.price}" />&euro;</td>
							</security:authorize>
							<td><jstl:out
									value="${device.localization.latitude}, ${device.localization.longitude}" /></td>
							<jstl:choose>
								<jstl:when
									test="${device.comments=='' || device.comments==null}">
									<td><spring:message code="device.noComments" /></td>
								</jstl:when>
								<jstl:otherwise>
									<td><jstl:out value="${device.comments}" /></td>
								</jstl:otherwise>
							</jstl:choose>
						</tr>
					</jstl:forEach>
				</tbody>
			</table>
			<div class="container justify-content-center">
				<div class="row text-center pt-2">
					<div style="width: 100%; height: 100vh" id="mapContainer"></div>
				</div>
			</div>
		</jstl:when>
		<jstl:otherwise>
			<br />
			<div class="form-row justify-content-center">
				<p class="text-reset text-center">
					<spring:message code="device.empty" />
				</p>
			</div>
		</jstl:otherwise>
	</jstl:choose>



</div>




