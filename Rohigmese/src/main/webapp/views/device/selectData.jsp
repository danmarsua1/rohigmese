<%--
 * new_researcher.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<script>
	$(document).ready(function() {
		
		$("input[name='run']").click(function() {
			$("input[name='run']").hide();
			$("input[name='cancel']").hide();
			$("#alert").hide();
			$("#progressBar1").removeAttr("hidden");
			$("#progressText1").removeAttr("hidden");
		});
		
		$("input[name='save']").click(function() {
			$("input[name='save']").hide();
			$("input[name='cancel']").hide();
			$("#alert").hide();
			$("#progressBar2").removeAttr("hidden");
			$("#progressText2").removeAttr("hidden");
		});
	});
</script>

<!-- ALERTS -->
<jstl:if test="${wrongFileAlert!=null}">
	<div id="alert" class="row justify-content-center">
		<div class="col-12">
			<div class="alert alert-danger text-center" role="alert">
				${wrongFileAlert}</div>
		</div>
	</div>
</jstl:if>

<!-- CODE -->
<div class="container justify-content-center py-1">

	<div class="form-row text-center mt-1 mb-3">
		<div class="col-12">
			<input class="btn bg-dark text-white ml-2" type="button"
				name="cancel" value="<spring:message code="device.back" />"
				onclick="javascript: relativeRedir('device/researcher/selectDevice.do?stationId=${station.id}');" />
		</div>
	</div>

	<div id="accordion">
		<div class="card">
			<div class="card-header" id="headingOne">
				<h5 class="mb-0">
					<a style="color: #007bff" class="btn" data-toggle="collapse"
						data-target="#collapseOne" aria-expanded="true"
						aria-controls="collapseOne"> <spring:message
							code="device.options.scriptsr" />
					</a>
				</h5>
			</div>
			<div id="collapseOne" class="collapse p-3"
				aria-labelledby="headingOne" data-parent="#accordion">
				<form class="needs-validation" action="device/researcher/runRScript.do"
					method="POST" novalidate="novalidate">

					<input type="hidden" value="${scriptPath}" name="scriptPath" />
					<input type="hidden" value="${script}" name="scriptName" />
					<input type="hidden" value="${station.id}" name="stationId" />

					<div class="form-row justify-content-left">
						<p class="text-reset text-justify">
							<spring:message code="device.options.runScript" />
						</p>
					</div>

					<div class="form-row justify-content-left">
						<span class="text-reset text-justify"> <b><spring:message
									code="device.options.runScript.zone" /></b> <jstl:out
								value=" ${zone.name}"></jstl:out>
						</span>
					</div>
					<div class="form-row justify-content-left">
						<span class="text-reset text-justify"> <b><spring:message
									code="device.options.runScript.station" /></b> <jstl:out
								value=" ${station.name}"></jstl:out>
						</span>
					</div>
					<div class="form-row justify-content-left">
						<jstl:choose>
							<jstl:when
								test="${device.brand!=null and !fn:contains(device.name,'Sensores')}">
								<span class="text-reset text-justify"> <b><spring:message
											code="device.options.runScript.device" /></b> <jstl:out
										value=" ${device.name}  [${device.brand}]"></jstl:out>
								</span>
							</jstl:when>
							<jstl:otherwise>
								<span class="text-reset text-justify"> <b><spring:message
											code="device.options.runScript.device" /></b> <jstl:out
										value=" ${device.name}"></jstl:out>
								</span>
							</jstl:otherwise>
						</jstl:choose>

					</div>
					<br />
					<div class="form-row justify-content-left">
						<span class="text-reset text-justify"><spring:message
								code="device.options.chooseScript" /> <b><jstl:out
									value=" ${script}"></jstl:out></b> </span>
					</div>
					<div class="form-row justify-content-left">
						<jstl:choose>
							<jstl:when test="${fileExist==true}">
								<span style="color:green!important" class="text-reset text-justify"><spring:message
											code="device.options.existScript" />
								</span>
							</jstl:when>
							<jstl:otherwise>
								<span style="color:gray!important" class="text-reset text-justify"><spring:message
											code="device.options.notExistScript" />
								</span>
							</jstl:otherwise>
						</jstl:choose>
					</div>
					<br />
					<!-- Bootstrap Progress bar -->
					<div hidden="hidden" id="progressBar1" class="progress mt-3 mb-3">
						<div
							class="progress-bar progress-bar-striped progress-bar-animated"
							role="progressbar" aria-valuenow="100" aria-valuemin="0"
							aria-valuemax="100" style="width: 100%">
							<spring:message code="device.dataLoading" />
						</div>
					</div>
					<div hidden="hidden" id="progressText1">
						<p class="text-reset text-center mt-1">
							<b><spring:message code="device.script.textLoading" /></b>
						</p>
					</div>
					<!-- Bootstrap Progress bar -->
					
					<div class="justify-content-center text-center col-12">
						<input class="btn bg-dark text-white" type="submit" name="run"
							value="<spring:message code="device.options.button.runScript" />" />
					</div>

				</form>

			</div>

		</div>
		<jstl:if test="${fileExist==true}">
		<div class="card">
			<div class="card-header" id="headingTwo">
				<h5 class="mb-0">
					<a style="color: #007bff" class="btn" data-toggle="collapse"
						data-target="#collapseTwo" aria-expanded="false"
						aria-controls="collapseTwo"> <spring:message
							code="device.options.importfile" />
					</a>
				</h5>
			</div>
			<div id="collapseTwo" class="collapse p-3"
				aria-labelledby="headingTwo" data-parent="#accordion">
				<form class="needs-validation" action="device/researcher/newData.do"
					method="POST" novalidate="novalidate">

					<input type="hidden" value="${device.id}" name="deviceSelected" />
					<input type="hidden" value="${filename}" name="filename" />

					<div class="form-row justify-content-center">
						<p class="text-reset text-justify">
							<spring:message code="device.newDataDevice" />
							<b><jstl:out value=" '${filename}'"></jstl:out></b>
						</p>
					</div>
					<div class="form-row justify-content-center">
						<input type="file" id="file" name="file">
					</div>
					<br />
					<!-- Bootstrap Progress bar -->
					<div hidden="hidden" id="progressBar2" class="progress mt-3 mb-3">
						<div
							class="progress-bar progress-bar-striped progress-bar-animated"
							role="progressbar" aria-valuenow="100" aria-valuemin="0"
							aria-valuemax="100" style="width: 100%">
							<spring:message code="device.dataLoading" />
						</div>
					</div>
					<div hidden="hidden" id="progressText2">
						<p class="text-reset text-center mt-1">
							<b><spring:message code="device.textLoading" /></b>
						</p>
					</div>
					<!-- Bootstrap Progress bar -->
					<div class="justify-content-center text-center col-12">
						<input class="btn bg-dark text-white" type="submit" name="save"
							value="<spring:message code="device.options.button.import" />" />
					</div>
				</form>
			</div>
		</div>
		</jstl:if>
	</div>
	


</div>

