<%--
 * new_researcher.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<!-- ALERTS -->
<jstl:if test="${scriptExecuted!=null && scriptExecuted!=''}">
	<div id="alert-success" class="row justify-content-center">
		<div class="col-12">
			<div class="alert alert-success text-center" role="alert">
				${scriptExecuted}</div>
		</div>
	</div>
</jstl:if>

<!-- CODE -->
<div class="container justify-content-center py-1">

	<form class="needs-validation" action="device/researcher/selectData.do" method="POST" novalidate="novalidate">
		
		<br/>
		<div class="form-row justify-content-center">
			<p class="text-reset text-center"><spring:message code="device.selectDevice" /></p>
		</div>
		
		<hr class="hr-style">

		<div class="form-row text-center">
			<div class="col-12">
				<select class="w-50" name="deviceSelected">
				    <jstl:forEach var="d" items="${devices}">
				    	<jstl:choose>
				    	<jstl:when test="${d.brand!=null}">
				    		<option value="${d.id}"><jstl:out value="${d.name} [${d.brand}]"/></option>
				    	</jstl:when>
				    	<jstl:otherwise>
				    		<option value="${d.id}"><jstl:out value="${d.name}"/></option>
				    	</jstl:otherwise>
				    	</jstl:choose>
				        
				    </jstl:forEach>
				</select>
			</div>
		</div>
	
		<div class="form-row text-center mt-3 mb-4">
	  		<div class="col-12">
		  		<input class="btn bg-dark text-white" type="submit" name="save"
				value="<spring:message code="device.next" />" />
				
				<input class="btn bg-dark text-white ml-2" type="button" name="cancel"
					value="<spring:message code="device.cancel" />"
					onclick="javascript: relativeRedir('device/station/list.do?stationId=${station.id}');" />
			</div>
	  	</div>
	
	</form>

</div>

