 <%--
 * login.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<head>
	<link rel="stylesheet" href="styles/login.css" type="text/css">
</head>

<script>

$(document).ready(function(){
	$("#alert-register").fadeOut(5000);
});

</script>

<!-- ALERT SUCCESS -->
<jstl:if test="${registerSuccess!=''}">
	<div id="alert-register" class="row justify-content-center">
		<div class="rounded-sm">
			<div class="alert alert-success text-center" role="alert">
				${registerSuccess}
			</div>
		</div>
	</div>
</jstl:if>

<div class="login-body container-fluid">

	<div class="row py-5">
		<div class="login-form container col-9 col-sm-8 col-md-6 col-lg-6 col-xl-4 rounded-sm">
		
			<form:form action="j_spring_security_check" modelAttribute="credentials">
			
				<div class="imgcontainer container-fluid text-center pt-3 pb-2">
			    	<img src="images/rohigmese.png" alt="Rohigmese" width="70px">
			  	</div>
			
				<hr class="hr-style">
			
				<div class="container">
					<div class="row justify-content-center py-2">
						<div class="col-11">
							<form:label path="username" for="user" class="mb-0">
								<spring:message code="security.username" />
							</form:label>
							<form:input class="form-control" id="user" path="username" />	
							<form:errors class="error" path="username" />
						</div>
					</div>
					<div class="row justify-content-center py-2">
						<div class="col-11">
							<form:label path="password" for="pass" class="mb-0">
								<spring:message code="security.password" />
							</form:label>
							<form:password class="form-control" id="pass" path="password" />	
							<form:errors class="error" path="password" />
						</div>
					</div>
					<div class="row justify-content-center pt-2">
						<div class="col-11">
							<jstl:if test="${showError == true}">
								<div class="alert alert-danger text-center" role="alert">
									<spring:message code="security.login.failed" />
								</div>
							</jstl:if>
						</div>
					</div>
					<div class="row justify-content-center pt-0 mb-3">
						<div class="col-12 text-center">
							<button class="btn submit rounded-sm" type="submit" ><span class="font-weight-bold"><spring:message code="security.login" /></span></button>
						</div>
						<div class="col-12 text-center mt-3">
							<a href="user/create.do" class="stretched-link"><spring:message code="security.newAccount" /></a>
						</div>
					</div>
				</div>
				
			</form:form>
		
		</div>
	</div>
</div>


