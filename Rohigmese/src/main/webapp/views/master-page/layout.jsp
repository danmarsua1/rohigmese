<%--
 * layout.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!DOCTYPE html>
<html>
<head>

<base
	href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/" />

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<meta name="viewport" content="initial-scale=1.0, width=device-width" />

<link rel="shortcut icon" href="/favicon.ico" />

<link rel="stylesheet" href="styles/common.css" type="text/css">
<!--  <link rel="stylesheet" href="styles/jmenu.css" media="screen" type="text/css" />-->
<link rel="stylesheet" href="styles/displaytag.css" type="text/css">
<link rel="stylesheet" href="styles/bootstrap.min.css" type="text/css">

<!-- FONT-AWESOME -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<!-- HERE MAPS -->
<link rel="stylesheet" type="text/css"
	href="https://js.api.here.com/v3/3.1/mapsjs-ui.css" />

<script type="text/javascript" src="scripts/jquery.js"></script>
<script type="text/javascript" src="scripts/jquery-ui.js"></script>

<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script type="text/javascript" src="scripts/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="scripts/popper.min.js"></script>
<script type="text/javascript" src="scripts/bootstrap.min.js"></script>

<!-- HERE MAPS -->
<script src="https://js.api.here.com/v3/3.1/mapsjs-core.js"
	type="text/javascript" charset="utf-8"></script>
<script src="https://js.api.here.com/v3/3.1/mapsjs-service.js"
	type="text/javascript" charset="utf-8"></script>
<script src="https://js.api.here.com/v3/3.1/mapsjs-mapevents.js"
	type="text/javascript" charset="utf-8"></script>
<script src="https://js.api.here.com/v3/3.1/mapsjs-ui.js"
	type="text/javascript" charset="utf-8"></script>

<title><tiles:insertAttribute name="title" ignore="true" /></title>

<script type="text/javascript">
	/*$(document).ready(function() {
		$("#jMenu").jMenu();
	});*/

	function askSubmission(msg, form) {
		if (confirm(msg))
			form.submit();
	}

	function relativeRedir(loc) {
		var b = document.getElementsByTagName('base');
		if (b && b[0] && b[0].href) {
			if (b[0].href.substr(b[0].href.length - 1) == '/'
					&& loc.charAt(0) == '/')
				loc = loc.substr(1);
			loc = b[0].href + loc;
		}
		window.location.replace(loc);
	}
</script>

</head>

<body>

	<!-- CABECERA -->

	<div>
		<tiles:insertAttribute name="header" />
	</div>



	<div id="body-content">

		<!-- BARRA DE NAVEGACION -->

		<nav class="navbar navbar-expand-lg navbar-dark bg-dark py-2">
			<a style="color: #e1e1e1; font-size: 14px"
				class="navbar-brand font-italic" href="welcome/index.do">ROHIGMESE
				v1.0</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown"
				aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNavDropdown">
				<ul class="navbar-nav">

					<!-- ADMINISTRATOR -->
					<security:authorize access="hasRole('ADMINISTRATOR')">
						<li class="nav-item dropdown"><a
							class="nav-link dropdown-toggle text-white" href="#"
							id="navbarDropdownMenuLink" role="button" data-toggle="dropdown"
							aria-haspopup="true" aria-expanded="false"> <spring:message
									code="master.page.administrator" />
						</a>
							<div class="dropdown-menu"
								aria-labelledby="navbarDropdownMenuLink">
								<a class="dropdown-item" href="administrator/user/list.do">
									<spring:message code="master.page.administrator.listUsers" />
								</a> <a class="dropdown-item"
									href="researcher/administrator/create.do"> <spring:message
										code="master.page.administrator.newResearcher" /></a> <a
									class="dropdown-item" href="administrator/invitation/list.do">
									<spring:message
										code="master.page.administrator.listInvitations" />
								</a>
							</div></li>
						<li class="nav-item"><a class="nav-link text-white"
							href="zone/list.do"> <spring:message
									code="master.page.listData" /></a></li>
					</security:authorize>

					<!-- RESEARCHER -->
					<security:authorize access="hasRole('RESEARCHER')">
						<li class="nav-item dropdown"><a
							class="nav-link dropdown-toggle text-white" href="#"
							id="navbarDropdownMenuLink" role="button" data-toggle="dropdown"
							aria-haspopup="true" aria-expanded="false"> <spring:message
									code="master.page.researcher.controlPanel" />
						</a>
							<div class="dropdown-menu"
								aria-labelledby="navbarDropdownMenuLink">
								<a class="dropdown-item" href="zone/researcher/list.do"> <spring:message
										code="master.page.researcher.listZones" /></a> <a
									class="dropdown-item" href="history/list.do"> <spring:message
										code="master.page.history" /></a>
							</div></li>
					</security:authorize>

					<!-- USER -->
					<security:authorize access="hasRole('USER')">
						<li class="nav-item"><a class="nav-link text-white"
							href="zone/list.do"> <spring:message
									code="master.page.listData" /></a></li>
					</security:authorize>

					<!-- ANONYMOUS -->
					<security:authorize access="isAnonymous()">
						<li class="nav-item"><a class="nav-link text-white"
							href="security/login.do"><spring:message
									code="master.page.login" /></a></li>
					</security:authorize>

					<!-- LOGGED -->
					<security:authorize access="isAuthenticated()">
						<li class="nav-item dropdown"><a
							class="nav-link dropdown-toggle text-white" href="#"
							id="navbarDropdownMenuLink" role="button" data-toggle="dropdown"
							aria-haspopup="true" aria-expanded="false"> <spring:message
									code="master.page.profile" /> (<security:authentication
									property="principal.username" />)
						</a>
							<div class="dropdown-menu"
								aria-labelledby="navbarDropdownMenuLink">
								<security:authorize access="hasRole('ADMINISTRATOR')">
									<a class="dropdown-item" href="administrator/edit.do"> <spring:message
											code="master.page.myinfo" /></a>
								</security:authorize>
								<security:authorize access="hasRole('RESEARCHER')">
									<a class="dropdown-item" href="researcher/edit.do"> <spring:message
											code="master.page.myinfo" /></a>
								</security:authorize>
								<security:authorize access="hasRole('USER')">
									<a class="dropdown-item" href="user/edit.do"> <spring:message
											code="master.page.myinfo" /></a>
								</security:authorize>
								<a class="dropdown-item" href="j_spring_security_logout"> <spring:message
										code="master.page.logout" /></a>
							</div></li>
					</security:authorize>

				</ul>
			</div>
		</nav>

		<jstl:choose>
			<jstl:when test="${fn:length(sessionScope.username)>0}">
				<tiles:insertAttribute name="body" />
			</jstl:when>
			<jstl:otherwise>
				<jstl:choose>
					<jstl:when
						test="${!fn:contains(requestScope['javax.servlet.forward.request_uri'],'/login.do') && !fn:contains(requestScope['javax.servlet.forward.request_uri'],'/Rohigmese') && !fn:contains(requestScope['javax.servlet.forward.request_uri'].concat('?'.concat(requestScope['javax.servlet.forward.query_string'])),'/Rohigmese/?language')}">
						<div class="message pt-4 pb-4 text-center">
							<spring:message code="master.page.sessionTimeOut" />
						</div>
					</jstl:when>
					<jstl:otherwise>
						<tiles:insertAttribute name="body" />
					</jstl:otherwise>
				</jstl:choose>
			</jstl:otherwise>
		</jstl:choose>

		<jstl:if test="${message != null}">
			<br />
			<span class="message"><spring:message code="${message}" /></span>
		</jstl:if>
	</div>

	<!-- PIE DE PAGINA -->

	<div>
		<tiles:insertAttribute name="footer" />
	</div>

</body>
</html>