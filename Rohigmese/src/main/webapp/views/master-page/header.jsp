<%--
 * header.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>

		<jstl:choose>
		<jstl:when test="${fn:contains(requestURI,'loginURI')}">
		<div class="header-container container-fluid border-bottom border-secondary pb-1">
			<div style="height:90px" class="row align-items-center">
		
				<div style="width:200px" class="ml-2 mr-3">
					<img src="images/logo_igme.jpg" alt="Rohigmese, IGME Sevilla" width="200px"/>
				</div>
				
				<div class="languages p-2">
					<a href="?language=es"><img src="images/languages/spanish.png" width="35px" /></a>&nbsp;&nbsp;
					<a href="?language=en"><img src="images/languages/english.png" width="35px" /></a>
				</div>
				
			</div>
		</div>
		</jstl:when>
		
		<jstl:otherwise>
		<div class="header-container container-fluid pb-3">
			<div style="height:90px" class="row align-items-center">
		
				<div style="width:200px" class="ml-2 mr-3">
					<img src="images/logo_igme.jpg" alt="Rohigmese, IGME Sevilla" width="200px"/>
				</div>
				
				<div class="languages p-2">
					<a href="welcome/index.do?language=es"><img src="images/languages/spanish.png" width="35px" /></a>&nbsp;&nbsp;
					<a href="welcome/index.do?language=en"><img src="images/languages/english.png" width="35px" /></a>
				</div>
			</div>
			<div class="row">
				<div class="div-appTitle col-12 mb-1">
					<span class="appTitle h4"><spring:message code="appTitle" /></span>
				</div>
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb bg-white pb-0 mb-0">
				  	<jstl:choose>
				  	<jstl:when test="${fn:contains(currentPage,'Index')}">
				   	 	<li class="breadcrumb-item active" aria-current="page"><spring:message code="appIndex" /></li>
				   	 </jstl:when>
				   	 <jstl:otherwise>
				   	 	<li class="breadcrumb-item"><a href="javascript: relativeRedir('welcome/index.do');"><spring:message code="appIndex" /></a></li>
				   	 	<jstl:if test="${parent!=null}">
				   	 		<li class="breadcrumb-item active" aria-current="page"><span>${parent}</span></li>
				   	 	</jstl:if>
				   	 	<li class="breadcrumb-item active" aria-current="page"><span>${currentPage}</span></li>
				   	 </jstl:otherwise>
				    </jstl:choose>
				  </ol>
				</nav>
			</div>
			
		</div>
		</jstl:otherwise>
		</jstl:choose>

