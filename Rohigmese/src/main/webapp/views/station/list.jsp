<%--
 * list.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<script>
function loadMap() {

	// Initialize the platform object:
	var platform = new H.service.Platform({
		'apikey' : '8YfcRUCCSmjqi5Fyd9Awv0mbyEZFijL3nqfQuVnHd1A'
	});

	// Obtain the default map types from the platform object
	var defaultLayers = platform.createDefaultLayers();

	// Instantiate (and display) a map object:
	var map = new H.Map(document.getElementById('mapContainer'),
			defaultLayers.raster.terrain.map, {
				zoom : 10,
				center : {
					lat : 37.0427289,
					lng : -6.4344467
				}
			});

	//Enable map events on current map
	var mapEvents = new H.mapevents.MapEvents(map);

	// Instantiate the default behavior, providing the mapEvents object:
	var behavior = new H.mapevents.Behavior(mapEvents);

	// Create the default UI:
	var ui = H.ui.UI.createDefault(map, defaultLayers);
	var zoom = ui.getControl('zoom');
	var scalebar = ui.getControl('scalebar');
	
	zoom.setAlignment('top-left');
	scalebar.setAlignment('top-left');
	
	//Markers
	<jstl:forEach var="station" items="${stations}">
		var slat = <jstl:out value="${station.localization.latitude}"/>;
		var slng = <jstl:out value="${station.localization.longitude}"/>;
		var content = "<span style='color:#007bff' class='mapLink' "
		+ "onclick='redirect(<jstl:out value='${station.id}' />);'>"
		+ "<jstl:out value='${station.name}' /></span>"
		+ "<br/>(<jstl:out value='${station.localization.latitude}' />, "
		+ "<jstl:out value='${station.localization.longitude}' />)";
		var customMarker = newMarker(slat,slng, map);
		addEventsMarker(customMarker,ui,content);
	</jstl:forEach>

	}
	
	function redirect(stationId){
		var str = "device/station/list.do?stationId="+stationId;
		relativeRedir(str)
	}
	
	function newMarker(clat,clng, map){
		
		// Create a marker icon from an image
		var icon = new H.map.Icon("images/marker2.png", {size: {w: 35, h: 35}});
		
		// Create a marker
		var marker = new H.map.Marker({
			lat : clat,
			lng : clng
		}, {
			icon : icon
		});
		
		// Add the marker to the map:
		map.addObject(marker);
		
		return marker;
		
	}
	
	function addEventsMarker(marker,ui,contentString){
		
		marker.addEventListener('pointerenter', function(evt) {
			$("#mapContainer").css("cursor", "pointer"); 
		}, false);
		
		marker.addEventListener('pointerleave', function(evt) {
			$("#mapContainer").css("cursor", "default"); 
		}, false);
		
		marker.addEventListener('tap', function(evt) {
			
			ui.addBubble(new H.ui.InfoBubble({
				lat : marker.b.lat,
				lng : marker.b.lng
			}, {
				content : contentString
			}));
		}, false);
		
	}

	$(document).ready(function() {
		loadMap();
	});
</script>

<div class="container justify-content-center py-1">
		
	<jstl:choose>
		<jstl:when test="${fn:length(stations)>0}">
			<div class="form-row text-center mt-3 mb-4">
				<div class="col-12">
					<input class="btn bg-dark text-white ml-2" type="button"
						name="cancel" value="<spring:message code="device.back" />"
						onclick="javascript: relativeRedir('zone/list.do');" />
				</div>
			</div>
			<table class="table">
				<thead>
					<tr>
						<th scope="col">#</th>
						<th scope="col"><spring:message code="station.data1" /></th>
						<th scope="col"><spring:message code="station.name" /></th>
						<th scope="col"><spring:message code="station.authoritativeEntity" /></th>
						<th scope="col"><spring:message code="station.localization" /></th>
						<th scope="col"><spring:message code="station.comments" /></th>
					</tr>
				</thead>
				<tbody>
					<jstl:set var="cont" value="0" />
					<jstl:forEach var="station" items="${stations}">
					<jstl:set var="cont" value="${cont + 1}" />
						<tr>
							<th scope="row">${cont}</th>
							
							<td><i onclick='redirect(<jstl:out value='${station.id}' />);' style='color:#007bff;cursor:pointer' class='fa fa-search fa-lg'><span style='color:#007bff;font: 14px sans-serif;font-weight:bolder'>&nbsp;&nbsp;<spring:message code="station.viewData" /></span></i></td>
							<td><jstl:out value="${station.name}" /></td>
							<td><jstl:out value="${station.authoritativeEntity}" /></td>
							<td><jstl:out value="${station.localization.latitude}, ${station.localization.longitude}" /></td>
							<jstl:choose>
								<jstl:when test="${station.comments=='' || station.comments==null}">
									<td><spring:message code="station.noComments" /></td> 
								</jstl:when>
								<jstl:otherwise>
									<td><jstl:out value="${station.comments}" /></td> 
								</jstl:otherwise>
							</jstl:choose>
						</tr>
					</jstl:forEach>
				</tbody>
			</table>
			<div class="row text-center pt-2">
				<div style="width: 100%; height: 100vh" id="mapContainer"></div>
			</div>
		</jstl:when>
		<jstl:otherwise>
			<br/>
			<div class="form-row justify-content-center">
				<p class="text-reset text-center"><spring:message code="station.empty" /></p>
			</div>
			<div class="form-row text-center mt-3 mb-4">
				<div class="col-12">
					<input class="btn bg-dark text-white ml-2" type="button"
						name="cancel" value="<spring:message code="device.back" />"
						onclick="javascript: relativeRedir('zone/list.do');" />
				</div>
			</div>
		</jstl:otherwise>
	</jstl:choose>
	
	

</div>




