<%--
 * list.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<div class="container justify-content-center py-1">

	<jstl:choose>
		<jstl:when test="${fn:length(history)>0}">
			<table class="table">
				<thead>
					<tr>
						<th scope="col">#</th>
						<th scope="col"><spring:message code="history.date" /></th>
						<th scope="col"><spring:message code="history.author" /></th>
						<th scope="col"><spring:message code="history.device" /></th>
						<th scope="col"><spring:message code="history.records" /></th>
					</tr>
				</thead>
				<tbody>
					<jstl:set var="cont" value="0" />
					<jstl:forEach var="rec" items="${history}">
						<jstl:set var="cont" value="${cont + 1}" />
						<tr>
							<th scope="row">${cont}</th>
							<td><jstl:out value="${rec.date}" /></td>
							<td><jstl:out value="${rec.author}" /></td>
							<jstl:choose>
							<jstl:when test = "${fn:contains(rec.device, '[-]')}">
								<td><i style="color:red" class="fa fa-minus-circle"></i><jstl:out value="${fn:replace(rec.device, '[-]', '')}" /></td>
							</jstl:when>
							<jstl:otherwise>
								<td><i style="color:green" class="fa fa-plus-circle"></i>&nbsp;<jstl:out value="${rec.device}" /></td>
							</jstl:otherwise>
							</jstl:choose>
							<td><jstl:out value="${(rec.idEnd-rec.idStart)}" /></td>
						</tr>
					</jstl:forEach>
				</tbody>
			</table>
		</jstl:when>
		<jstl:otherwise>
			<br />
			<div class="form-row justify-content-center">
				<p class="text-reset text-center">
					<spring:message code="history.empty" />
				</p>
			</div>
		</jstl:otherwise>
	</jstl:choose>

</div>




