<%--
 * list.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<div class="container justify-content-center py-1">

	
	<jstl:choose>
		<jstl:when test="${fn:length(invitations)>0}">
			<table class="table">
				<thead>
					<tr>
						<th scope="col">#</th>
						<th scope="col"><spring:message code="invitations.acceptedText" /></th>
						<th scope="col"><spring:message code="invitations.moment" /></th>
						<th scope="col"><spring:message code="invitations.email" /></th>
						<th scope="col"><spring:message code="invitations.username" /></th>
					</tr>
				</thead>
				<tbody>
					<jstl:set var="cont" value="0" />
					<jstl:forEach var="inv" items="${invitations}">
					<jstl:set var="cont" value="${cont + 1}" />
						<tr>
							<th scope="row">${cont}</th>
							<jstl:choose>
								<jstl:when test="${inv.accepted==false || inv.accepted==null}">
									<td style="color:red"><b><spring:message code="invitations.notAccepted" /></b></td> 
								</jstl:when>
								<jstl:otherwise>
									<td style="color:green"><b><spring:message code="invitations.accepted" /></b></td> 
								</jstl:otherwise>
							</jstl:choose>
							<td><jstl:out value="${inv.moment}" /></td>
							<td><jstl:out value="${inv.recipient}" /></td>
							<td><jstl:out value="${inv.username}" /></td>
						</tr>
					</jstl:forEach>
				</tbody>
			</table>
		</jstl:when>
		<jstl:otherwise>
			<br/>
			<div class="form-row justify-content-center">
				<p class="text-reset text-center"><spring:message code="invitations.empty" /></p>
			</div>
		</jstl:otherwise>
	</jstl:choose>

</div>




