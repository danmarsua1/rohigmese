<%--
 * new_researcher.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<script>

$(document).ready(function(){
	var errors = $("[class='error']");
	
	if(errors.length>0){
		$("input[class='form-control']").addClass("is-valid");
		
		for (var i = 0; i < errors.length; i++) {
			var e = errors[i].id.split(".");
		  	
		  	if(e.length>2){
		  		e = e[1];
		  	}else{
		  		e = e[0];
		  	}
		  	
		  	$("input[id="+e+"]").removeClass("is-valid").addClass("is-invalid");
		  	
		}
	
	}
	 
	
});

</script>

<!-- ALERTS -->
<jstl:if test="${errorsAlert!=null}">
	<div id="alert" class="row justify-content-center">
		<div class="col-12">
			<div class="alert alert-danger text-center" role="alert">
				${errorsAlert}
			</div>
		</div>
	</div>
</jstl:if>

<jstl:if test="${verifyAlert!=null}">
	<div id="alert" class="row justify-content-center">
		<div class="col-12">
			<div class="alert alert-danger text-center" role="alert">
				${verifyAlert}
			</div>
		</div>
	</div>
</jstl:if>

<!-- CODE -->
<div class="container justify-content-center py-1">

	<form:form class="needs-validation" action="researcher/verify.do" novalidate="novalidate">
		
		<br/>
		<div class="form-row justify-content-center">
			<p class="text-reset text-center"><spring:message code="researcher.verify" /></p>
		</div>
	
		<hr class="hr-style">
	
		<fieldset>
		
		<legend>
				<spring:message code="researcher.credentials" />
		</legend>

		    <div class="form-row">
			    <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6 mb-3">
					<label for="username">
				    * <spring:message code="researcher.username" />
				    </label>
				    <input name="username" type="text" class="form-control text-left" id="username" />
			    </div>
			    <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6 mb-3">
			      <label for="password">
			      	* <spring:message code="researcher.password" />
			      </label>
			      <input name="password" type="password" class="form-control text-left" id="password" />
			    </div>
			</div>
	
		</fieldset>
	
		<div class="form-row">
	  		<div class="col-8 col-sm-6 col-md-4 col-lg-6 col-xl-6 mb-3">
		  		<input class="btn bg-dark text-white" type="submit" name="save"
				value="<spring:message code="researcher.save" />" />
		
				<input class="btn bg-dark text-white" type="button" name="cancel"
					value="<spring:message code="researcher.cancel" />"
					onclick="javascript: relativeRedir('security/login.do');" />
			</div>
	  	</div>
	
	</form:form>

</div>

