<%--
 * register.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<script>

$(document).ready(function(){
	var errors = $("[class='error']");
	
	if(errors.length>0){
		$("input[class='form-control']").addClass("is-valid");
		
		for (var i = 0; i < errors.length; i++) {
			var e = errors[i].id.split(".");
		  	
		  	if(e.length>2){
		  		e = e[1];
		  	}else{
		  		e = e[0];
		  	}
		  	
		  	$("input[id="+e+"]").removeClass("is-valid").addClass("is-invalid");
		  	
		}
	
	}
	 
	
});

</script>

<!-- ALERTS -->
<jstl:if test="${errorsAlert!=null}">
	<div id="alert" class="row justify-content-center">
		<div class="col-12">
			<div class="alert alert-danger text-center" role="alert">
				${errorsAlert}
			</div>
		</div>
	</div>
</jstl:if>

<jstl:if test="${verifyAlert!=null}">
	<div id="alert" class="row justify-content-center">
		<div class="col-12">
			<div class="alert alert-danger text-center" role="alert">
				${verifyAlert}
			</div>
		</div>
	</div>
</jstl:if>

<div class="container justify-content-center py-1">

	<form:form class="needs-validation" action="researcher/register.do" modelAttribute="researcher" novalidate="novalidate">
	
		<form:hidden path="id" />
		<form:hidden path="version" />
		<form:hidden path="userAccount.authorities" />
	
		<fieldset>
		
			<legend>
				<spring:message code="researcher.account" />
			</legend>
	
			<div class="form-row">
		    <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6 mb-3">
		      <form:label path="userAccount.username" for="username">
		      	* <spring:message code="researcher.username" />
		      </form:label>
		      <form:input path="userAccount.username" type="text" class="form-control" id="username" />
		      <div class="invalid-feedback">
        		<form:errors class="error" path="userAccount.username" />
      		  </div>
		    </div>
		    <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6 mb-3">
		      <form:label path="userAccount.password" for="password">
		      	* <spring:message code="researcher.newPassword" />
		      </form:label>
		      <form:input path="userAccount.password" type="password" class="form-control" id="password" />
		      <div class="invalid-feedback">
        		<form:errors class="error" path="userAccount.password" />
      		  </div>
		    </div>
		  </div>
	
		</fieldset>
	
		<fieldset>
		
		<legend>
				<spring:message code="researcher.personal" />
		</legend>
			
		  <div class="form-row">
		    <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6 mb-3">
		      <form:label path="name" for="name">
		      	* <spring:message code="researcher.name" />
		      </form:label>
		      <form:input path="name" type="text" class="form-control" id="name" />
		      <div class="invalid-feedback">
        		<form:errors class="error" path="name" />
      		  </div>
		    </div>
		    <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6 mb-3">
		      <form:label path="surname" for="surname">
		      	* <spring:message code="researcher.surname" />
		      </form:label>
		      <form:input path="surname" type="text" class="form-control" id="surname" />
		      <div class="invalid-feedback">
        		<form:errors class="error" path="surname" />
      		  </div>
		    </div>
		  </div>
		  
		  <div class="form-row">
		    <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6 mb-3">
		      <form:label path="entity" for="entity">
		      	* <spring:message code="researcher.entity" />
		      </form:label>
		      <form:input path="entity" type="text" class="form-control" id="entity" />
		      <div class="invalid-feedback">
        		<form:errors class="error" path="entity" />
      		  </div>
		    </div>
		    <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6 mb-3">
		      <form:label path="phone" for="phone">
		      	* <spring:message code="researcher.phone" />
		      </form:label>
		      <form:input path="phone" type="text" class="form-control" id="phone" />
		      <div class="invalid-feedback">
        		<form:errors class="error" path="phone" />
      		  </div>
		    </div>
		  </div>
		  
		  <div class="form-row">
		    <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-4 mb-4">
		      <form:label path="email" for="email">
		      	* <spring:message code="researcher.email" />
		      </form:label>
		      <form:input path="email" type="text" class="form-control" id="email" />
		      <div class="invalid-feedback">
        		<form:errors class="error" path="email" />
      		  </div>
		    </div>
		  </div>
		  
	  	</fieldset>
	
		<div class="form-row">
	  		<div class="col-8 col-sm-6 col-md-4 col-lg-6 col-xl-6 mb-3">
		  		<input class="btn bg-dark text-white" type="submit" name="save"
				value="<spring:message code="researcher.save" />" />
		
				<input class="btn bg-dark text-white" type="button" name="cancel"
					value="<spring:message code="researcher.cancel" />"
					onclick="javascript: relativeRedir('welcome/index.do');" />
			</div>
	  	</div>
	
	</form:form>

</div>

