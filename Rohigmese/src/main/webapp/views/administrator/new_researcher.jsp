<%--
 * new_researcher.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<script>

$(document).ready(function(){
	var errors = $("[class='error']");
	
	if(errors.length>0){
		$("input[class='form-control']").addClass("is-valid");
		
		for (var i = 0; i < errors.length; i++) {
			var e = errors[i].id.split(".");
		  	
		  	if(e.length>2){
		  		e = e[1];
		  	}else{
		  		e = e[0];
		  	}
		  	
		  	$("input[id="+e+"]").removeClass("is-valid").addClass("is-invalid");
		  	
		}
	
	}
	 
	
});

</script>

<!-- ALERTS -->
<jstl:if test="${errorsAlert!=null}">
	<div id="alert" class="row justify-content-center">
		<div class="col-12">
			<div class="alert alert-danger text-center" role="alert">
				${errorsAlert}
			</div>
		</div>
	</div>
</jstl:if>

<jstl:if test="${usernameAlert!=null}">
	<div id="alert" class="row justify-content-center">
		<div class="col-12">
			<div class="alert alert-danger text-center" role="alert">
				${usernameAlert}
			</div>
		</div>
	</div>
</jstl:if>

<jstl:if test="${authAlert!=null}">
	<div id="alert" class="row justify-content-center">
		<div class="col-12">
			<div class="alert alert-danger text-center" role="alert">
				${authAlert}
			</div>
		</div>
	</div>
</jstl:if>

<!-- CODE -->
<div class="container justify-content-center py-1">

	<form:form class="needs-validation" action="researcher/administrator/register.do" modelAttribute="invitation" novalidate="novalidate">
	
		<form:hidden path="id" />
		<form:hidden path="version" />
		<form:hidden path="moment" />
		<form:hidden path="tempPassword" />
		<form:hidden path="accepted" />
		
		<br/>
		<div class="form-row justify-content-center">
			<p class="text-reset text-center"><spring:message code="administrator.researcherRegister" /></p>
		</div>
	
		<hr class="hr-style">
	
		<fieldset>
		
		<legend>
				<spring:message code="administrator.emailPersonal" />
		</legend>

		    <div class="form-row">
			    <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6 mb-3">
			      <label for="email">
			      	* <spring:message code="administrator.email" />
			      </label>
			      <input name="fromEmail" value="${adminEmail}" type="text" class="form-control text-left" id="email" />
			    </div>
			    <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6 mb-3">
			      <label for="password">
			      	* <spring:message code="administrator.password" />
			      </label>
			      <input name="fromPassword" type="password" class="form-control  text-left" id="password" />
			    </div>
			</div>
	
		</fieldset>
	
		<fieldset>
		
		<legend>
				<spring:message code="administrator.emailRecipient" />
		</legend>
			
			<div class="form-row">
			    <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6 mb-3">
					<form:label path="recipient" for="recipient">
				    * <spring:message code="administrator.researcher.email" />
				    </form:label>
				    <form:input path="recipient" type="text" class="form-control" id="recipient" />
				    <div class="invalid-feedback">
			      		<form:errors class="error" path="recipient" />
		     		</div>
			    </div>
			    <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6 mb-3">
			      <form:label path="username" for="username">
			      	* <spring:message code="administrator.researcher.username" />
			      </form:label>
			      <form:input path="username" type="text" class="form-control" id="username" />
			      <div class="invalid-feedback">
			      	<form:errors class="error" path="username" />
			      </div>
			    </div>
			</div>  
		  
	  	</fieldset>
	
		<div class="form-row">
	  		<div class="col-8 col-sm-6 col-md-4 col-lg-6 col-xl-6 mb-3">
		  		<input class="btn bg-dark text-white" type="submit" name="save"
				value="<spring:message code="administrator.save" />" />
		
				<input class="btn bg-dark text-white" type="button" name="cancel"
					value="<spring:message code="administrator.cancel" />"
					onclick="javascript: relativeRedir('welcome/index.do');" />
			</div>
	  	</div>
	
	</form:form>

</div>

