
package converters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import repositories.InvitationRepository;
import domain.Invitation;

@Component
@Transactional
public class StringToInvitationConverter implements Converter<String, Invitation> {

	@Autowired
	InvitationRepository	invitationRepository;


	@Override
	public Invitation convert(final String source) {

		final Invitation invitation;

		int id;

		if (StringUtils.isEmpty(source))

			invitation = null;

		else

			try {

				id = Integer.valueOf(source);

				invitation = this.invitationRepository.findOne(id);

			} catch (final Throwable throwable) {

				throw new IllegalArgumentException();

			}

		return invitation;

	}

}
