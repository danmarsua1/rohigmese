
package converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import domain.History;

@Component
@Transactional
public class HistoryToStringConverter implements Converter<History, String> {

	@Override
	public String convert(final History history) {

		String string;

		if (history == null)

			string = null;

		else

			string = String.valueOf(history.getId());

		return string;

	}

}
