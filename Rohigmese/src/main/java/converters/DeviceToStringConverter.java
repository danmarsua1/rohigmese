
package converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import domain.Device;

@Component
@Transactional
public class DeviceToStringConverter implements Converter<Device, String> {

	@Override
	public String convert(final Device device) {

		String string;

		if (device == null)

			string = null;

		else

			string = String.valueOf(device.getId());

		return string;

	}

}
