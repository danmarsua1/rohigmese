
package converters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import repositories.DeviceRepository;
import domain.Device;

@Component
@Transactional
public class StringToDeviceConverter implements Converter<String, Device> {

	@Autowired
	DeviceRepository	deviceRepository;


	@Override
	public Device convert(final String source) {

		final Device device;

		int id;

		if (StringUtils.isEmpty(source))

			device = null;

		else

			try {

				id = Integer.valueOf(source);

				device = this.deviceRepository.findOne(id);

			} catch (final Throwable throwable) {

				throw new IllegalArgumentException();

			}

		return device;

	}

}
