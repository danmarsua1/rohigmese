
package converters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import repositories.StationRepository;
import domain.Station;

@Component
@Transactional
public class StringToStationConverter implements Converter<String, Station> {

	@Autowired
	StationRepository	stationRepository;


	@Override
	public Station convert(final String source) {

		final Station station;

		int id;

		if (StringUtils.isEmpty(source))

			station = null;

		else

			try {

				id = Integer.valueOf(source);

				station = this.stationRepository.findOne(id);

			} catch (final Throwable throwable) {

				throw new IllegalArgumentException();

			}

		return station;

	}

}
