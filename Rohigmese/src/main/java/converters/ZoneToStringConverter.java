
package converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import domain.Zone;

@Component
@Transactional
public class ZoneToStringConverter implements Converter<Zone, String> {

	@Override
	public String convert(final Zone zone) {

		String string;

		if (zone == null)

			string = null;

		else

			string = String.valueOf(zone.getId());

		return string;

	}

}
