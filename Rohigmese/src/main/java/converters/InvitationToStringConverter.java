
package converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import domain.Invitation;

@Component
@Transactional
public class InvitationToStringConverter implements Converter<Invitation, String> {

	@Override
	public String convert(final Invitation invitation) {

		String string;

		if (invitation == null)

			string = null;

		else

			string = String.valueOf(invitation.getId());

		return string;

	}

}
