
package converters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import repositories.ResearcherRepository;
import domain.Researcher;

@Component
@Transactional
public class StringToResearcherConverter implements Converter<String, Researcher> {

	@Autowired
	ResearcherRepository	researcherRepository;


	@Override
	public Researcher convert(final String source) {

		final Researcher researcher;

		int id;

		if (StringUtils.isEmpty(source))

			researcher = null;

		else

			try {

				id = Integer.valueOf(source);

				researcher = this.researcherRepository.findOne(id);

			} catch (final Throwable throwable) {

				throw new IllegalArgumentException();

			}

		return researcher;

	}

}
