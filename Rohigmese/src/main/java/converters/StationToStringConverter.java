
package converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import domain.Station;

@Component
@Transactional
public class StationToStringConverter implements Converter<Station, String> {

	@Override
	public String convert(final Station station) {

		String string;

		if (station == null)

			string = null;

		else

			string = String.valueOf(station.getId());

		return string;

	}

}
