
package converters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import repositories.ZoneRepository;
import domain.Zone;

@Component
@Transactional
public class StringToZoneConverter implements Converter<String, Zone> {

	@Autowired
	ZoneRepository	zoneRepository;


	@Override
	public Zone convert(final String source) {

		final Zone zone;

		int id;

		if (StringUtils.isEmpty(source))

			zone = null;

		else

			try {

				id = Integer.valueOf(source);

				zone = this.zoneRepository.findOne(id);

			} catch (final Throwable throwable) {

				throw new IllegalArgumentException();

			}

		return zone;

	}

}
