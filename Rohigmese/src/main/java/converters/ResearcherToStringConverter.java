
package converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import domain.Researcher;

@Component
@Transactional
public class ResearcherToStringConverter implements Converter<Researcher, String> {

	@Override
	public String convert(final Researcher researcher) {

		String string;

		if (researcher == null)

			string = null;

		else

			string = String.valueOf(researcher.getId());

		return string;

	}

}
