
package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

@Entity
@Access(AccessType.PROPERTY)
public class Station extends DomainEntity {

	//Atributes--------------------------------------------------------------------------------
	private String	name;
	private Localization localization;
	private String authoritativeEntity;
	private String comments;
	
	//Getters methods
	@NotBlank
	public String getName() {
		return this.name;
	}
	
	@Valid
	public Localization getLocalization() {
		return this.localization;
	}
	
	@NotBlank
	public String getAuthoritativeEntity() {
		return this.authoritativeEntity;
	}
	
	public String getComments() {
		return this.comments;
	}

	//Setters methods
	public void setName(String name) {
		this.name = name;
	}

	public void setLocalization(Localization localization) {
		this.localization = localization;
	}

	public void setAuthoritativeEntity(String authoritativeEntity) {
		this.authoritativeEntity = authoritativeEntity;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}
	
	//Relationships
	
	private Zone zone;
	private Collection<Device> devices;
	
	@Valid
	@NotNull
	@ManyToOne(optional = false)
	public Zone getZone() {
		return this.zone;
	}

	public void setZone(Zone zone) {
		this.zone = zone;
	}
	
	@Valid
	@OneToMany(mappedBy = "station")
	public Collection<Device> getDevices() {
		return this.devices;
	}

	public void setDevices(Collection<Device> devices) {
		this.devices = devices;
	}

}