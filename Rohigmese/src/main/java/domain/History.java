
package domain;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Access(AccessType.PROPERTY)
public class History extends DomainEntity{

	//Atributes--------------------------------------------------------------------------------
	private Date date;
	private String author;
	private int	idStart;
	private int	idEnd;
	private String device;
	
	//Getters methods	
	@Past
	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "dd/MM/yy HH:mm")
	public Date getDate() {
		return this.date;
	}
	
	@NotBlank
	public String getAuthor() {
		return this.author;
	}

	@NotNull
	public int getIdStart() {
		return idStart;
	}
	
	@NotNull
	public int getIdEnd() {
		return idEnd;
	}
	
	public String getDevice() {
		return device;
	}

	//Setters methods
	public void setDate(Date date) {
		this.date = date;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public void setIdStart(int idStart) {
		this.idStart = idStart;
	}

	public void setIdEnd(int idEnd) {
		this.idEnd = idEnd;
	}
	
	public void setDevice(String device) {
		this.device = device;
	}

	//Relationships

}