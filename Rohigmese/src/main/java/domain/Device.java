
package domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.Valid;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import domain.dataTables.Helman.Helman;
import domain.dataTables.Helman.HelmanLisimeter;

@Entity
@Access(AccessType.PROPERTY)
public class Device extends DomainEntity {

	//Atributes--------------------------------------------------------------------------------
	private String	name;
	private Localization localization;
	private String description;
	private String brand;
	private double price;
	private boolean operative;
	private String comments;
	
	//Getters methods
	@NotBlank
	public String getName() {
		return this.name;
	}
	
	@Valid
	public Localization getLocalization() {
		return this.localization;
	}

	public String getDescription() {
		return this.description;
	}
	
	public String getBrand() {
		return brand;
	}
	
	@Digits(integer = 9, fraction = 2, message = "{master.page.priceError}")
	public double getPrice() {
		return price;
	}

	public boolean isOperative() {
		return operative;
	}

	public String getComments() {
		return this.comments;
	}

	//Setters methods
	public void setName(String name) {
		this.name = name;
	}

	public void setLocalization(Localization localization) {
		this.localization = localization;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public void setBrand(String brand) {
		this.brand = brand;
	}
	
	public void setPrice(double price) {
		this.price = price;
	}
	
	public void setOperative(boolean operative) {
		this.operative = operative;
	}
	
	public void setComments(String comments) {
		this.comments = comments;
	}
	
	//Relationships
	private Station station;
	
	@Valid
	@NotNull
	@ManyToOne(optional = false)
	public Station getStation() {
		return this.station;
	}

	public void setStation(Station station) {
		this.station = station;
	}

}