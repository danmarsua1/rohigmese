
package domain;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Access(AccessType.PROPERTY)
public class Invitation extends DomainEntity {

	//Atributes--------------------------------------------------------------------------------
	private Date 	moment;
	private String	recipient;
	private String 	username;
	private String 	tempPassword;
	private boolean accepted;
	
	//Getters methods
	@Past
	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "dd/MM/yy HH:mm")
	public Date getMoment() {
		return this.moment;
	}
	
	@NotBlank
	@Pattern(regexp = "^[\\w\\.\\-\\_]+\\@(:?\\w+\\.+\\w+)+$", message = "{master.page.errorEmail}")
	public String getRecipient() {
		return this.recipient;
	}
	
	@Size(min = 5, max = 32)
	@Column(unique = true)
	public String getUsername() {
		return this.username;
	}
	
	@NotBlank
	public String getTempPassword() {
		return this.tempPassword;
	}
	
	public boolean isAccepted() {
		return this.accepted;
	}

	//Setters methods
	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}
	
	public void setMoment(Date moment) {
		this.moment = moment;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setTempPassword(String tempPassword) {
		this.tempPassword = tempPassword;
	}
	
	public void setAccepted(final boolean accepted) {
		this.accepted = accepted;
	}

	//Relationships

}