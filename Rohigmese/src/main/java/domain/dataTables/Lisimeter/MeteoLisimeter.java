
package domain.dataTables.Lisimeter;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;

@Entity
@Access(AccessType.PROPERTY)
public class MeteoLisimeter extends Lisimeter {

	//Atributes--------------------------------------------------------------------------------
	private String moment;
	private String tLisi;
	private String tField;
	private String temp_m140;
	private String temp_m020;
	private String temp_m010;
	private String temp_m005;
	private String temp_005;
	private String temp_050;
	private String temp_200;
	private String rain;
	private String windSpeed;
	
	//Getters methods
	public String getMoment() {
		return this.moment;
	}
	
	public String gettLisi() {
		return tLisi;
	}

	public String gettField() {
		return tField;
	}

	public String getTemp_m140() {
		return temp_m140;
	}

	public String getTemp_m020() {
		return temp_m020;
	}

	public String getTemp_m010() {
		return temp_m010;
	}

	public String getTemp_m005() {
		return temp_m005;
	}

	public String getTemp_005() {
		return temp_005;
	}

	public String getTemp_050() {
		return temp_050;
	}

	public String getTemp_200() {
		return temp_200;
	}

	public String getRain() {
		return rain;
	}

	public String getWindSpeed() {
		return windSpeed;
	}
	
	//Setters methods
	public void setMoment(String moment) {
		this.moment = moment;
	}

	public void settLisi(String tLisi) {
		this.tLisi = tLisi;
	}

	public void settField(String tField) {
		this.tField = tField;
	}

	public void setTemp_m140(String temp_m140) {
		this.temp_m140 = temp_m140;
	}

	public void setTemp_m020(String temp_m020) {
		this.temp_m020 = temp_m020;
	}

	public void setTemp_m010(String temp_m010) {
		this.temp_m010 = temp_m010;
	}

	public void setTemp_m005(String temp_m005) {
		this.temp_m005 = temp_m005;
	}

	public void setTemp_005(String temp_005) {
		this.temp_005 = temp_005;
	}

	public void setTemp_050(String temp_050) {
		this.temp_050 = temp_050;
	}

	public void setTemp_200(String temp_200) {
		this.temp_200 = temp_200;
	}

	public void setRain(String rain) {
		this.rain = rain;
	}

	public void setWindSpeed(String windSpeed) {
		this.windSpeed = windSpeed;
	}



}