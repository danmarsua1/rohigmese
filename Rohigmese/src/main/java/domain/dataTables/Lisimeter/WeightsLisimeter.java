
package domain.dataTables.Lisimeter;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;

@Entity
@Access(AccessType.PROPERTY)
public class WeightsLisimeter extends Lisimeter {

	//Atributes--------------------------------------------------------------------------------
	private String moment;
	private String wPot;
	private String wLisi;
	
	//Getters methods
	public String getMoment() {
		return this.moment;
	}
	
	public String getwPot() {
		return wPot;
	}

	public String getwLisi() {
		return wLisi;
	}

	//Setters methods
	public void setMoment(String moment) {
		this.moment = moment;
	}

	public void setwPot(String wPot) {
		this.wPot = wPot;
	}

	public void setwLisi(String wLisi) {
		this.wLisi = wLisi;
	}
	
	

}