
package domain.dataTables.Davis;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;

@Entity
@Access(AccessType.PROPERTY)
public class DavisLisimeter extends Davis {

	//Atributes--------------------------------------------------------------------------------
	private String moment;
	private String temp;
	private String humidity;
	private String windSpeed;
	private String bar;
	private String rain;
	
	//Getters methods
	public String getMoment() {
		return this.moment;
	}
	
	public String getTemp() {
		return temp;
	}

	public String getHumidity() {
		return humidity;
	}

	public String getWindSpeed() {
		return windSpeed;
	}

	public String getBar() {
		return bar;
	}

	public String getRain() {
		return rain;
	}

	//Setters methods
	public void setMoment(String moment) {
		this.moment = moment;
	}
	
	public void setTemp(String temp) {
		this.temp = temp;
	}
	
	public void setHumidity(String humidity) {
		this.humidity = humidity;
	}
	
	public void setWindSpeed(String windSpeed) {
		this.windSpeed = windSpeed;
	}
	
	public void setBar(String bar) {
		this.bar = bar;
	}
	
	public void setRain(String rain) {
		this.rain = rain;
	}



}