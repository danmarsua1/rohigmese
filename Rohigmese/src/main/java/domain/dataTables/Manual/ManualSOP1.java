
package domain.dataTables.Manual;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;

@Entity
@Access(AccessType.PROPERTY)
public class ManualSOP1 extends Manual {

	//Atributes--------------------------------------------------------------------------------
	private String moment;
	private String level;
	
	//Getters methods
	public String getMoment() {
		return this.moment;
	}
	
	public String getLevel() {
		return this.level;
	}
	
	//Setters methods
	public void setMoment(String moment) {
		this.moment = moment;
	}

	public void setLevel(String level) {
		this.level = level;
	}


}