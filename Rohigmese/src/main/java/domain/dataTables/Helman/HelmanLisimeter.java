
package domain.dataTables.Helman;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;

@Entity
@Access(AccessType.PROPERTY)
public class HelmanLisimeter extends Helman {

	//Atributes--------------------------------------------------------------------------------
	private String moment;
	private String rain;
	
	//Getters methods
	public String getMoment() {
		return this.moment;
	}
	
	public String getRain() {
		return this.rain;
	}
	
	//Setters methods
	public void setMoment(String moment) {
		this.moment = moment;
	}

	public void setRain(String rain) {
		this.rain = rain;
	}
	
	//Relationships


}