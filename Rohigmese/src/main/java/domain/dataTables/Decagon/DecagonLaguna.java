
package domain.dataTables.Decagon;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;

@Entity
@Access(AccessType.PROPERTY)
public class DecagonLaguna extends Decagon {

	//Atributes--------------------------------------------------------------------------------
	private String moment;
	private String vwc_p1;
	private String temp_p1;
	private String ec_p1;
	private String vwc_p2;
	private String temp_p2;
	private String ec_p2;
	
	//Getters methods
	public String getMoment() {
		return this.moment;
	}
	
	public String getVwc_p1() {
		return vwc_p1;
	}

	public String getEc_p1() {
		return ec_p1;
	}

	public String getTemp_p1() {
		return temp_p1;
	}

	public String getVwc_p2() {
		return vwc_p2;
	}

	public String getEc_p2() {
		return ec_p2;
	}

	public String getTemp_p2() {
		return temp_p2;
	}

	//Setters methods
	public void setMoment(String moment) {
		this.moment = moment;
	}

	public void setVwc_p1(String vwc_p1) {
		this.vwc_p1 = vwc_p1;
	}

	public void setEc_p1(String ec_p1) {
		this.ec_p1 = ec_p1;
	}

	public void setTemp_p1(String temp_p1) {
		this.temp_p1 = temp_p1;
	}

	public void setVwc_p2(String vwc_p2) {
		this.vwc_p2 = vwc_p2;
	}

	public void setEc_p2(String ec_p2) {
		this.ec_p2 = ec_p2;
	}

	public void setTemp_p2(String temp_p2) {
		this.temp_p2 = temp_p2;
	}

}