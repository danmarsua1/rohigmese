
package domain.dataTables.Campbell;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;

@Entity
@Access(AccessType.PROPERTY)
public class CampbellC7DT2 extends Campbell {

	//Atributes--------------------------------------------------------------------------------
	private String moment;
	private String vwc_p1;
	private String ec_p1;
	private String temp_p1;
	private String vwc_p2;
	private String ec_p2;
	private String temp_p2;
	private String vwc_p3;
	private String ec_p3;
	private String temp_p3;
	private String vwc_p4;
	private String ec_p4;
	private String temp_p4;
	private String vwc_p5;
	private String ec_p5;
	private String temp_p5;
	private String vwc_p6;
	private String ec_p6;
	private String temp_p6;
	
	//Getters methods
	public String getMoment() {
		return this.moment;
	}
	
	public String getVwc_p1() {
		return vwc_p1;
	}

	public String getEc_p1() {
		return ec_p1;
	}

	public String getTemp_p1() {
		return temp_p1;
	}

	public String getVwc_p2() {
		return vwc_p2;
	}

	public String getEc_p2() {
		return ec_p2;
	}

	public String getTemp_p2() {
		return temp_p2;
	}

	public String getVwc_p3() {
		return vwc_p3;
	}

	public String getEc_p3() {
		return ec_p3;
	}

	public String getTemp_p3() {
		return temp_p3;
	}

	public String getVwc_p4() {
		return vwc_p4;
	}

	public String getEc_p4() {
		return ec_p4;
	}

	public String getTemp_p4() {
		return temp_p4;
	}

	public String getVwc_p5() {
		return vwc_p5;
	}

	public String getEc_p5() {
		return ec_p5;
	}

	public String getTemp_p5() {
		return temp_p5;
	}

	public String getVwc_p6() {
		return vwc_p6;
	}

	public String getEc_p6() {
		return ec_p6;
	}

	public String getTemp_p6() {
		return temp_p6;
	}

	//Setters methods
	public void setMoment(String moment) {
		this.moment = moment;
	}

	public void setVwc_p1(String vwc_p1) {
		this.vwc_p1 = vwc_p1;
	}

	public void setEc_p1(String ec_p1) {
		this.ec_p1 = ec_p1;
	}

	public void setTemp_p1(String temp_p1) {
		this.temp_p1 = temp_p1;
	}

	public void setVwc_p2(String vwc_p2) {
		this.vwc_p2 = vwc_p2;
	}

	public void setEc_p2(String ec_p2) {
		this.ec_p2 = ec_p2;
	}

	public void setTemp_p2(String temp_p2) {
		this.temp_p2 = temp_p2;
	}

	public void setVwc_p3(String vwc_p3) {
		this.vwc_p3 = vwc_p3;
	}

	public void setEc_p3(String ec_p3) {
		this.ec_p3 = ec_p3;
	}

	public void setTemp_p3(String temp_p3) {
		this.temp_p3 = temp_p3;
	}

	public void setVwc_p4(String vwc_p4) {
		this.vwc_p4 = vwc_p4;
	}

	public void setEc_p4(String ec_p4) {
		this.ec_p4 = ec_p4;
	}

	public void setTemp_p4(String temp_p4) {
		this.temp_p4 = temp_p4;
	}

	public void setVwc_p5(String vwc_p5) {
		this.vwc_p5 = vwc_p5;
	}

	public void setEc_p5(String ec_p5) {
		this.ec_p5 = ec_p5;
	}

	public void setTemp_p5(String temp_p5) {
		this.temp_p5 = temp_p5;
	}

	public void setVwc_p6(String vwc_p6) {
		this.vwc_p6 = vwc_p6;
	}

	public void setEc_p6(String ec_p6) {
		this.ec_p6 = ec_p6;
	}

	public void setTemp_p6(String temp_p6) {
		this.temp_p6 = temp_p6;
	}


}