
package domain.dataTables.Campbell;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToOne;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import domain.Device;

@Entity
@Access(AccessType.PROPERTY)
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Campbell{

	//Atributes--------------------------------------------------------------------------------
	private int rid;
		
	//Getters methods
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	public int getRid() {
		return this.rid;
	}

	//Setters methods
	public void setRid(int rid) {
		this.rid = rid;
	}
	
	//Relationships
	private Device device;
	
	@Valid
	@NotNull
	@OneToOne(optional = true)
	public Device getDevice() {
		return this.device;
	}

	public void setDevice(Device device) {
		this.device = device;
	}

}