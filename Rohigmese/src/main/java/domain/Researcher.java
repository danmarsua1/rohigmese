
package domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.validation.Valid;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;

@Entity
@Access(AccessType.PROPERTY)
public class Researcher extends Actor {

	//Atributes
	private String	entity;
	private String	phone;
	
	//Getters
	@NotBlank
	public String getEntity() {
		return this.entity;
	}
	
	@Pattern(regexp = "^\\+\\d{2,} \\d{9}$", message = "{master.page.errorPhone}")
	public String getPhone() {
		return this.phone;
	}
	
	//Setters
	public void setPhone(final String phone) {
		this.phone = phone;
	}
	
	public void setEntity(final String entity) {
		this.entity = entity;
	}
	
	//Relationships---------------------------------
	private Invitation	invitation;


	@Valid
	@OneToOne(optional = false)
	public Invitation getInvitation() {
		return this.invitation;
	}

	public void setInvitation(final Invitation invitation) {
		this.invitation = invitation;
	}


}
