
package domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Embeddable;
import javax.validation.constraints.Digits;

@Embeddable
@Access(AccessType.PROPERTY)
public class Localization{

	//Atributes--------------------------------------------------------------------------------
	private double	latitude;
	private double	longitude;
	
	//Getters methods
	@Digits(integer = 3, fraction = 20, message = "{master.page.latitudeError}")
	public double getLatitude() {
		return this.latitude;
	}
	
	@Digits(integer = 3, fraction = 20, message = "{master.page.longitudeError}")
	public double getLongitude() {
		return this.longitude;
	}

	//Setters methods
	public void setLatitude(final double latitude) {
		this.latitude = latitude;
	}
	
	public void setLongitude(final double longitude) {
		this.longitude = longitude;
	}
	
	//Relationships

}