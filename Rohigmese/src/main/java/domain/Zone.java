
package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.Valid;

import org.hibernate.validator.constraints.NotBlank;

@Entity
@Access(AccessType.PROPERTY)
public class Zone extends DomainEntity {

	//Atributes--------------------------------------------------------------------------------
	private String	name;
	private Localization localization;
	private String type;
	private String comments;
	
	//Getters methods
	@NotBlank
	public String getName() {
		return this.name;
	}
	
	@Valid
	public Localization getLocalization() {
		return this.localization;
	}
	
	@NotBlank
	public String getType() {
		return this.type;
	}
	
	public String getComments() {
		return this.comments;
	}

	//Setters methods
	public void setName(String name) {
		this.name = name;
	}

	public void setLocalization(Localization localization) {
		this.localization = localization;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}
	
	//Relationships
	private Collection<Station> stations;
	
	@Valid
	@OneToMany(mappedBy = "zone")
	public Collection<Station> getStations() {
		return this.stations;
	}

	public void setStations(Collection<Station> stations) {
		this.stations = stations;
	}

}