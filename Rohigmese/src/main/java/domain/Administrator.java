
package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.Valid;

@Entity
@Access(AccessType.PROPERTY)
public class Administrator extends Actor {

	//Atributes
	

	//Relationships
	private Collection<Invitation> invitations;
	
	@Valid
	@OneToMany
	public Collection<Invitation> getInvitations() {
		return this.invitations;
	}

	public void setInvitations(Collection<Invitation> invitations) {
		this.invitations = invitations;
	}
	
	

}
