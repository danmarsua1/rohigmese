/*
 * WelcomeController.java
 * 
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import domain.Actor;
import domain.Zone;
import services.ActorService;
import services.ZoneService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.mysql.cj.xdevapi.JsonString;

@Controller
@RequestMapping("/welcome")
public class WelcomeController extends AbstractController {
	
	//Services
	
	@Autowired
	private ActorService actorService;
	@Autowired
	private ZoneService zoneService;

	// Constructors -----------------------------------------------------------

	public WelcomeController() {
		super();
	}

	// Index ------------------------------------------------------------------		

	@RequestMapping(value = "/index")
	public ModelAndView index(final HttpServletRequest request, @ModelAttribute("invitationSuccess") final String invitationSuccess) {
		ModelAndView result;
		SimpleDateFormat formatter;
		String moment;

		formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		moment = formatter.format(new Date());

		result = new ModelAndView("welcome/index");
		
		//Interceptor de cambio de idioma
		final LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(request);
		
		//Mostrar nombre y apellidos del usuario cuando inicie sesi�n
		Actor user = null;

		try {
			user = this.actorService.findByPrincipal();
			result.addObject("name", user);
		} catch (final IllegalArgumentException e) {
			if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage()))
				result.addObject("name", "Guest");
			else
				result.addObject("name", "Invitado");
		}
		
		result.addObject("moment", moment);
		
		result.addObject("currentPage", "Index");
		
		if(invitationSuccess!="") {
			result.addObject("invitationSuccess", invitationSuccess);
		}
		
		//Markers
		
		Collection<Zone> zones = this.zoneService.findAll();
		result.addObject("zones", zones);

		return result;
	}
}
