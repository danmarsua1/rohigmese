/*
 * UserController.java
 * 
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.ImageIcon;
import javax.swing.event.ListSelectionEvent;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.github.rcaller.rstuff.RCaller;
import com.github.rcaller.rstuff.RCode;

import controllers.researcher.DeviceResearcherController;
import domain.Device;
import domain.Station;
import domain.Zone;
import domain.dataTables.Helman.HelmanLisimeter;
import forms.ProjectConfiguration;
import services.CampbellService;
import services.DavisService;
import services.DecagonService;
import services.DeviceService;
import services.HelmanService;
import services.LisimeterService;
import services.ManualService;
import services.StationService;
import services.ZoneService;

@Controller
@RequestMapping("/device")
public class DeviceController extends AbstractController {

	// Services--------------------------------------------------------------------------
	@Autowired
	private DeviceService deviceService;
	@Autowired
	private StationService stationService;
	@Autowired
	private ZoneService zoneService;
	@Autowired
	private HelmanService helmanService;
	@Autowired
	private DavisService davisService;
	@Autowired
	private CampbellService campbellService;
	@Autowired
	private LisimeterService lisimeterService;
	@Autowired
	private DecagonService decagonService;
	@Autowired
	private ManualService manualService;

	// Constructors -----------------------------------------------------------

	public DeviceController() {
		super();
	}

	// Download data
	@RequestMapping(value = "/downloadData", method = RequestMethod.GET)
	public ModelAndView downloadData(final int deviceId, final HttpServletRequest request, HttpServletResponse response,
			final RedirectAttributes redirect) throws IOException {
		ModelAndView modelAndView;

		final Device device = this.deviceService.findOne(deviceId);
		final Station station = this.stationService.findOne(device.getStation().getId());
		final Zone zone = this.zoneService.findOne(station.getZone().getId());

		modelAndView = new ModelAndView("device/viewData");

		final int BUFFER_SIZE = 4096;

		File root = new File(ProjectConfiguration.ROOTPATHRSCRIPTS);

		String filename = "";
		File devicePath = null;
		File fullPath = null;

		if (zone.getName().equals("LISIMETRO")) {
			File zonePath = new File(root, "Lisimetro");

			switch (device.getName()) {
			case "Meteo-Lisímetro":
				filename = "EXPORTED_LISIMETRO_METEO.csv";
				devicePath = new File(zonePath, "Lisimetro/METEO/OUTPUT/" + filename);
				fullPath = new File(devicePath.getAbsolutePath());
				break;
			case "Pesos-Lisímetro":
				filename = "EXPORTED_LISIMETRO_WEIGHTS.csv";
				devicePath = new File(zonePath, "Lisimetro/WEIGHTS/OUTPUT/" + filename);
				fullPath = new File(devicePath.getAbsolutePath());
				break;
			case "Sensores Campbell":
				filename = "EXPORTED_LISIMETRO_CAMPBELL.csv";
				devicePath = new File(zonePath, "Campbell/OUTPUT/" + filename);
				fullPath = new File(devicePath.getAbsolutePath());
				break;
			case "Pluviómetro digital":
				filename = "EXPORTED_LISIMETRO_DAVIS.txt";
				devicePath = new File(zonePath, "Davis/OUTPUT/" + filename);
				fullPath = new File(devicePath.getAbsolutePath());
				break;
			case "Pluviómetro de pesada":
				System.out.println("ENTRO helman");
				filename = "EXPORTED_LISIMETRO_HELMAN.txt";
				devicePath = new File(zonePath, "Helman/OUTPUT/" + filename);
				fullPath = new File(devicePath.getAbsolutePath());
				break;
			}

		}

		if (zone.getName().equals("MARISMILLAS")) {
			File zonePath = new File(root, "Marismillas");

			if (station.getName().equals("Cortafuegos 7")) {

				switch (device.getName()) {
				case "Datalogger 1":
					filename = "EXPORTED_MARISMILLAS_CAMPBELL_C7_DT1.csv";
					devicePath = new File(zonePath, "Campbell/C7 DT1/OUTPUT/" + filename);
					fullPath = new File(devicePath.getAbsolutePath());
					break;
				case "Datalogger 2":
					filename = "EXPORTED_MARISMILLAS_CAMPBELL_C7_DT2.csv";
					devicePath = new File(zonePath, "Campbell/C7 DT2/OUTPUT/" + filename);
					fullPath = new File(devicePath.getAbsolutePath());
					break;
				case "Pluviómetro digital":
					filename = "EXPORTED_MARISMILLAS_DAVIS_C7.txt";
					devicePath = new File(zonePath, "Davis/C7/OUTPUT/" + filename);
					fullPath = new File(devicePath.getAbsolutePath());
					break;
				}

			}

			if (station.getName().equals("Llanos")) {

				switch (device.getName()) {
				case "Pluviómetro digital":
					filename = "EXPORTED_MARISMILLAS_DAVIS_LLANOS.txt";
					devicePath = new File(zonePath, "Davis/LLANOS/OUTPUT/" + filename);
					fullPath = new File(devicePath.getAbsolutePath());
					break;
				case "Sensores Decagon":
					filename = "EXPORTED_MARISMILLAS_DECAGON_LLANOS.csv";
					devicePath = new File(zonePath, "Decagon/LLANOS/OUTPUT/" + filename);
					fullPath = new File(devicePath.getAbsolutePath());
					break;
				}

			}

			if (station.getName().equals("Laguna")) {

				switch (device.getName()) {
				case "Sensores Decagon":
					filename = "EXPORTED_MARISMILLAS_DECAGON_LAGUNA.csv";
					devicePath = new File(zonePath, "Decagon/LAGUNA/OUTPUT/" + filename);
					fullPath = new File(devicePath.getAbsolutePath());
					break;
				}

			}

			if (station.getName().equals("Palacio")) {

				switch (device.getName()) {
				case "Sensores Decagon":
					filename = "EXPORTED_MARISMILLAS_DECAGON_PALACIO.csv";
					devicePath = new File(zonePath, "Decagon/PALACIO/OUTPUT/" + filename);
					fullPath = new File(devicePath.getAbsolutePath());
					break;
				}

			}

		}

		if (zone.getName().equals("SANTA OLALLA")) {
			File zonePath = new File(root, "SantaOlalla");

			if (station.getName().equals("SO-IGME")) {

				switch (device.getName()) {
				case "Pluviómetro digital":
					filename = "EXPORTED_SANTA_OLALLA_DAVIS.txt";
					devicePath = new File(zonePath, "Davis/OUTPUT/" + filename);
					fullPath = new File(devicePath.getAbsolutePath());
					break;
				}

			}

			if (station.getName().equals("SO-P2")) {

				switch (device.getName()) {
				case "Sensores Campbell":
					filename = "EXPORTED_SANTA_OLALLA_CAMPBELL_P2.csv";
					devicePath = new File(zonePath, "Campbell/P2/OUTPUT/" + filename);
					fullPath = new File(devicePath.getAbsolutePath());
					break;
				}

			}

			if (station.getName().equals("SO-P3")) {

				switch (device.getName()) {
				case "Sensores Campbell":
					filename = "EXPORTED_SANTA_OLALLA_CAMPBELL_P3.csv";
					devicePath = new File(zonePath, "Campbell/P3/OUTPUT/" + filename);
					fullPath = new File(devicePath.getAbsolutePath());
					break;
				}

			}

			if (station.getName().equals("SO-P1")) {

				switch (device.getName()) {
				case "Nivel freático":
					filename = "EXPORTED_SANTA_OLALLA_NIVEL_P1.csv";
					devicePath = new File(zonePath, "P1/OUTPUT/" + filename);
					fullPath = new File(devicePath.getAbsolutePath());
					break;
				}

			}

		}

		try {

			File downloadFile = new File(fullPath.getAbsolutePath());
			@SuppressWarnings("resource")
			FileInputStream inputStream = new FileInputStream(downloadFile);

			// MIME type of the file
			String mimeType = "text/plain";

			// set content attributes for the response
			response.setContentType(mimeType);
			response.setContentLength((int) downloadFile.length());

			// set headers for the response
			String headerKey = "Content-Disposition";
			String headerValue = String.format("attachment; filename=\"%s\"", downloadFile.getName());
			response.setHeader(headerKey, headerValue);

			// get output stream of the response
			OutputStream outStream = response.getOutputStream();

			byte[] buffer = new byte[BUFFER_SIZE];
			int bytesRead = -1;

			// write bytes read from the input stream into the output stream
			while ((bytesRead = inputStream.read(buffer)) != -1) {
				outStream.write(buffer, 0, bytesRead);
			}

		} catch (FileNotFoundException e) {

			final String notFoundError;

			// Interceptor de cambio de idioma
			final LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(request);

			if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				notFoundError = "The selected file does not exist. Try to download another.";
			} else {
				notFoundError = "No existe el fichero seleccionado. Intente descargar otro.";
			}

			redirect.addFlashAttribute("notFoundError", notFoundError);

			modelAndView = new ModelAndView("redirect:/device/station/list.do?stationId=" + station.getId());

		} catch (Exception e) {

		}

		return modelAndView;

	}

	// Graph data
	@RequestMapping(value = "/graphData", method = RequestMethod.GET)
	public ModelAndView graphData(final int deviceId, final HttpServletRequest request) {

		final ModelAndView modelAndView;

		final Device device = this.deviceService.findOne(deviceId);
		final Station station = this.stationService.findOne(device.getStation().getId());
		final Zone zone = this.zoneService.findOne(station.getZone().getId());

		modelAndView = new ModelAndView("device/viewData");

		modelAndView.addObject("device", device);
		modelAndView.addObject("station", station);

		Collection<Object> deviceData = null;

		String stationName = StringUtils.stripAccents(station.getName());

		String scriptPath = "";
		String plot = "";
		String imgPath = "";

		String deviceName = "";

		String AppPlotsRoot = ProjectConfiguration.APPPLOTSROOT;

		// LISIMETRO

		if (stationName.equals("Lisimetro")) {

			switch (device.getId()) {
			case 59:
				deviceData = new ArrayList<>(this.lisimeterService.getMeteoData());
				
				if (deviceData.size() > 0) {

					File root = new File(ProjectConfiguration.ROOTPATHRSCRIPTS);

					scriptPath = "Lisimetro/Lisimetro/METEO";
					plot = "Lisimetro_Meteo_Plot.R";

					deviceName = "lisimeterMeteo";

					scriptPath = new File(root, scriptPath).getAbsolutePath();
					File rPlot = new File(scriptPath, plot);
					File iPlot = new File(scriptPath, deviceName + "_1.png");
					File iPlot2 = new File(scriptPath, deviceName + "_2.png");
					File iPlot3 = new File(scriptPath, deviceName + "_3.png");
					File iPlot4 = new File(scriptPath, deviceName + "_4.png");

					String fullPath = rPlot.getAbsolutePath();
					String fullPlotPath = iPlot.getAbsolutePath();
					String fullPlotPath2 = iPlot2.getAbsolutePath();
					String fullPlotPath3 = iPlot3.getAbsolutePath();
					String fullPlotPath4 = iPlot4.getAbsolutePath();


					// Ejecucion de R-Script
					try {

						RCaller rcaller = RCaller.create();
						RCode code = RCode.create();

						rcaller.setRCode(code);

						code.addRCode("library(openxlsx)");
						code.addRCode("library(tidyr)");
						code.addRCode("library(dplyr)");
						code.addRCode("library(DescTools)");
						code.addRCode("library(ggplot2)");
						code.addRCode("library(rstudioapi)");
						code.addRCode("library(lubridate)");
						code.addRCode("library(scales)");
						code.addRCode("library(png)");
						code.addRCode("library(zoo)");
						code.addRCode("library(cowplot)");
						code.addRCode("library(grid)");
						
						scriptPath = scriptPath.replace("\\", "/");
						fullPath = fullPath.replace("\\", "/");

						code.addRCode("setwd('" + scriptPath + "')");
						code.addRCode("source('" + fullPath + "')");

						code.addRCode("png(\"lisimeterMeteo_1.png\", width = 1280)");
						code.addRCode("print(graph_1)");
						code.addRCode("dev.off()");
						
						code.addRCode("png(\"lisimeterMeteo_2.png\", width = 1280)");
						code.addRCode("print(graph_2)");
						code.addRCode("dev.off()");
						
						code.addRCode("png(\"lisimeterMeteo_3.png\", width = 1280)");
						code.addRCode("print(graph_3)");
						code.addRCode("dev.off()");
						
						code.addRCode("png(\"lisimeterMeteo_4.png\", width = 1280)");
						code.addRCode("print(graph_4)");
						code.addRCode("dev.off()");

						rcaller.runOnly();

						rcaller.deleteTempFiles();

						Files.copy(Paths.get(fullPlotPath), Paths.get(AppPlotsRoot + "/" + deviceName + "_1.png"),
								StandardCopyOption.REPLACE_EXISTING);
						Files.copy(Paths.get(fullPlotPath2), Paths.get(AppPlotsRoot + "/" + deviceName + "_2.png"),
								StandardCopyOption.REPLACE_EXISTING);
						Files.copy(Paths.get(fullPlotPath3), Paths.get(AppPlotsRoot + "/" + deviceName + "_3.png"),
								StandardCopyOption.REPLACE_EXISTING);
						Files.copy(Paths.get(fullPlotPath4), Paths.get(AppPlotsRoot + "/" + deviceName + "_4.png"),
								StandardCopyOption.REPLACE_EXISTING);
						
						iPlot.delete();iPlot2.delete();iPlot3.delete();iPlot4.delete();

						imgPath = "images/plots/" + deviceName + ".png";

					} catch (Exception e) {
						System.out.println(e.toString());
					}

				}
				
				break;
			case 60:
				deviceData = new ArrayList<>(this.lisimeterService.getWeightsData());
				
				if (deviceData.size() > 0) {

					File root = new File(ProjectConfiguration.ROOTPATHRSCRIPTS);

					scriptPath = "Lisimetro/Lisimetro/WEIGHTS";
					plot = "Lisimetro_Weights_Plot.R";

					deviceName = "lisimeterWeights";

					scriptPath = new File(root, scriptPath).getAbsolutePath();
					File rPlot = new File(scriptPath, plot);
					File iPlot = new File(scriptPath, deviceName + ".png");

					String fullPath = rPlot.getAbsolutePath();
					String fullPlotPath = iPlot.getAbsolutePath();


					// Ejecucion de R-Script
					try {

						RCaller rcaller = RCaller.create();
						RCode code = RCode.create();

						rcaller.setRCode(code);

						code.addRCode("library(openxlsx)");
						code.addRCode("library(tidyr)");
						code.addRCode("library(dplyr)");
						code.addRCode("library(DescTools)");
						code.addRCode("library(ggplot2)");
						code.addRCode("library(rstudioapi)");
						code.addRCode("library(lubridate)");
						code.addRCode("library(scales)");
						code.addRCode("library(png)");
						code.addRCode("library(cowplot)");
						code.addRCode("library(zoo)");
						
						scriptPath = scriptPath.replace("\\", "/");
						fullPath = fullPath.replace("\\", "/");

						code.addRCode("setwd('" + scriptPath + "')");
						code.addRCode("source('" + fullPath + "')");

						code.addRCode("png(\"lisimeterWeights.png\", width = 1280)");
						code.addRCode("print(graph)");
						code.addRCode("dev.off()");

						rcaller.runOnly();

						rcaller.deleteTempFiles();

						Files.copy(Paths.get(fullPlotPath), Paths.get(AppPlotsRoot + "/" + deviceName + ".png"),
								StandardCopyOption.REPLACE_EXISTING);
						
						iPlot.delete();

						imgPath = "images/plots/" + deviceName + ".png";

					} catch (Exception e) {
						System.out.println(e.toString());
					}

				}
				
				break;
			case 61:
				deviceData = new ArrayList<>(this.campbellService.getLisimeterData());
				
				if (deviceData.size() > 0) {

					File root = new File(ProjectConfiguration.ROOTPATHRSCRIPTS);

					scriptPath = "Lisimetro/Campbell";
					plot = "Lisimetro_Campbell_Plot.R";

					deviceName = "lisimeterCampbell";

					scriptPath = new File(root, scriptPath).getAbsolutePath();
					File rPlot = new File(scriptPath, plot);
					File iPlot = new File(scriptPath, deviceName + ".png");

					String fullPath = rPlot.getAbsolutePath();
					String fullPlotPath = iPlot.getAbsolutePath();


					// Ejecucion de R-Script
					try {

						RCaller rcaller = RCaller.create();
						RCode code = RCode.create();

						rcaller.setRCode(code);

						code.addRCode("library(openxlsx)");
						code.addRCode("library(tidyr)");
						code.addRCode("library(dplyr)");
						code.addRCode("library(DescTools)");
						code.addRCode("library(ggplot2)");
						code.addRCode("library(rstudioapi)");
						code.addRCode("library(lubridate)");
						code.addRCode("library(scales)");
						code.addRCode("library(png)");
						code.addRCode("library(cowplot)");
						code.addRCode("library(zoo)");
						
						scriptPath = scriptPath.replace("\\", "/");
						fullPath = fullPath.replace("\\", "/");

						code.addRCode("setwd('" + scriptPath + "')");
						code.addRCode("source('" + fullPath + "')");

						code.addRCode("png(\"lisimeterCampbell.png\", width = 1280)");
						code.addRCode("print(graph)");
						code.addRCode("dev.off()");

						rcaller.runOnly();

						rcaller.deleteTempFiles();

						Files.copy(Paths.get(fullPlotPath), Paths.get(AppPlotsRoot + "/" + deviceName + ".png"),
								StandardCopyOption.REPLACE_EXISTING);
						
						iPlot.delete();

						imgPath = "images/plots/" + deviceName + ".png";

					} catch (Exception e) {
						System.out.println(e.toString());
					}

				}
				
				break;
			case 62:
				deviceData = new ArrayList<>(this.davisService.getLisimeterData());
				
				if (deviceData.size() > 0) {

					File root = new File(ProjectConfiguration.ROOTPATHRSCRIPTS);

					scriptPath = "Lisimetro/Davis";
					plot = "Lisimetro_Davis_Plot.R";

					deviceName = "lisimeterDavis";

					scriptPath = new File(root, scriptPath).getAbsolutePath();
					File rPlot = new File(scriptPath, plot);
					File iPlot = new File(scriptPath, deviceName + ".png");

					String fullPath = rPlot.getAbsolutePath();
					String fullPlotPath = iPlot.getAbsolutePath();


					// Ejecucion de R-Script
					try {

						RCaller rcaller = RCaller.create();
						RCode code = RCode.create();

						rcaller.setRCode(code);

						code.addRCode("library(openxlsx)");
						code.addRCode("library(tidyr)");
						code.addRCode("library(dplyr)");
						code.addRCode("library(DescTools)");
						code.addRCode("library(ggplot2)");
						code.addRCode("library(rstudioapi)");
						code.addRCode("library(lubridate)");
						code.addRCode("library(scales)");
						code.addRCode("library(png)");
						code.addRCode("library(cowplot)");
						code.addRCode("library(zoo)");
						
						scriptPath = scriptPath.replace("\\", "/");
						fullPath = fullPath.replace("\\", "/");

						code.addRCode("setwd('" + scriptPath + "')");
						code.addRCode("source('" + fullPath + "')");

						code.addRCode("png(\"lisimeterDavis.png\", width = 1280)");
						code.addRCode("print(graph)");
						code.addRCode("dev.off()");

						rcaller.runOnly();

						rcaller.deleteTempFiles();

						Files.copy(Paths.get(fullPlotPath), Paths.get(AppPlotsRoot + "/" + deviceName + ".png"),
								StandardCopyOption.REPLACE_EXISTING);
						
						iPlot.delete();

						imgPath = "images/plots/" + deviceName + ".png";

					} catch (Exception e) {
						System.out.println(e.toString());
					}

				}
				
				break;
			case 63:
				deviceData = new ArrayList<>(this.helmanService.getAllData());

				if (deviceData.size() > 0) {

					File root = new File(ProjectConfiguration.ROOTPATHRSCRIPTS);

					scriptPath = "Lisimetro/Helman";
					plot = "Lisimetro_Helman_Plot.R";

					deviceName = "lisimeterHelman";

					scriptPath = new File(root, scriptPath).getAbsolutePath();
					File rPlot = new File(scriptPath, plot);
					File iPlot = new File(scriptPath, deviceName + ".png");

					String fullPath = rPlot.getAbsolutePath();
					String fullPlotPath = iPlot.getAbsolutePath();


					// Ejecucion de R-Script
					try {

						RCaller rcaller = RCaller.create();
						RCode code = RCode.create();

						rcaller.setRCode(code);

						code.addRCode("library(openxlsx)");
						code.addRCode("library(tidyr)");
						code.addRCode("library(dplyr)");
						code.addRCode("library(DescTools)");
						code.addRCode("library(ggplot2)");
						code.addRCode("library(rstudioapi)");
						code.addRCode("library(lubridate)");
						code.addRCode("library(scales)");
						code.addRCode("library(png)");
						
						scriptPath = scriptPath.replace("\\", "/");
						fullPath = fullPath.replace("\\", "/");

						code.addRCode("setwd('" + scriptPath + "')");
						code.addRCode("source('" + fullPath + "')");

						code.addRCode("png(\"lisimeterHelman.png\", width = 1280)");
						code.addRCode("print(graph)");
						code.addRCode("dev.off()");

						rcaller.runOnly();

						rcaller.deleteTempFiles();

						Files.copy(Paths.get(fullPlotPath), Paths.get(AppPlotsRoot + "/" + deviceName + ".png"),
								StandardCopyOption.REPLACE_EXISTING);
						
						iPlot.delete();

						imgPath = "images/plots/" + deviceName + ".png";

					} catch (Exception e) {
						System.out.println(e.toString());
					}

				}

				break;
			}

		}

		// MARISMILLAS

		if (station.getName().equals("Cortafuegos 7")) {

			switch (device.getId()) {
			case 64:
				deviceData = new ArrayList<>(this.campbellService.getC7DT1Data());
				
				if (deviceData.size() > 0) {

					File root = new File(ProjectConfiguration.ROOTPATHRSCRIPTS);

					scriptPath = "Marismillas/Campbell/C7 DT1";
					plot = "C7_DT1_Campbell_Plot.R";

					deviceName = "c7dt1Campbell";

					scriptPath = new File(root, scriptPath).getAbsolutePath();
					File rPlot = new File(scriptPath, plot);
					File iPlot = new File(scriptPath, deviceName + ".png");

					String fullPath = rPlot.getAbsolutePath();
					String fullPlotPath = iPlot.getAbsolutePath();


					// Ejecucion de R-Script
					try {

						RCaller rcaller = RCaller.create();
						RCode code = RCode.create();

						rcaller.setRCode(code);

						code.addRCode("library(openxlsx)");
						code.addRCode("library(tidyr)");
						code.addRCode("library(dplyr)");
						code.addRCode("library(DescTools)");
						code.addRCode("library(ggplot2)");
						code.addRCode("library(rstudioapi)");
						code.addRCode("library(lubridate)");
						code.addRCode("library(scales)");
						code.addRCode("library(png)");
						code.addRCode("library(cowplot)");
						code.addRCode("library(zoo)");
						
						scriptPath = scriptPath.replace("\\", "/");
						fullPath = fullPath.replace("\\", "/");

						code.addRCode("setwd('" + scriptPath + "')");
						code.addRCode("source('" + fullPath + "')");

						code.addRCode("png(\"c7dt1Campbell.png\", width = 1280)");
						code.addRCode("print(graph)");
						code.addRCode("dev.off()");

						rcaller.runOnly();

						rcaller.deleteTempFiles();

						Files.copy(Paths.get(fullPlotPath), Paths.get(AppPlotsRoot + "/" + deviceName + ".png"),
								StandardCopyOption.REPLACE_EXISTING);
						
						iPlot.delete();

						imgPath = "images/plots/" + deviceName + ".png";

					} catch (Exception e) {
						System.out.println(e.toString());
					}

				}
				
				break;
			case 65:
				deviceData = new ArrayList<>(this.campbellService.getC7DT2Data());
				
				if (deviceData.size() > 0) {

					File root = new File(ProjectConfiguration.ROOTPATHRSCRIPTS);

					scriptPath = "Marismillas/Campbell/C7 DT2";
					plot = "C7_DT2_Campbell_Plot.R";

					deviceName = "c7dt2Campbell";

					scriptPath = new File(root, scriptPath).getAbsolutePath();
					File rPlot = new File(scriptPath, plot);
					File iPlot = new File(scriptPath, deviceName + ".png");

					String fullPath = rPlot.getAbsolutePath();
					String fullPlotPath = iPlot.getAbsolutePath();


					// Ejecucion de R-Script
					try {

						RCaller rcaller = RCaller.create();
						RCode code = RCode.create();

						rcaller.setRCode(code);

						code.addRCode("library(openxlsx)");
						code.addRCode("library(tidyr)");
						code.addRCode("library(dplyr)");
						code.addRCode("library(DescTools)");
						code.addRCode("library(ggplot2)");
						code.addRCode("library(rstudioapi)");
						code.addRCode("library(lubridate)");
						code.addRCode("library(scales)");
						code.addRCode("library(png)");
						code.addRCode("library(cowplot)");
						code.addRCode("library(zoo)");
						
						scriptPath = scriptPath.replace("\\", "/");
						fullPath = fullPath.replace("\\", "/");

						code.addRCode("setwd('" + scriptPath + "')");
						code.addRCode("source('" + fullPath + "')");

						code.addRCode("png(\"c7dt2Campbell.png\", width = 1280)");
						code.addRCode("print(graph)");
						code.addRCode("dev.off()");

						rcaller.runOnly();

						rcaller.deleteTempFiles();

						Files.copy(Paths.get(fullPlotPath), Paths.get(AppPlotsRoot + "/" + deviceName + ".png"),
								StandardCopyOption.REPLACE_EXISTING);
						
						iPlot.delete();

						imgPath = "images/plots/" + deviceName + ".png";

					} catch (Exception e) {
						System.out.println(e.toString());
					}

				}
				
				break;
			case 66:
				deviceData = new ArrayList<>(this.davisService.getC7Data());
				
				if (deviceData.size() > 0) {

					File root = new File(ProjectConfiguration.ROOTPATHRSCRIPTS);

					scriptPath = "Marismillas/Davis/C7";
					plot = "C7_Davis_Plot.R";

					deviceName = "c7Davis";

					scriptPath = new File(root, scriptPath).getAbsolutePath();
					File rPlot = new File(scriptPath, plot);
					File iPlot = new File(scriptPath, deviceName + ".png");

					String fullPath = rPlot.getAbsolutePath();
					String fullPlotPath = iPlot.getAbsolutePath();


					// Ejecucion de R-Script
					try {

						RCaller rcaller = RCaller.create();
						RCode code = RCode.create();

						rcaller.setRCode(code);

						code.addRCode("library(openxlsx)");
						code.addRCode("library(tidyr)");
						code.addRCode("library(dplyr)");
						code.addRCode("library(DescTools)");
						code.addRCode("library(ggplot2)");
						code.addRCode("library(rstudioapi)");
						code.addRCode("library(lubridate)");
						code.addRCode("library(scales)");
						code.addRCode("library(png)");
						code.addRCode("library(cowplot)");
						code.addRCode("library(zoo)");
						
						scriptPath = scriptPath.replace("\\", "/");
						fullPath = fullPath.replace("\\", "/");

						code.addRCode("setwd('" + scriptPath + "')");
						code.addRCode("source('" + fullPath + "')");

						code.addRCode("png(\"c7Davis.png\", width = 1280)");
						code.addRCode("print(graph)");
						code.addRCode("dev.off()");

						rcaller.runOnly();

						rcaller.deleteTempFiles();

						Files.copy(Paths.get(fullPlotPath), Paths.get(AppPlotsRoot + "/" + deviceName + ".png"),
								StandardCopyOption.REPLACE_EXISTING);
						
						iPlot.delete();

						imgPath = "images/plots/" + deviceName + ".png";

					} catch (Exception e) {
						System.out.println(e.toString());
					}

				}
				
				break;
			}

		}

		if (station.getName().equals("Llanos")) {

			switch (device.getId()) {
			case 67:
				deviceData = new ArrayList<>(this.davisService.getLlanosData());
				
				if (deviceData.size() > 0) {

					File root = new File(ProjectConfiguration.ROOTPATHRSCRIPTS);

					scriptPath = "Marismillas/Davis/LLANOS";
					plot = "Llanos_Davis_Plot.R";

					deviceName = "llanosDavis";

					scriptPath = new File(root, scriptPath).getAbsolutePath();
					File rPlot = new File(scriptPath, plot);
					File iPlot = new File(scriptPath, deviceName + ".png");

					String fullPath = rPlot.getAbsolutePath();
					String fullPlotPath = iPlot.getAbsolutePath();


					// Ejecucion de R-Script
					try {

						RCaller rcaller = RCaller.create();
						RCode code = RCode.create();

						rcaller.setRCode(code);

						code.addRCode("library(openxlsx)");
						code.addRCode("library(tidyr)");
						code.addRCode("library(dplyr)");
						code.addRCode("library(DescTools)");
						code.addRCode("library(ggplot2)");
						code.addRCode("library(rstudioapi)");
						code.addRCode("library(lubridate)");
						code.addRCode("library(scales)");
						code.addRCode("library(png)");
						code.addRCode("library(cowplot)");
						code.addRCode("library(zoo)");
						
						scriptPath = scriptPath.replace("\\", "/");
						fullPath = fullPath.replace("\\", "/");

						code.addRCode("setwd('" + scriptPath + "')");
						code.addRCode("source('" + fullPath + "')");

						code.addRCode("png(\"llanosDavis.png\", width = 1280)");
						code.addRCode("print(graph)");
						code.addRCode("dev.off()");

						rcaller.runOnly();

						rcaller.deleteTempFiles();

						Files.copy(Paths.get(fullPlotPath), Paths.get(AppPlotsRoot + "/" + deviceName + ".png"),
								StandardCopyOption.REPLACE_EXISTING);
						
						iPlot.delete();

						imgPath = "images/plots/" + deviceName + ".png";

					} catch (Exception e) {
						System.out.println(e.toString());
					}

				}
				
				break;
			case 68:
				deviceData = new ArrayList<>(this.decagonService.getLlanosData());
				
				if (deviceData.size() > 0) {

					File root = new File(ProjectConfiguration.ROOTPATHRSCRIPTS);

					scriptPath = "Marismillas/Decagon/LLANOS";
					plot = "Llanos_Decagon_Plot.R";

					deviceName = "llanosDecagon";

					scriptPath = new File(root, scriptPath).getAbsolutePath();
					File rPlot = new File(scriptPath, plot);
					File iPlot = new File(scriptPath, deviceName + ".png");

					String fullPath = rPlot.getAbsolutePath();
					String fullPlotPath = iPlot.getAbsolutePath();


					// Ejecucion de R-Script
					try {

						RCaller rcaller = RCaller.create();
						RCode code = RCode.create();

						rcaller.setRCode(code);

						code.addRCode("library(openxlsx)");
						code.addRCode("library(tidyr)");
						code.addRCode("library(dplyr)");
						code.addRCode("library(DescTools)");
						code.addRCode("library(ggplot2)");
						code.addRCode("library(rstudioapi)");
						code.addRCode("library(lubridate)");
						code.addRCode("library(scales)");
						code.addRCode("library(png)");
						code.addRCode("library(cowplot)");
						code.addRCode("library(zoo)");
						
						scriptPath = scriptPath.replace("\\", "/");
						fullPath = fullPath.replace("\\", "/");

						code.addRCode("setwd('" + scriptPath + "')");
						code.addRCode("source('" + fullPath + "')");

						code.addRCode("png(\"llanosDecagon.png\", width = 1280)");
						code.addRCode("print(graph)");
						code.addRCode("dev.off()");

						rcaller.runOnly();

						rcaller.deleteTempFiles();

						Files.copy(Paths.get(fullPlotPath), Paths.get(AppPlotsRoot + "/" + deviceName + ".png"),
								StandardCopyOption.REPLACE_EXISTING);
						
						iPlot.delete();

						imgPath = "images/plots/" + deviceName + ".png";

					} catch (Exception e) {
						System.out.println(e.toString());
					}

				}
				
				break;
			}

		}

		if (station.getName().equals("Laguna")) {

			switch (device.getId()) {
			case 69:
				deviceData = new ArrayList<>(this.decagonService.getLagunaData());
				
				if (deviceData.size() > 0) {

					File root = new File(ProjectConfiguration.ROOTPATHRSCRIPTS);

					scriptPath = "Marismillas/Decagon/LAGUNA";
					plot = "Laguna_Decagon_Plot.R";

					deviceName = "lagunaDecagon";

					scriptPath = new File(root, scriptPath).getAbsolutePath();
					File rPlot = new File(scriptPath, plot);
					File iPlot = new File(scriptPath, deviceName + ".png");

					String fullPath = rPlot.getAbsolutePath();
					String fullPlotPath = iPlot.getAbsolutePath();


					// Ejecucion de R-Script
					try {

						RCaller rcaller = RCaller.create();
						RCode code = RCode.create();

						rcaller.setRCode(code);

						code.addRCode("library(openxlsx)");
						code.addRCode("library(tidyr)");
						code.addRCode("library(dplyr)");
						code.addRCode("library(DescTools)");
						code.addRCode("library(ggplot2)");
						code.addRCode("library(rstudioapi)");
						code.addRCode("library(lubridate)");
						code.addRCode("library(scales)");
						code.addRCode("library(png)");
						code.addRCode("library(cowplot)");
						code.addRCode("library(zoo)");
						
						scriptPath = scriptPath.replace("\\", "/");
						fullPath = fullPath.replace("\\", "/");

						code.addRCode("setwd('" + scriptPath + "')");
						code.addRCode("source('" + fullPath + "')");

						code.addRCode("png(\"lagunaDecagon.png\", width = 1280)");
						code.addRCode("print(graph)");
						code.addRCode("dev.off()");

						rcaller.runOnly();

						rcaller.deleteTempFiles();

						Files.copy(Paths.get(fullPlotPath), Paths.get(AppPlotsRoot + "/" + deviceName + ".png"),
								StandardCopyOption.REPLACE_EXISTING);
						
						iPlot.delete();

						imgPath = "images/plots/" + deviceName + ".png";

					} catch (Exception e) {
						System.out.println(e.toString());
					}

				}
				
				break;
			}

		}

		if (station.getName().equals("Palacio")) {

			switch (device.getId()) {
			case 70:
				deviceData = new ArrayList<>(this.decagonService.getPalacioData());
				
				if (deviceData.size() > 0) {

					File root = new File(ProjectConfiguration.ROOTPATHRSCRIPTS);

					scriptPath = "Marismillas/Decagon/PALACIO";
					plot = "Palacio_Decagon_Plot.R";

					deviceName = "palacioDecagon";

					scriptPath = new File(root, scriptPath).getAbsolutePath();
					File rPlot = new File(scriptPath, plot);
					File iPlot = new File(scriptPath, deviceName + ".png");

					String fullPath = rPlot.getAbsolutePath();
					String fullPlotPath = iPlot.getAbsolutePath();


					// Ejecucion de R-Script
					try {

						RCaller rcaller = RCaller.create();
						RCode code = RCode.create();

						rcaller.setRCode(code);

						code.addRCode("library(openxlsx)");
						code.addRCode("library(tidyr)");
						code.addRCode("library(dplyr)");
						code.addRCode("library(DescTools)");
						code.addRCode("library(ggplot2)");
						code.addRCode("library(rstudioapi)");
						code.addRCode("library(lubridate)");
						code.addRCode("library(scales)");
						code.addRCode("library(png)");
						code.addRCode("library(cowplot)");
						code.addRCode("library(zoo)");
						
						scriptPath = scriptPath.replace("\\", "/");
						fullPath = fullPath.replace("\\", "/");

						code.addRCode("setwd('" + scriptPath + "')");
						code.addRCode("source('" + fullPath + "')");

						code.addRCode("png(\"palacioDecagon.png\", width = 1280)");
						code.addRCode("print(graph)");
						code.addRCode("dev.off()");

						rcaller.runOnly();

						rcaller.deleteTempFiles();

						Files.copy(Paths.get(fullPlotPath), Paths.get(AppPlotsRoot + "/" + deviceName + ".png"),
								StandardCopyOption.REPLACE_EXISTING);
						
						iPlot.delete();

						imgPath = "images/plots/" + deviceName + ".png";

					} catch (Exception e) {
						System.out.println(e.toString());
					}

				}
				
				break;
			}

		}

		// SANTA-OLALLA

		if (station.getName().equals("SO-IGME")) {

			switch (device.getId()) {
			case 71:
				deviceData = new ArrayList<>(this.davisService.getSOData());
				
				if (deviceData.size() > 0) {

					File root = new File(ProjectConfiguration.ROOTPATHRSCRIPTS);

					scriptPath = "SantaOlalla/Davis";
					plot = "SO_Davis_Plot.R";

					deviceName = "SODavis";

					scriptPath = new File(root, scriptPath).getAbsolutePath();
					File rPlot = new File(scriptPath, plot);
					File iPlot = new File(scriptPath, deviceName + ".png");

					String fullPath = rPlot.getAbsolutePath();
					String fullPlotPath = iPlot.getAbsolutePath();


					// Ejecucion de R-Script
					try {

						RCaller rcaller = RCaller.create();
						RCode code = RCode.create();

						rcaller.setRCode(code);

						code.addRCode("library(openxlsx)");
						code.addRCode("library(tidyr)");
						code.addRCode("library(dplyr)");
						code.addRCode("library(DescTools)");
						code.addRCode("library(ggplot2)");
						code.addRCode("library(rstudioapi)");
						code.addRCode("library(lubridate)");
						code.addRCode("library(scales)");
						code.addRCode("library(png)");
						code.addRCode("library(cowplot)");
						code.addRCode("library(zoo)");
						
						scriptPath = scriptPath.replace("\\", "/");
						fullPath = fullPath.replace("\\", "/");

						code.addRCode("setwd('" + scriptPath + "')");
						code.addRCode("source('" + fullPath + "')");

						code.addRCode("png(\"SODavis.png\", width = 1280)");
						code.addRCode("print(graph)");
						code.addRCode("dev.off()");

						rcaller.runOnly();

						rcaller.deleteTempFiles();

						Files.copy(Paths.get(fullPlotPath), Paths.get(AppPlotsRoot + "/" + deviceName + ".png"),
								StandardCopyOption.REPLACE_EXISTING);
						
						iPlot.delete();

						imgPath = "images/plots/" + deviceName + ".png";

					} catch (Exception e) {
						System.out.println(e.toString());
					}

				}
				
				break;
			}

		}

		if (station.getName().equals("SO-P2")) {

			switch (device.getId()) {
			case 72:
				deviceData = new ArrayList<>(this.campbellService.getSOP2Data());
				
				if (deviceData.size() > 0) {

					File root = new File(ProjectConfiguration.ROOTPATHRSCRIPTS);

					scriptPath = "SantaOlalla/Campbell/P2";
					plot = "SO_P2_Campbell_Plot.R";

					deviceName = "SOP2Campbell";

					scriptPath = new File(root, scriptPath).getAbsolutePath();
					File rPlot = new File(scriptPath, plot);
					File iPlot = new File(scriptPath, deviceName + ".png");

					String fullPath = rPlot.getAbsolutePath();
					String fullPlotPath = iPlot.getAbsolutePath();


					// Ejecucion de R-Script
					try {

						RCaller rcaller = RCaller.create();
						RCode code = RCode.create();

						rcaller.setRCode(code);

						code.addRCode("library(openxlsx)");
						code.addRCode("library(tidyr)");
						code.addRCode("library(dplyr)");
						code.addRCode("library(DescTools)");
						code.addRCode("library(ggplot2)");
						code.addRCode("library(rstudioapi)");
						code.addRCode("library(lubridate)");
						code.addRCode("library(scales)");
						code.addRCode("library(png)");
						code.addRCode("library(cowplot)");
						code.addRCode("library(zoo)");
						
						scriptPath = scriptPath.replace("\\", "/");
						fullPath = fullPath.replace("\\", "/");

						code.addRCode("setwd('" + scriptPath + "')");
						code.addRCode("source('" + fullPath + "')");

						code.addRCode("png(\"SOP2Campbell.png\", width = 1280)");
						code.addRCode("print(graph)");
						code.addRCode("dev.off()");

						rcaller.runOnly();

						rcaller.deleteTempFiles();

						Files.copy(Paths.get(fullPlotPath), Paths.get(AppPlotsRoot + "/" + deviceName + ".png"),
								StandardCopyOption.REPLACE_EXISTING);
						
						iPlot.delete();

						imgPath = "images/plots/" + deviceName + ".png";

					} catch (Exception e) {
						System.out.println(e.toString());
					}

				}
				
				break;
			}

		}

		if (station.getName().equals("SO-P3")) {

			switch (device.getId()) {
			case 73:
				deviceData = new ArrayList<>(this.campbellService.getSOP3Data());
				
				if (deviceData.size() > 0) {

					File root = new File(ProjectConfiguration.ROOTPATHRSCRIPTS);

					scriptPath = "SantaOlalla/Campbell/P3";
					plot = "SO_P3_Campbell_Plot.R";

					deviceName = "SOP3Campbell";

					scriptPath = new File(root, scriptPath).getAbsolutePath();
					File rPlot = new File(scriptPath, plot);
					File iPlot = new File(scriptPath, deviceName + ".png");

					String fullPath = rPlot.getAbsolutePath();
					String fullPlotPath = iPlot.getAbsolutePath();


					// Ejecucion de R-Script
					try {

						RCaller rcaller = RCaller.create();
						RCode code = RCode.create();

						rcaller.setRCode(code);

						code.addRCode("library(openxlsx)");
						code.addRCode("library(tidyr)");
						code.addRCode("library(dplyr)");
						code.addRCode("library(DescTools)");
						code.addRCode("library(ggplot2)");
						code.addRCode("library(rstudioapi)");
						code.addRCode("library(lubridate)");
						code.addRCode("library(scales)");
						code.addRCode("library(png)");
						code.addRCode("library(cowplot)");
						code.addRCode("library(zoo)");
						
						scriptPath = scriptPath.replace("\\", "/");
						fullPath = fullPath.replace("\\", "/");

						code.addRCode("setwd('" + scriptPath + "')");
						code.addRCode("source('" + fullPath + "')");

						code.addRCode("png(\"SOP3Campbell.png\", width = 1280)");
						code.addRCode("print(graph)");
						code.addRCode("dev.off()");

						rcaller.runOnly();

						rcaller.deleteTempFiles();

						Files.copy(Paths.get(fullPlotPath), Paths.get(AppPlotsRoot + "/" + deviceName + ".png"),
								StandardCopyOption.REPLACE_EXISTING);
						
						iPlot.delete();

						imgPath = "images/plots/" + deviceName + ".png";

					} catch (Exception e) {
						System.out.println(e.toString());
					}

				}
				
				break;
			}

		}

		if (station.getName().equals("SO-P1")) {

			switch (device.getId()) {
			case 74:
				deviceData = new ArrayList<>(this.manualService.getP1Data());
				
				if (deviceData.size() > 0) {

					File root = new File(ProjectConfiguration.ROOTPATHRSCRIPTS);

					scriptPath = "SantaOlalla/P1";
					plot = "SO_P1_Plot.R";

					deviceName = "SOP1";

					scriptPath = new File(root, scriptPath).getAbsolutePath();
					File rPlot = new File(scriptPath, plot);
					File iPlot = new File(scriptPath, deviceName + ".png");

					String fullPath = rPlot.getAbsolutePath();
					String fullPlotPath = iPlot.getAbsolutePath();


					// Ejecucion de R-Script
					try {

						RCaller rcaller = RCaller.create();
						RCode code = RCode.create();

						rcaller.setRCode(code);

						code.addRCode("library(openxlsx)");
						code.addRCode("library(tidyr)");
						code.addRCode("library(dplyr)");
						code.addRCode("library(DescTools)");
						code.addRCode("library(ggplot2)");
						code.addRCode("library(rstudioapi)");
						code.addRCode("library(lubridate)");
						code.addRCode("library(scales)");
						code.addRCode("library(png)");
						code.addRCode("library(cowplot)");
						code.addRCode("library(zoo)");
						
						scriptPath = scriptPath.replace("\\", "/");
						fullPath = fullPath.replace("\\", "/");
						
						code.addRCode("setwd('" + scriptPath + "')");
						code.addRCode("source('" + fullPath + "')");

						code.addRCode("png(\"SOP1.png\", width = 1280)");
						code.addRCode("print(graph)");
						code.addRCode("dev.off()");

						rcaller.runOnly();

						rcaller.deleteTempFiles();

						Files.copy(Paths.get(fullPlotPath), Paths.get(AppPlotsRoot + "/" + deviceName + ".png"),
								StandardCopyOption.REPLACE_EXISTING);
						
						iPlot.delete();

						imgPath = "images/plots/" + deviceName + ".png";

					} catch (Exception e) {
						System.out.println(e.toString());
					}

				}
				
				break;
			}

		}

		// OBTENER GRAFICA DATOS
		modelAndView.addObject("imgPath", imgPath);

		modelAndView.addObject("device", device);
		modelAndView.addObject("deviceData", deviceData);
		modelAndView.addObject("requestURI", "/device/viewData.do?deviceId=" + device.getId());

		if (device.getBrand() != null) {
			this.updateBreadCrumb(modelAndView,
					"Zone: " + zone.getName() + ", Station: " + station.getName() + ", Device: " + device.getName()
							+ " [" + device.getBrand() + "]",
					"Zona: " + zone.getName() + ", Estación: " + station.getName() + ", Equipamiento: "
							+ device.getName() + " [" + device.getBrand() + "]",
					"View data", "Datos", request);
		} else {
			this.updateBreadCrumb(modelAndView,
					"Zone: " + zone.getName() + ", Station: " + station.getName() + ", Device: " + device.getName(),
					"Zona: " + zone.getName() + ", Estación: " + station.getName() + ", Equipamiento: "
							+ device.getName(),
					"View data", "Datos", request);
		}

		return modelAndView;

	}

	// View device data
	@RequestMapping(value = "/viewData", method = RequestMethod.GET)
	public ModelAndView viewData(final int deviceId, @ModelAttribute("fileSuccess") String fileSuccess,
			@ModelAttribute("fileError") String fileError, @ModelAttribute("deleteSuccess") String deleteSuccess,
			@ModelAttribute("deleteError") String deleteError, final HttpServletRequest request) {

		final ModelAndView modelAndView;

		final Device device = this.deviceService.findOne(deviceId);
		final Station station = this.stationService.findOne(device.getStation().getId());
		final Zone zone = this.zoneService.findOne(station.getZone().getId());

		modelAndView = new ModelAndView("device/viewData");

		if (fileSuccess != "") {
			modelAndView.addObject("fileSuccess", fileSuccess);
		}

		if (fileError != "") {
			modelAndView.addObject("fileError", fileError);
		}

		if (deleteSuccess != "") {
			modelAndView.addObject("deleteSuccess", deleteSuccess);
		}

		if (deleteError != "") {
			modelAndView.addObject("deleteError", deleteError);
		}

		modelAndView.addObject("device", device);
		modelAndView.addObject("station", station);

		Collection<Object> deviceData = null;

		String stationName = StringUtils.stripAccents(station.getName());

		File plot = null;
		String deviceName = "";
		String AppPlotsRoot = ProjectConfiguration.APPPLOTSROOT;
		
		FileInputStream input = null;

		try {

			// LISIMETRO

			if (stationName.equals("Lisimetro")) {

				switch (device.getId()) {
				case 59:
					deviceData = new ArrayList<>(this.lisimeterService.getMeteoData());
					deviceName = "lisimeterMeteo";
					plot = new File(AppPlotsRoot + "/" + deviceName + "_1.png");
					@SuppressWarnings("resource")
					FileInputStream input59 = new FileInputStream(plot);
					break;
				case 60:
					deviceData = new ArrayList<>(this.lisimeterService.getWeightsData());
					deviceName = "lisimeterWeights";
					plot = new File(AppPlotsRoot + "/" + deviceName + ".png");
					@SuppressWarnings("resource")
					FileInputStream input60 = new FileInputStream(plot);
					break;
				case 61:
					deviceData = new ArrayList<>(this.campbellService.getLisimeterData());
					deviceName = "lisimeterCampbell";
					plot = new File(AppPlotsRoot + "/" + deviceName + ".png");
					@SuppressWarnings("resource")
					FileInputStream input61 = new FileInputStream(plot);
					break;
				case 62:
					deviceData = new ArrayList<>(this.davisService.getLisimeterData());
					deviceName = "lisimeterDavis";
					plot = new File(AppPlotsRoot + "/" + deviceName + ".png");
					@SuppressWarnings("resource")
					FileInputStream input62 = new FileInputStream(plot);
					break;
				case 63:
					deviceData = new ArrayList<>(this.helmanService.getAllData());
					deviceName = "lisimeterHelman";
					plot = new File(AppPlotsRoot + "/" + deviceName + ".png");
					@SuppressWarnings("resource")
					FileInputStream input63 = new FileInputStream(plot);
					break;
				}

			}

			// MARISMILLAS

			if (station.getName().equals("Cortafuegos 7")) {

				switch (device.getId()) {
				case 64:
					deviceData = new ArrayList<>(this.campbellService.getC7DT1Data());
					deviceName = "c7dt1Campbell";
					plot = new File(AppPlotsRoot + "/" + deviceName + ".png");
					@SuppressWarnings("resource")
					FileInputStream input64 = new FileInputStream(plot);
					break;
				case 65:
					deviceData = new ArrayList<>(this.campbellService.getC7DT2Data());
					deviceName = "c7dt2Campbell";
					plot = new File(AppPlotsRoot + "/" + deviceName + ".png");
					@SuppressWarnings("resource")
					FileInputStream input65 = new FileInputStream(plot);
					break;
				case 66:
					deviceData = new ArrayList<>(this.davisService.getC7Data());
					deviceName = "c7Davis";
					plot = new File(AppPlotsRoot + "/" + deviceName + ".png");
					@SuppressWarnings("resource")
					FileInputStream input66 = new FileInputStream(plot);
					break;
				}

			}

			if (station.getName().equals("Llanos")) {

				switch (device.getId()) {
				case 67:
					deviceData = new ArrayList<>(this.davisService.getLlanosData());
					deviceName = "llanosDavis";
					plot = new File(AppPlotsRoot + "/" + deviceName + ".png");
					@SuppressWarnings("resource")
					FileInputStream input67 = new FileInputStream(plot);
					break;
				case 68:
					deviceData = new ArrayList<>(this.decagonService.getLlanosData());
					deviceName = "llanosDecagon";
					plot = new File(AppPlotsRoot + "/" + deviceName + ".png");
					@SuppressWarnings("resource")
					FileInputStream input68 = new FileInputStream(plot);
					break;
				}

			}

			if (station.getName().equals("Laguna")) {

				switch (device.getId()) {
				case 69:
					deviceData = new ArrayList<>(this.decagonService.getLagunaData());
					deviceName = "lagunaDecagon";
					plot = new File(AppPlotsRoot + "/" + deviceName + ".png");
					@SuppressWarnings("resource")
					FileInputStream input69 = new FileInputStream(plot);
					break;
				}

			}

			if (station.getName().equals("Palacio")) {

				switch (device.getId()) {
				case 70:
					deviceData = new ArrayList<>(this.decagonService.getPalacioData());
					deviceName = "palacioDecagon";
					plot = new File(AppPlotsRoot + "/" + deviceName + ".png");
					@SuppressWarnings("resource")
					FileInputStream input70 = new FileInputStream(plot);
					break;
				}

			}

			// SANTA-OLALLA

			if (station.getName().equals("SO-IGME")) {

				switch (device.getId()) {
				case 71:
					deviceData = new ArrayList<>(this.davisService.getSOData());
					deviceName = "SODavis";
					plot = new File(AppPlotsRoot + "/" + deviceName + ".png");
					@SuppressWarnings("resource")
					FileInputStream input71 = new FileInputStream(plot);
					break;
				}

			}

			if (station.getName().equals("SO-P2")) {

				switch (device.getId()) {
				case 72:
					deviceData = new ArrayList<>(this.campbellService.getSOP2Data());
					deviceName = "SOP2Campbell";
					plot = new File(AppPlotsRoot + "/" + deviceName + ".png");
					@SuppressWarnings("resource")
					FileInputStream input72 = new FileInputStream(plot);
					break;
				}

			}

			if (station.getName().equals("SO-P3")) {

				switch (device.getId()) {
				case 73:
					deviceData = new ArrayList<>(this.campbellService.getSOP3Data());
					deviceName = "SOP3Campbell";
					plot = new File(AppPlotsRoot + "/" + deviceName + ".png");
					@SuppressWarnings("resource")
					FileInputStream input73 = new FileInputStream(plot);
					break;
				}

			}

			if (station.getName().equals("SO-P1")) {

				switch (device.getId()) {
				case 74:
					deviceData = new ArrayList<>(this.manualService.getP1Data());
					deviceName = "SOP1";
					plot = new File(AppPlotsRoot + "/" + deviceName + ".png");
					@SuppressWarnings("resource")
					FileInputStream input74 = new FileInputStream(plot);
					break;
				}

			}

			if(deviceName!="") {
				modelAndView.addObject("imgPath", "images/plots/" + deviceName + ".png");
			}else {
				modelAndView.addObject("imgPath", "");
			}
			

		} catch (FileNotFoundException e) {
			modelAndView.addObject("imgPath", "");
		}

		modelAndView.addObject("device", device);
		modelAndView.addObject("deviceData", deviceData);
		modelAndView.addObject("requestURI", "/device/viewData.do?deviceId=" + device.getId());

		if (device.getBrand() != null) {
			this.updateBreadCrumb(modelAndView,
					"Zone: " + zone.getName() + ", Station: " + station.getName() + ", Device: " + device.getName()
							+ " [" + device.getBrand() + "]",
					"Zona: " + zone.getName() + ", Estación: " + station.getName() + ", Equipamiento: "
							+ device.getName() + " [" + device.getBrand() + "]",
					"View data", "Datos", request);
		} else {
			this.updateBreadCrumb(modelAndView,
					"Zone: " + zone.getName() + ", Station: " + station.getName() + ", Device: " + device.getName(),
					"Zona: " + zone.getName() + ", Estación: " + station.getName() + ", Equipamiento: "
							+ device.getName(),
					"View data", "Datos", request);
		}

		return modelAndView;
	}

	// Ancillary methods
	// --------------------------------------------------------------

	protected void updateBreadCrumb(ModelAndView mv, String parentEN, String parentES, String currentPageEN,
			String currentPageES, final HttpServletRequest request) {

		final LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(request);

		if (parentEN != null && parentES != null && currentPageEN != null && currentPageES != null) {
			if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				mv.addObject("parent", parentEN);
				mv.addObject("currentPage", currentPageEN);
			} else {
				mv.addObject("parent", parentES);
				mv.addObject("currentPage", currentPageES);
			}
		} else if (parentEN == null || parentES == null) {

			if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				mv.addObject("currentPage", currentPageEN);
			} else {
				mv.addObject("currentPage", currentPageES);
			}

		} else {

			mv.addObject("currentPage", "Index");

		}

	}

}
