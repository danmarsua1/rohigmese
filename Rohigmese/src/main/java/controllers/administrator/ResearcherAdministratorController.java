/*
 * ResearcherAdministratorController.java
 * 
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.administrator;

import java.util.Date;
import java.util.Locale;
import java.util.Properties;

import javax.mail.AuthenticationFailedException;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import controllers.AbstractController;
import domain.Administrator;
import domain.Invitation;
import services.AdministratorService;
import services.InvitationService;

@Controller
@RequestMapping("/researcher/administrator")
public class ResearcherAdministratorController extends AbstractController {
	
	//Services--------------------------------------------------------------------------
	@Autowired
	private AdministratorService	administratorService;
	@Autowired
	private InvitationService	invitationService;

	// Constructors -----------------------------------------------------------

	public ResearcherAdministratorController() {
		super();
	}
	
	//Creating--------------------------------------------------------------------------
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create(final HttpServletRequest request) {
		ModelAndView modelAndView;

		final Invitation invitation = this.invitationService.create();
		modelAndView = this.createRegisterModelAndView(invitation);
		
		final Administrator administrator = this.administratorService.findByPrincipal();
		modelAndView.addObject("adminEmail", administrator.getEmail());
		
		this.updateBreadCrumb(modelAndView, "Administrator","Administrador","New researcher","Nuevo investigador",request);

		return modelAndView;
	}
	
	//Register-------------------------------------------------------------------------
	@RequestMapping(value = "/register", method = RequestMethod.POST, params = "save")
	public ModelAndView register(@Valid final Invitation invitation, final BindingResult bindingResult, @RequestParam final String fromEmail, @RequestParam final String fromPassword,final HttpServletRequest request, final RedirectAttributes redirect) {
		ModelAndView modelAndView;
		final Administrator administrator = this.administratorService.findByPrincipal();
		
		//Interceptor de cambio de idioma
		final LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(request);

		if (bindingResult.hasErrors() || fromEmail==null || fromPassword==null) {
			modelAndView = this.createRegisterModelAndView(invitation);
			
			modelAndView.addObject("adminEmail", administrator.getEmail());
			
			final String errorsAlert;
			
			if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				errorsAlert = "(*)  Obligatory fields";
			}else {
				errorsAlert = "(*)  Campos obligatorios";
			}
			
			modelAndView.addObject("errorsAlert", errorsAlert);
			
			this.updateBreadCrumb(modelAndView, "Administrator","Administrador","New researcher","Nuevo investigador",request);
			
		}else {
			
			if(!this.invitationService.isResearcherUnique(invitation.getUsername(),invitation.getRecipient())) {
				
				modelAndView = this.createRegisterModelAndView(invitation);
				modelAndView.addObject("adminEmail", administrator.getEmail());
				
				final String usernameAlert;
				
				if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
					usernameAlert = "A researcher with the assigned username/e-mail has already registered or invited. Try a different one.";
				}else {
					usernameAlert = "Ya se ha registrado o invitado un investigador con el username/correo asignado. Pruebe otro distinto.";
				}
				
				modelAndView.addObject("usernameAlert", usernameAlert);
				
				this.updateBreadCrumb(modelAndView, "Administrator","Administrador","New researcher","Nuevo investigador",request);
				
			}else
			
				try {
					
					//PREPARAR EMAIL A ENVIAR-----------------------------------------------------
					final String from = fromEmail; 
					final String password = fromPassword; 
					final String toEmail = invitation.getRecipient();
					
					Properties props = new Properties();
					props.put("mail.smtp.host", "smtp.gmail.com"); //SMTP Host
					props.put("mail.smtp.port", "587"); //TLS Port
					props.put("mail.smtp.auth", "true"); //enable authentication
					props.put("mail.smtp.starttls.enable", "true"); //enable STARTTLS
					
			        //create Authenticator object to pass in Session.getInstance argument
					Authenticator auth = new Authenticator() {
						//override the getPasswordAuthentication method
						protected PasswordAuthentication getPasswordAuthentication() {
							return new PasswordAuthentication(from, password);
						}
					};
					
					Session session = Session.getInstance(props, auth);
					
					//pass: kuehcqjnwqdbbylv
					//ENVIO DE EMAIL
					
					MimeMessage msg = new MimeMessage(session);
				    //set message headers
				    msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
				    msg.addHeader("format", "flowed");
				    msg.addHeader("Content-Transfer-Encoding", "8bit");
		
				    msg.setFrom(new InternetAddress(administrator.getEmail(), "ROHIGMESE Support"));	
				    msg.setReplyTo(InternetAddress.parse(administrator.getEmail(), false));
				    
					final String subject;
					final String body;
					
					if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
						subject = "Invitation to Web App ROHIGMESE";
						body = "Hello,<br/><br/>You have been invited to be part of the ROHIGMESE project as a researcher."
						+ "<br/><br/>The data you will need for registration are the following:"
						+ "<br/>Username: <b>"+invitation.getUsername()+"</b>"
						+ "<br/>Temporary password: <b>"+invitation.getTempPassword()+"</b>"
						+ "<br/><br/>Please click on the link to go to the website and register:"
						+ "<br/><a href='http://localhost:8080/Rohigmese/researcher/create.do'>"
						+ "http://localhost:8080/Rohigmese/researcher/create.do?language=en</a>";
					}else {
						subject = "Invitaci�n Aplicaci�n Web ROHIGMESE";
						body = "Hola,<br/><br/>Ha sido invitado para formar parte del proyecto ROHIGMESE como investigador." 
						+ "<br/><br/>Los datos que necesitara para el registro son los siguientes:"
						+ "<br/>Username: <b>"+invitation.getUsername()+"</b>"
						+ "<br/>Password temporal: <b>"+invitation.getTempPassword()+"</b>"
						+ "<br/><br/>Por favor, haga click en el enlace para ir al sitio web y poder registrarse:"
						+ "<br/><a href='http://localhost:8080/Rohigmese/researcher/create.do'>"
						+ "http://localhost:8080/Rohigmese/researcher/create.do?language=es</a>";
					}
				    
				    msg.setSubject(subject, "UTF-8");
				    msg.setText(body, "UTF-8");
				    msg.setSentDate(new Date(System.currentTimeMillis()));
				    msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail, false));
	
			    	Transport.send(msg);  
					//-----------------------------------------------------------------------------
			    	
			    	//Registro de la invitaci�n enviada
			    	this.invitationService.save(invitation);
					
					
					final String invitationSuccess;
					
			         if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
			        	 invitationSuccess = "Invitation successfully sent";
			         }else {
			        	 invitationSuccess = "Invitaci�n enviada con �xito";
			         }
			         
			         redirect.addFlashAttribute("invitationSuccess", invitationSuccess);
			         
			         modelAndView = new ModelAndView("redirect:/welcome/index.do");
						
					this.updateBreadCrumb(modelAndView, null,null,null,null,request);
	
				} catch (AuthenticationFailedException e) {
			         modelAndView = this.createRegisterModelAndView(invitation);
			         modelAndView.addObject("adminEmail", administrator.getEmail());
			         
			         final String authAlert;
						
			         if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
			        	 authAlert = "Authentication failure. The password or e-mail you entered are wrong.";
			         }else {
			        	 authAlert = "Fallo de autenticaci�n. La contrase�a o el e-mail que ha introducido son incorrectos.";
			         }
						
			         modelAndView.addObject("authAlert", authAlert);
						
			         this.updateBreadCrumb(modelAndView, "Administrator","Administrador","New researcher","Nuevo investigador",request);
			   
				} catch (Exception e) {
					modelAndView = this.createRegisterModelAndView(invitation, "administrator.commit.error");
					modelAndView.addObject("adminEmail", administrator.getEmail());
					
					this.updateBreadCrumb(modelAndView, "Administrator","Administrador","New researcher","Nuevo investigador",request);
	
				}
			}

		return modelAndView;

	}

	// Ancillary methods --------------------------------------------------------------
	protected ModelAndView createRegisterModelAndView(final Invitation invitation) {
		ModelAndView result;
		result = this.createRegisterModelAndView(invitation, null);
		return result;
	}

	protected ModelAndView createRegisterModelAndView(final Invitation invitation, final String message) {
		ModelAndView result;

		result = new ModelAndView("researcher/administrator/register");

		result.addObject("invitation", invitation);
		result.addObject("message", message);

		return result;
	}
	
	protected void updateBreadCrumb(ModelAndView mv,String parentEN,String parentES,String currentPageEN,String currentPageES,final HttpServletRequest request) {
		
		final LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(request);
		
		if(parentEN!=null && parentES!=null && currentPageEN!=null && currentPageES!=null) {
			if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				mv.addObject("parent", parentEN);
				mv.addObject("currentPage", currentPageEN);
			}else {
				mv.addObject("parent", parentES);
				mv.addObject("currentPage", currentPageES);
			}
		}else if(parentEN==null || parentES==null ) {
			
			if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				mv.addObject("currentPage", currentPageEN);
			}else {
				mv.addObject("currentPage", currentPageES);
			}
			
		}else {
			
			mv.addObject("currentPage", "Index");
			
		}
		
		
		
	}

}
