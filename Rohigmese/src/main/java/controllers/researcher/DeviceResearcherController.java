/*
 * ResearcherController.java
 * 
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.researcher;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.github.rcaller.rstuff.RCaller;
import com.github.rcaller.rstuff.RCode;
import com.opencsv.CSVReader;
import com.univocity.parsers.common.processor.RowListProcessor;
import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;

import controllers.AbstractController;
import domain.Device;
import domain.DomainEntity;
import domain.History;
import domain.Researcher;
import domain.Station;
import domain.User;
import domain.Zone;
import domain.dataTables.Helman.HelmanLisimeter;
import forms.ProjectConfiguration;
import services.DeviceService;
import services.HelmanService;
import services.HistoryService;
import services.ResearcherService;
import services.StationService;
import services.ZoneService;
import java.util.concurrent.TimeUnit;

@Controller
@RequestMapping("/device/researcher")
public class DeviceResearcherController extends AbstractController {

	// Services--------------------------------------------------------------------------
	@Autowired
	private DeviceService deviceService;
	@Autowired
	private StationService stationService;
	@Autowired
	private ZoneService zoneService;
	@Autowired
	private HistoryService historyService;
	@Autowired
	private ResearcherService researcherService;
	@Autowired
	private HelmanService helmanService;

	// Constructors -----------------------------------------------------------

	public DeviceResearcherController() {
		super();
	}

	// Editing---------------------------------------------------------------------------
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(final int deviceId, final HttpServletRequest request) {
		ModelAndView modelAndView;

		final Device device = this.deviceService.findOne(deviceId);
		final Station station = this.stationService.findOne(device.getStation().getId());
		final Zone zone = this.zoneService.findOne(station.getZone().getId());

		modelAndView = this.createEditModelAndView(device);

		this.updateBreadCrumb(modelAndView,
				"Zone: " + zone.getName() + ", Station: " + station.getName() + ", Device: " + device.getName(),
				"Zona: " + zone.getName() + ", Estación: " + station.getName() + ", Dispositivo: " + device.getName(),
				"Data", "Datos", request);

		return modelAndView;
	}

	// Saving---------------------------------------------------------------------------
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final Device device, final BindingResult bindingResult,
			final HttpServletRequest request) {
		ModelAndView modelAndView;

		final Station station = this.stationService.findOne(device.getStation().getId());
		final Zone zone = this.zoneService.findOne(station.getZone().getId());

		this.deviceService.save(device);

		modelAndView = new ModelAndView("redirect:/device/station/list.do?stationId=" + station.getId());

		this.updateBreadCrumb(modelAndView, "Zone: " + zone.getName() + ", Station: " + station.getName(),
				"Zona: " + zone.getName() + ", Estación: " + station.getName(), "Equipment", "Equipamiento", request);

		return modelAndView;
	}

	// Delete data-----------------------------------------------------------------
	@SuppressWarnings("resource")
	@RequestMapping(value = "/deleteData", method = RequestMethod.GET)
	public ModelAndView deleteData(final int stationId, final int deviceId, final HttpServletRequest request,
			final RedirectAttributes redirect) {
		final ModelAndView modelAndView;

		// Interceptor de cambio de idioma
		final LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(request);

		final Station station = this.stationService.findOne(stationId);
		final Zone zone = this.zoneService.findOne(station.getZone().getId());
		final Device device = this.deviceService.findOne(deviceId);

		Connection con = null;
		Statement st = null;
		ResultSet rs = null;

		String url = "jdbc:mysql://localhost:3306/Rohigmese";
		String user = "root";
		String password = "rohigmese";
		int count = 0;
		int totalcount = 0;
		String query = "";

		boolean error = false;

		int minid = 0;
		int maxid = 0;

		try {

			// CONFIGURACIONES DE LA BD
			con = DriverManager.getConnection(url, user, password);
			st = con.createStatement();

			String deviceTable = "";

			// ZONA: LISIMETRO - ESTACION: LISIMETRO
			if (station.getName().equals("Lisímetro")) {

				switch (device.getName()) {
				case "Meteo-Lisímetro":
					deviceTable = "MeteoLisimeter";

					// MAXIMO ID
					rs = st.executeQuery("SELECT MAX(RID) FROM `Rohigmese`.`" + deviceTable + "`");
					while (rs.next()) {
						maxid = rs.getInt("MAX(RID)");
					}
					break;
				case "Pesos-Lisímetro":
					deviceTable = "WeightsLisimeter";

					// MAXIMO ID
					rs = st.executeQuery("SELECT MAX(RID) FROM `Rohigmese`.`" + deviceTable + "`");
					while (rs.next()) {
						maxid = rs.getInt("MAX(RID)");
					}
					break;
				case "Sensores Campbell":
					deviceTable = "CampbellLisimeter";

					// MAXIMO ID
					rs = st.executeQuery("SELECT MAX(RID) FROM `Rohigmese`.`" + deviceTable + "`");
					while (rs.next()) {
						maxid = rs.getInt("MAX(RID)");
					}
					break;
				case "Pluviómetro digital":
					deviceTable = "DavisLisimeter";

					// MAXIMO ID
					rs = st.executeQuery("SELECT MAX(RID) FROM `Rohigmese`.`" + deviceTable + "`");
					while (rs.next()) {
						maxid = rs.getInt("MAX(RID)");
					}
					break;
				case "Pluviómetro de pesada":
					deviceTable = "HelmanLisimeter";

					// MAXIMO ID
					rs = st.executeQuery("SELECT MAX(RID) FROM `Rohigmese`.`" + deviceTable + "`");
					while (rs.next()) {
						maxid = rs.getInt("MAX(RID)");
					}
					break;
				}

			}

			// ZONA: MARISMILLAS - ESTACION: CORTAFUEGOS 7
			if (station.getName().equals("Cortafuegos 7")) {

				switch (device.getName()) {
				case "Datalogger 1":
					deviceTable = "CampbellC7DT1";

					// MAXIMO ID
					rs = st.executeQuery("SELECT MAX(RID) FROM `Rohigmese`.`" + deviceTable + "`");
					while (rs.next()) {
						maxid = rs.getInt("MAX(RID)");
					}
					break;
				case "Datalogger 2":
					deviceTable = "CampbellC7DT2";

					// MAXIMO ID
					rs = st.executeQuery("SELECT MAX(RID) FROM `Rohigmese`.`" + deviceTable + "`");
					while (rs.next()) {
						maxid = rs.getInt("MAX(RID)");
					}
					break;
				case "Pluviómetro digital":
					deviceTable = "DavisC7";

					// MAXIMO ID
					rs = st.executeQuery("SELECT MAX(RID) FROM `Rohigmese`.`" + deviceTable + "`");
					while (rs.next()) {
						maxid = rs.getInt("MAX(RID)");
					}
					break;
				}

			}

			// ZONA: MARISMILLAS - ESTACION: LLANOS
			if (station.getName().equals("Llanos")) {

				switch (device.getName()) {
				case "Pluviómetro digital":
					deviceTable = "DavisLlanos";

					// MAXIMO ID
					rs = st.executeQuery("SELECT MAX(RID) FROM `Rohigmese`.`" + deviceTable + "`");
					while (rs.next()) {
						maxid = rs.getInt("MAX(RID)");
					}
					break;
				case "Sensores Decagon":
					deviceTable = "DecagonLlanos";

					// MAXIMO ID
					rs = st.executeQuery("SELECT MAX(RID) FROM `Rohigmese`.`" + deviceTable + "`");
					while (rs.next()) {
						maxid = rs.getInt("MAX(RID)");
					}
					break;
				}

			}

			// ZONA: MARISMILLAS - ESTACION: LAGUNA
			if (station.getName().equals("Laguna")) {

				switch (device.getName()) {
				case "Sensores Decagon":
					deviceTable = "DecagonLaguna";

					// MAXIMO ID
					rs = st.executeQuery("SELECT MAX(RID) FROM `Rohigmese`.`" + deviceTable + "`");
					while (rs.next()) {
						maxid = rs.getInt("MAX(RID)");
					}
					break;
				}

			}

			// ZONA: MARISMILLAS - ESTACION: PALACIO
			if (station.getName().equals("Palacio")) {

				switch (device.getName()) {
				case "Sensores Decagon":
					deviceTable = "DecagonPalacio";

					// MAXIMO ID
					rs = st.executeQuery("SELECT MAX(RID) FROM `Rohigmese`.`" + deviceTable + "`");
					while (rs.next()) {
						maxid = rs.getInt("MAX(RID)");
					}
					break;
				}

			}

			// ZONA: SANTA-OLALLA - ESTACION: SO-IGME
			if (station.getName().equals("SO-IGME")) {

				switch (device.getName()) {
				case "Pluviómetro digital":
					deviceTable = "DavisSO";

					// MAXIMO ID
					rs = st.executeQuery("SELECT MAX(RID) FROM `Rohigmese`.`" + deviceTable + "`");
					while (rs.next()) {
						maxid = rs.getInt("MAX(RID)");
					}
					break;
				}

			}

			// ZONA: SANTA-OLALLA - ESTACION: SO-P2
			if (station.getName().equals("SO-P2")) {

				switch (device.getName()) {
				case "Sensores Campbell":
					deviceTable = "CampbellSOP2";

					// MAXIMO ID
					rs = st.executeQuery("SELECT MAX(RID) FROM `Rohigmese`.`" + deviceTable + "`");
					while (rs.next()) {
						maxid = rs.getInt("MAX(RID)");
					}
					break;
				}

			}

			// ZONA: SANTA-OLALLA - ESTACION: SO-P3
			if (station.getName().equals("SO-P3")) {

				switch (device.getName()) {
				case "Sensores Campbell":
					deviceTable = "CampbellSOP3";

					// MAXIMO ID
					rs = st.executeQuery("SELECT MAX(RID) FROM `Rohigmese`.`" + deviceTable + "`");
					while (rs.next()) {
						maxid = rs.getInt("MAX(RID)");
					}
					break;
				}

			}

			// ZONA: SANTA-OLALLA - ESTACION: SO-P1
			if (station.getName().equals("SO-P1")) {

				switch (device.getName()) {
				case "Nivel freático":
					deviceTable = "ManualSOP1";

					// MAXIMO ID
					rs = st.executeQuery("SELECT MAX(RID) FROM `Rohigmese`.`" + deviceTable + "`");
					while (rs.next()) {
						maxid = rs.getInt("MAX(RID)");
					}
					break;
				}

			}

			// BORRAR TABLA
			st.executeUpdate("DELETE FROM `Rohigmese`.`" + deviceTable + "`");

		} catch (Exception e) {
			e.printStackTrace();

			final String deleteError;

			if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				deleteError = "Error in deleting data.";
			} else {
				deleteError = "Error al borrar los datos.";
			}

			redirect.addFlashAttribute("deleteError", deleteError);

			error = true;

		} finally {
			try {

				if (error == false) {

					// Borrado reflejado en el historial
					History history = new History();
					Researcher author = this.researcherService.findByPrincipal();

					history.setIdEnd(minid);
					history.setIdEnd(maxid);
					history.setAuthor(author.getName() + " " + author.getSurname());
					history.setDate(new Date(System.currentTimeMillis() - 1000));
					if (zone.getName().equals("LISIMETRO")) {
						if (device.getBrand() != null) {
							history.setDevice("[-] " + station.getName().toUpperCase() + " > " + device.getName() + " ["
									+ device.getBrand() + "]");
						} else {
							history.setDevice("[-] " + station.getName().toUpperCase() + " > " + device.getName());
						}

					} else {
						if (device.getBrand() != null) {
							history.setDevice("[-] " + zone.getName() + "-" + station.getName().toUpperCase() + " > "
									+ device.getName() + " [" + device.getBrand() + "]");
						} else {
							history.setDevice("[-] " + zone.getName() + "-" + station.getName().toUpperCase() + " > "
									+ device.getName());
						}
					}

					historyService.save(history);

					final String deleteSuccess;

					if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
						deleteSuccess = "Data successfully deleted.";
					} else {
						deleteSuccess = "Datos borrados con éxito.";
					}

					redirect.addFlashAttribute("deleteSuccess", deleteSuccess);

				}

				// CIERRE DE CONEXION CON LA BD

				if (st != null) {
					st.close();
				}
				if (con != null) {
					con.close();
				}

			} catch (SQLException ex) {
				Logger lgr = Logger.getLogger(DeviceResearcherController.class.getName());
				lgr.log(Level.WARNING, ex.getMessage(), ex);

				final String deleteError;

				if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
					deleteError = "Error deleting data.";
				} else {
					deleteError = "Error al borrar los datos.";
				}

				redirect.addFlashAttribute("deleteError", deleteError);

				error = true;

			}
		}

		modelAndView = new ModelAndView("redirect:/device/viewData.do?deviceId=" + device.getId());

		this.updateBreadCrumb(modelAndView, null, null, null, null, request);

		return modelAndView;
	}

	// Select
	// device------------------------------------------------------------------
	@RequestMapping(value = "/selectDevice", method = RequestMethod.GET)
	public ModelAndView selectDevice(final int stationId, @ModelAttribute("scriptExecuted") String scriptExecuted, final HttpServletRequest request) {

		final ModelAndView modelAndView;

		final Station station = this.stationService.findOne(stationId);
		final Zone zone = this.zoneService.findOne(station.getZone().getId());

		final Collection<Device> devices = new ArrayList<Device>(station.getDevices());

		modelAndView = new ModelAndView("device/selectDevice");

		modelAndView.addObject("devices", devices);
		modelAndView.addObject("station", station);
		
		if (scriptExecuted != "") {
			modelAndView.addObject("scriptExecuted", scriptExecuted);
		}

		this.updateBreadCrumb(modelAndView, "Zone: " + zone.getName() + ", Station: " + station.getName(),
				"Zona: " + zone.getName() + ", Estación: " + station.getName(), "New data", "Nuevos datos", request);

		return modelAndView;

	}

	// Run R-Scripts
	@RequestMapping(value = "/runRScript", method = RequestMethod.POST, params = "run")
	public ModelAndView runRScript(@RequestParam final int stationId, @RequestParam String scriptPath,
			@RequestParam String scriptName, final HttpServletRequest request,final RedirectAttributes redirect) {

		final ModelAndView modelAndView;

		// Ejecucion de R-Script
		try {

			RCaller rcaller = RCaller.create();
			RCode code = RCode.create();

			String fullPath = new File(scriptPath,scriptName).getAbsolutePath();

			rcaller.setRCode(code);

			code.addRCode("library(openxlsx)");
			code.addRCode("library(tidyr)");
			code.addRCode("library(dplyr)");
			code.addRCode("library(DescTools)");
			code.addRCode("library(rstudioapi)");
			scriptPath = scriptPath.replace("\\", "/");
			fullPath = fullPath.replace("\\", "/");
			code.addRCode("setwd('" + scriptPath + "')");
			code.addRCode("source('" + fullPath + "')");
			rcaller.runOnly();
			rcaller.deleteTempFiles();

		} catch (Exception e) {
			System.out.println(e.toString());
		} finally {
			final String scriptExecuted;

			// Interceptor de cambio de idioma
			final LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(request);

			if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				scriptExecuted = "R-Script successfully executed.";
			} else {
				scriptExecuted = "R-Script ejecutado con éxito.";
			}

			redirect.addFlashAttribute("scriptExecuted", scriptExecuted);
		}

		modelAndView = new ModelAndView("redirect:/device/researcher/selectDevice.do?stationId=" + stationId);

		return modelAndView;

	}

	// Select Data------------------------------------------------------------------
	@RequestMapping(value = "/selectData", method = RequestMethod.POST, params = "save")
	public ModelAndView selectData(@RequestParam final int deviceSelected, final HttpServletRequest request,final RedirectAttributes redirect) {

		final ModelAndView modelAndView;

		final Device device = this.deviceService.findOne(deviceSelected);
		final Station station = this.stationService.findOne(device.getStation().getId());
		final Zone zone = this.zoneService.findOne(station.getZone().getId());

		modelAndView = new ModelAndView("device/selectData");

		modelAndView.addObject("zone", zone);
		modelAndView.addObject("device", device);
		modelAndView.addObject("station", station);

		File root = new File(ProjectConfiguration.ROOTPATHRSCRIPTS);

		String filename = "";
		String scriptPath = "";
		String script = "";

		if (station.getName().equals("Lisímetro")) {

			switch (device.getName()) {
			case "Meteo-Lisímetro":
				filename = "EXPORTED_LISIMETRO_METEO.csv";
				scriptPath = "Lisimetro/Lisimetro/METEO";
				script = "Lisimetro_Meteo.R";
				break;
			case "Pesos-Lisímetro":
				filename = "EXPORTED_LISIMETRO_WEIGHTS.csv";
				scriptPath = "Lisimetro/Lisimetro/WEIGHTS";
				script = "Lisimetro_Weights.R";
				break;
			case "Sensores Campbell":
				filename = "EXPORTED_LISIMETRO_CAMPBELL.csv";
				scriptPath = "Lisimetro/Campbell";
				script = "Lisimetro_Campbell.R";
				break;
			case "Pluviómetro digital":
				filename = "EXPORTED_LISIMETRO_DAVIS.txt";
				scriptPath = "Lisimetro/Davis";
				script = "Lisimetro_Davis.R";
				break;
			case "Pluviómetro de pesada":
				filename = "EXPORTED_LISIMETRO_HELMAN.txt";
				scriptPath = "Lisimetro/Helman";
				script = "Lisimetro_Helman.R";
				break;
			}

		}

		if (station.getName().equals("Cortafuegos 7")) {

			switch (device.getName()) {
			case "Datalogger 1":
				filename = "EXPORTED_MARISMILLAS_CAMPBELL_C7_DT1.csv";
				scriptPath = "Marismillas/Campbell/C7 DT1";
				script = "Marismillas_Campbell_C7_DT1.R";
				break;
			case "Datalogger 2":
				filename = "EXPORTED_MARISMILLAS_CAMPBELL_C7_DT2.csv";
				scriptPath = "Marismillas/Campbell/C7 DT2";
				script = "Marismillas_Campbell_C7_DT2.R";
				break;
			case "Pluviómetro digital":
				filename = "EXPORTED_MARISMILLAS_DAVIS_C7.txt";
				scriptPath = "Marismillas/Davis/C7";
				script = "Marismillas_Davis_C7.R";
				break;
			}

		}

		if (station.getName().equals("Llanos")) {

			switch (device.getName()) {
			case "Pluviómetro digital":
				filename = "EXPORTED_MARISMILLAS_DAVIS_LLANOS.txt";
				scriptPath = "Marismillas/Davis/LLANOS";
				script = "Marismillas_Davis_Llanos.R";
				break;
			case "Sensores Decagon":
				filename = "EXPORTED_MARISMILLAS_DECAGON_LLANOS.csv";
				scriptPath = "Marismillas/Decagon/LLANOS";
				script = "Marismillas_Decagon_Llanos.R";
				break;
			}

		}

		if (station.getName().equals("Laguna")) {

			switch (device.getName()) {
			case "Sensores Decagon":
				filename = "EXPORTED_MARISMILLAS_DECAGON_LAGUNA.csv";
				scriptPath = "Marismillas/Decagon/LAGUNA";
				script = "Marismillas_Decagon_Laguna.R";
				break;
			}

		}

		if (station.getName().equals("Palacio")) {

			switch (device.getName()) {
			case "Sensores Decagon":
				filename = "EXPORTED_MARISMILLAS_DECAGON_PALACIO.csv";
				scriptPath = "Marismillas/Decagon/PALACIO";
				script = "Marismillas_Decagon_Palacio.R";
				break;
			}

		}

		if (station.getName().equals("SO-IGME")) {

			switch (device.getName()) {
			case "Pluviómetro digital":
				filename = "EXPORTED_SANTA_OLALLA_DAVIS.txt";
				scriptPath = "SantaOlalla/Davis";
				script = "Santa_Olalla_Davis.R";
				break;
			}

		}

		if (station.getName().equals("SO-P2")) {

			switch (device.getName()) {
			case "Sensores Campbell":
				filename = "EXPORTED_SANTA_OLALLA_CAMPBELL_P2.csv";
				scriptPath = "SantaOlalla/Campbell/P2";
				script = "Santa_Olalla_Campbell_P2.R";
				break;
			}

		}

		if (station.getName().equals("SO-P3")) {

			switch (device.getName()) {
			case "Sensores Campbell":
				filename = "EXPORTED_SANTA_OLALLA_CAMPBELL_P3.csv";
				scriptPath = "SantaOlalla/Campbell/P3";
				script = "Santa_Olalla_Campbell_P3.R";
				break;
			}

		}

		if (station.getName().equals("SO-P1")) {

			switch (device.getName()) {
			case "Nivel freático":
				filename = "EXPORTED_SANTA_OLALLA_NIVEL_P1.csv";
				scriptPath = "SantaOlalla/P1";
				script = "Santa_Olalla_Niveles_P1.R";
				break;
			}

		}

		File rFile = new File(root, scriptPath);
		rFile = new File(rFile, "OUTPUT/" + filename);
		boolean fileExist = false;

		try {

			@SuppressWarnings("resource")
			FileInputStream inputStream = new FileInputStream(rFile);
			fileExist = true;

		} catch (FileNotFoundException e) {
			fileExist = false;
		}

		scriptPath = new File(root, scriptPath).getAbsolutePath();

		// Obtener nombre del script:
		modelAndView.addObject("scriptPath", scriptPath);
		modelAndView.addObject("script", script);

		modelAndView.addObject("fileExist", fileExist);
		redirect.addFlashAttribute("fileExist", fileExist);

		modelAndView.addObject("filename", filename);

		if (device.getBrand() != null) {
			this.updateBreadCrumb(modelAndView,
					"Zone: " + zone.getName() + ", Station: " + station.getName() + ", Device: " + device.getName()
							+ " [" + device.getBrand() + "]",
					"Zona: " + zone.getName() + ", Estación: " + station.getName() + ", Equipamiento: "
							+ device.getName() + " [" + device.getBrand() + "]",
					"Import data", "Importar datos", request);
		} else {
			this.updateBreadCrumb(modelAndView,
					"Zone: " + zone.getName() + ", Station: " + station.getName() + ", Device: " + device.getName(),
					"Zona: " + zone.getName() + ", Estación: " + station.getName() + ", Equipamiento: "
							+ device.getName(),
					"Import data", "Importar datos", request);
		}

		return modelAndView;
	}

	@RequestMapping(value = "/newData", method = RequestMethod.POST, params = "save")
	public ModelAndView newData(@RequestParam("file") final File file, @RequestParam final int deviceSelected,
			@RequestParam String filename,final HttpServletRequest request, final RedirectAttributes redirect)
			throws IOException {

		ModelAndView modelAndView;

		final Device device = this.deviceService.findOne(deviceSelected);
		final Station station = this.stationService.findOne(device.getStation().getId());
		final Zone zone = this.zoneService.findOne(station.getZone().getId());

		// Comprobar si el archivo es correcto
		if (filename.equals(file.getName())) {

			Researcher author = this.researcherService.findByPrincipal();

			String url = "jdbc:mysql://localhost:3306/Rohigmese";
			String user = "root";
			String password = "rohigmese";

			RowListProcessor rowProcessor = new RowListProcessor();

			CsvParserSettings parserSettings = new CsvParserSettings();
			parserSettings.setProcessor(rowProcessor);
			parserSettings.getFormat().setDelimiter(';');

			CsvParser parser = new CsvParser(parserSettings);

			File root = new File(ProjectConfiguration.ROOTPATHRSCRIPTS);

			if (zone.getName().equals("LISIMETRO")) {
				File stationPath = new File(root, "Lisimetro");

				switch (device.getName()) {

				// Dispositivo: Meteo Lisímetro
				case "Meteo-Lisímetro":
					storeMeteoLisimeter(parser, stationPath, file, filename, author, device, historyService, redirect,
							request, url, user, password);
					break;

				// Dispositivo: Pesos Lisímetro
				case "Pesos-Lisímetro":
					storeWeightsLisimeter(parser, stationPath, file, filename, author, device, historyService, redirect,
							request, url, user, password);
					break;

				// Dispositivo: Sensores Campbell Lisímetro
				case "Sensores Campbell":
					storeCampbellLisimeter(parser, stationPath, file, filename, author, device, historyService,
							redirect, request, url, user, password);
					break;

				// Dispositivo: Davis Lisímetro
				case "Pluviómetro digital":
					storeDavisLisimeter(parser, stationPath, file, filename, author, device, historyService, redirect,
							request, url, user, password);
					break;

				// Dispositivo: Helman Lisímetro
				case "Pluviómetro de pesada":
					storeHelmanLisimeter(parser, stationPath, file, filename, author, device, historyService, redirect,
							request, url, user, password);
					break;

				}

			}

			if (zone.getName().equals("MARISMILLAS")) {

				File stationPath = new File(root, "Marismillas");

				if (station.getName().equals("Cortafuegos 7")) {

					switch (device.getName()) {

					// Dispositivo: Campbell DT1
					case "Datalogger 1":
						storeCampbellDT1C7(parser, stationPath, file, filename, author, device, historyService,
								redirect, request, url, user, password);
						break;

					// Dispositivo: Campbell DT2
					case "Datalogger 2":
						storeCampbellDT2C7(parser, stationPath, file, filename, author, device, historyService,
								redirect, request, url, user, password);
						break;

					// Dispositivo: Davis C7
					case "Pluviómetro digital":
						storeDavisC7(parser, stationPath, file, filename, author, device, historyService, redirect,
								request, url, user, password);
						break;
					}

				}

				if (station.getName().equals("Llanos")) {

					switch (device.getName()) {

					// Dispositivo: Davis Llanos
					case "Pluviómetro digital":
						storeDavisLlanos(parser, stationPath, file, filename, author, device, historyService, redirect,
								request, url, user, password);
						break;
					// Dispositivo: Decagon Llanos
					case "Sensores Decagon":
						storeDecagonLlanos(parser, stationPath, file, filename, author, device, historyService,
								redirect, request, url, user, password);
						break;
					}

				}

				if (station.getName().equals("Laguna")) {

					switch (device.getName()) {

					// Dispositivo: Decagon Laguna
					case "Sensores Decagon":
						storeDecagonLaguna(parser, stationPath, file, filename, author, device, historyService,
								redirect, request, url, user, password);
						break;
					}

				}

				if (station.getName().equals("Palacio")) {

					switch (device.getName()) {

					// Dispositivo: Decagon Palacio
					case "Sensores Decagon":
						storeDecagonPalacio(parser, stationPath, file, filename, author, device, historyService,
								redirect, request, url, user, password);
						break;
					}

				}

			}

			if (zone.getName().equals("SANTA OLALLA")) {

				File stationPath = new File(root, "SantaOlalla");

				if (station.getName().equals("SO-IGME")) {

					switch (device.getName()) {

					// Dispositivo: Davis SO-IGME
					case "Pluviómetro digital":
						storeDavisSO(parser, stationPath, file, filename, author, device, historyService, redirect,
								request, url, user, password);
						break;
					}

				}

				if (station.getName().equals("SO-P2")) {

					switch (device.getName()) {

					// Dispositivo: Campbell SO-P2
					case "Sensores Campbell":
						storeCampbellSOP2(parser, stationPath, file, filename, author, device, historyService, redirect,
								request, url, user, password);
						break;
					}

				}

				if (station.getName().equals("SO-P3")) {

					switch (device.getName()) {

					// Dispositivo: Campbell SO-P3
					case "Sensores Campbell":
						storeCampbellSOP3(parser, stationPath, file, filename, author, device, historyService, redirect,
								request, url, user, password);
						break;
					}

				}

				if (station.getName().equals("SO-P1")) {

					switch (device.getName()) {

					// Dispositivo: Nivel freático SO-P1
					case "Nivel freático":
						storeManualSOP1(parser, stationPath, file, filename, author, device, historyService, redirect,
								request, url, user, password);
						break;
					}

				}

			}

			modelAndView = new ModelAndView("redirect:/device/viewData.do?deviceId=" + device.getId());

			this.updateBreadCrumb(modelAndView, null, null, null, null, request);

		} else {

			modelAndView = new ModelAndView("device/selectData");

			modelAndView.addObject("device", device);
			modelAndView.addObject("station", station);

			final String wrongFileAlert;

			// Interceptor de cambio de idioma
			final LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(request);

			if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				wrongFileAlert = "The selected file does not match '" + filename + "'";
			} else {
				wrongFileAlert = "El archivo seleccionado no coincide con '" + filename + "'";
			}

			modelAndView.addObject("wrongFileAlert", wrongFileAlert);
			
			File root = new File(ProjectConfiguration.ROOTPATHRSCRIPTS);

			String fileName = "";
			String scriptPath = "";
			String script = "";

			if (station.getName().equals("Lisímetro")) {

				switch (device.getName()) {
				case "Meteo-Lisímetro":
					fileName = "EXPORTED_LISIMETRO_METEO.csv";
					scriptPath = "Lisimetro/Lisimetro/METEO";
					script = "Lisimetro_Meteo.R";
					break;
				case "Pesos-Lisímetro":
					fileName = "EXPORTED_LISIMETRO_WEIGHTS.csv";
					scriptPath = "Lisimetro/Lisimetro/WEIGHTS";
					script = "Lisimetro_Weights.R";
					break;
				case "Sensores Campbell":
					fileName = "EXPORTED_LISIMETRO_CAMPBELL.csv";
					scriptPath = "Lisimetro/Campbell";
					script = "Lisimetro_Campbell.R";
					break;
				case "Pluviómetro digital":
					fileName = "EXPORTED_LISIMETRO_DAVIS.txt";
					scriptPath = "Lisimetro/Davis";
					script = "Lisimetro_Davis.R";
					break;
				case "Pluviómetro de pesada":
					fileName = "EXPORTED_LISIMETRO_HELMAN.txt";
					scriptPath = "Lisimetro/Helman";
					script = "Lisimetro_Helman.R";
					break;
				}

			}

			if (station.getName().equals("Cortafuegos 7")) {

				switch (device.getName()) {
				case "Datalogger 1":
					fileName = "EXPORTED_MARISMILLAS_CAMPBELL_C7_DT1.csv";
					scriptPath = "Marismillas/Campbell/C7 DT1";
					script = "Marismillas_Campbell_C7_DT1.R";
					break;
				case "Datalogger 2":
					fileName = "EXPORTED_MARISMILLAS_CAMPBELL_C7_DT2.csv";
					scriptPath = "Marismillas/Campbell/C7 DT2";
					script = "Marismillas_Campbell_C7_DT2.R";
					break;
				case "Pluviómetro digital":
					fileName = "EXPORTED_MARISMILLAS_DAVIS_C7.txt";
					scriptPath = "Marismillas/Davis/C7";
					script = "Marismillas_Davis_C7.R";
					break;
				}

			}

			if (station.getName().equals("Llanos")) {

				switch (device.getName()) {
				case "Pluviómetro digital":
					fileName = "EXPORTED_MARISMILLAS_DAVIS_LLANOS.txt";
					scriptPath = "Marismillas/Davis/LLANOS";
					script = "Marismillas_Davis_Llanos.R";
					break;
				case "Sensores Decagon":
					fileName = "EXPORTED_MARISMILLAS_DECAGON_LLANOS.csv";
					scriptPath = "Marismillas/Decagon/LLANOS";
					script = "Marismillas_Decagon_Llanos.R";
					break;
				}

			}

			if (station.getName().equals("Laguna")) {

				switch (device.getName()) {
				case "Sensores Decagon":
					fileName = "EXPORTED_MARISMILLAS_DECAGON_LAGUNA.csv";
					scriptPath = "Marismillas/Decagon/LAGUNA";
					script = "Marismillas_Decagon_Laguna.R";
					break;
				}

			}

			if (station.getName().equals("Palacio")) {

				switch (device.getName()) {
				case "Sensores Decagon":
					fileName = "EXPORTED_MARISMILLAS_DECAGON_PALACIO.csv";
					scriptPath = "Marismillas/Decagon/PALACIO";
					script = "Marismillas_Decagon_Palacio.R";
					break;
				}

			}

			if (station.getName().equals("SO-IGME")) {

				switch (device.getName()) {
				case "Pluviómetro digital":
					fileName = "EXPORTED_SANTA_OLALLA_DAVIS.txt";
					scriptPath = "SantaOlalla/Davis";
					script = "Santa_Olalla_Davis.R";
					break;
				}

			}

			if (station.getName().equals("SO-P2")) {

				switch (device.getName()) {
				case "Sensores Campbell":
					fileName = "EXPORTED_SANTA_OLALLA_CAMPBELL_P2.csv";
					scriptPath = "SantaOlalla/Campbell/P2";
					script = "Santa_Olalla_Campbell_P2.R";
					break;
				}

			}

			if (station.getName().equals("SO-P3")) {

				switch (device.getName()) {
				case "Sensores Campbell":
					fileName = "EXPORTED_SANTA_OLALLA_CAMPBELL_P3.csv";
					scriptPath = "SantaOlalla/Campbell/P3";
					script = "Santa_Olalla_Campbell_P3.R";
					break;
				}

			}

			if (station.getName().equals("SO-P1")) {

				switch (device.getName()) {
				case "Nivel freático":
					fileName = "EXPORTED_SANTA_OLALLA_NIVEL_P1.csv";
					scriptPath = "SantaOlalla/P1";
					script = "Santa_Olalla_Niveles_P1.R";
					break;
				}

			}

			File rFile = new File(root, scriptPath);
			rFile = new File(rFile, "OUTPUT/" + fileName);
			boolean fileExist = false;

			try {

				@SuppressWarnings("resource")
				FileInputStream inputStream = new FileInputStream(rFile);
				fileExist = true;

			} catch (FileNotFoundException e) {
				fileExist = false;
			}

			scriptPath = new File(root, scriptPath).getAbsolutePath();

			// Obtener nombre del script:
			modelAndView.addObject("scriptPath", scriptPath);
			modelAndView.addObject("script", script);

			modelAndView.addObject("fileExist", fileExist);
			redirect.addFlashAttribute("fileExist", fileExist);

			modelAndView.addObject("fileName", fileName);

			if (device.getBrand() != null) {
				this.updateBreadCrumb(modelAndView,
						"Zone: " + zone.getName() + ", Station: " + station.getName() + ", Device: " + device.getName()
								+ " [" + device.getBrand() + "]",
						"Zona: " + zone.getName() + ", Estación: " + station.getName() + ", Equipamiento: "
								+ device.getName() + " [" + device.getBrand() + "]",
						"Import data", "Importar datos", request);
			} else {
				this.updateBreadCrumb(modelAndView,
						"Zone: " + zone.getName() + ", Station: " + station.getName() + ", Device: " + device.getName(),
						"Zona: " + zone.getName() + ", Estación: " + station.getName() + ", Equipamiento: "
								+ device.getName(),
						"Import data", "Importar datos", request);
			}

		}

		modelAndView.addObject("filename", filename);

		return modelAndView;
	}

	// Ancillary methods
	// --------------------------------------------------------------

	protected ModelAndView createEditModelAndView(final Device device) {
		ModelAndView result;
		result = this.createEditModelAndView(device, null);
		return result;
	}

	protected ModelAndView createEditModelAndView(final Device device, final String message) {
		ModelAndView result;

		result = new ModelAndView("device/edit");

		result.addObject("device", device);
		result.addObject("message", message);

		return result;
	}

	protected void updateBreadCrumb(ModelAndView mv, String parentEN, String parentES, String currentPageEN,
			String currentPageES, final HttpServletRequest request) {

		final LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(request);

		if (parentEN != null && parentES != null && currentPageEN != null && currentPageES != null) {
			if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				mv.addObject("parent", parentEN);
				mv.addObject("currentPage", currentPageEN);
			} else {
				mv.addObject("parent", parentES);
				mv.addObject("currentPage", currentPageES);
			}
		} else if (parentEN == null || parentES == null) {

			if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				mv.addObject("currentPage", currentPageEN);
			} else {
				mv.addObject("currentPage", currentPageES);
			}

		} else {

			mv.addObject("currentPage", "Index");

		}

	}

	public static Reader getReader(String path) throws FileNotFoundException {
		try {
			InputStream inputFS = new FileInputStream(path);
			return new InputStreamReader(inputFS, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			throw new IllegalStateException("Unable to read input", e);
		}
	}

	public static int getLineCount(String fileName) {

		int lines = 0;

		try {
			BufferedReader reader;
			reader = new BufferedReader(new FileReader(fileName));
			while (reader.readLine() != null) {
				lines++;
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return lines;

	}

	public static void storeMeteoLisimeter(CsvParser parser, File stationPath, final File file, final String filename,
			Researcher author, Device device, HistoryService historyService, final RedirectAttributes redirect,
			final HttpServletRequest request, String url, String user, String password) {

		File devicePath = new File(stationPath, "Lisimetro/METEO/OUTPUT/" + file.getName());
		File fullPath = new File(devicePath.getAbsolutePath());

		// Interceptor de cambio de idioma
		final LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(request);

		Connection con = null;
		Statement st = null;
		ResultSet rs = null;

		String query = "";

		int count = 0;
		int contLines = 1;
		int totalcount = 0;
		long nanoTime = 0;

		boolean error = false;

		try {
			// LECTURA DEL CSV-----------------
			parser.beginParsing(getReader(fullPath.getAbsolutePath()));
			int linecount = getLineCount(fullPath.getAbsolutePath());

			String[] nextLine;

			// SALTARSE LAS CABECERAS
			String[] firstLine = parser.parseNext();

			// CONFIGURACIONES DE LA BD
			con = DriverManager.getConnection(url, user, password);
			st = con.createStatement();

			int rowid = 0;

			rs = st.executeQuery("SELECT MAX(RID) FROM `Rohigmese`.`MeteoLisimeter`");

			while (rs.next()) {
				rowid = rs.getInt("MAX(RID)");
				rowid++;
			}

			History history = new History();

			String values = "";

			long startNanos = System.nanoTime();
			double batchSize = 0;

			if (linecount > 500) {
				batchSize = linecount * 0.02;
			} else {
				batchSize = linecount - 1;
			}

			contLines = 1;
			boolean exit = false;
			boolean first = true;

			while ((nextLine = parser.parseNext()) != null) {

				if (contLines >= rowid) {
					if (exit == false) {
						history.setIdStart(rowid);
						exit = true;
					}

					// TIMESTAMP;TENSIOMETRO LISI;TENSIOMETRO CAMPO;TEMPERATURA -1.40m;
					// TEMPERATURA -0.20m;TEMPERATURA -0.10m;TEMPERATURA -0.05m;TEMPERATURA +0.05m;
					// TEMPERATURA +0.50m;TEMPERATURA +2.00m;PRECIPITACION;VELOCIDAD VIENTO
					if (first == true) {
						values += "('" + rowid + "','" + device.getId() + "','" + nextLine[0] + "','" + nextLine[1]
								+ "','" + nextLine[2] + "','" + nextLine[3] + "','" + nextLine[4] + "','" + nextLine[5]
								+ "','" + nextLine[6] + "','" + nextLine[7] + "','" + nextLine[8] + "','" + nextLine[9]
								+ "','" + nextLine[10] + "','" + nextLine[11] + "')";
						first = false;
					} else {
						values += ",('" + rowid + "','" + device.getId() + "','" + nextLine[0] + "','" + nextLine[1]
								+ "','" + nextLine[2] + "','" + nextLine[3] + "','" + nextLine[4] + "','" + nextLine[5]
								+ "','" + nextLine[6] + "','" + nextLine[7] + "','" + nextLine[8] + "','" + nextLine[9]
								+ "','" + nextLine[10] + "','" + nextLine[11] + "')";
					}

					count++;
					totalcount++;
					rowid++;

					if (count >= batchSize || contLines >= linecount - 1) {
						query = "INSERT INTO  `Rohigmese`.`MeteoLisimeter` (" + "`RID`, " + "`DEVICE_ID` ,"
								+ "`MOMENT` ," + "`TLISI` ," + "`TFIELD` ," + "`TEMP_M140` ," + "`TEMP_M020` ,"
								+ "`TEMP_M010` ," + "`TEMP_M005` ," + "`TEMP_005` ," + "`TEMP_050` ," + "`TEMP_200` ,"
								+ "`RAIN` ," + "`WINDSPEED` " + ")" + " VALUES " + values + ";";

						st.executeUpdate(query);
						count = 0;
						values = "";
						first = true;
						System.out.println("DATOS INTRODUCIDOS: " + totalcount);
						System.out.println("ELAPSED TIME: " + (System.nanoTime() - startNanos));
					}

				}

				contLines++;

			}

			nanoTime = (System.nanoTime() - startNanos);

			if (exit) {
				history.setIdEnd(rowid);
				history.setAuthor(author.getName() + " " + author.getSurname());
				history.setDate(new Date(System.currentTimeMillis() - 1000));
				history.setDevice("LISIMETRO > " + device.getName() + " [" + device.getBrand() + "]");
				historyService.save(history);
			}

		} catch (SQLException ex) {
			Logger lgr = Logger.getLogger(DeviceResearcherController.class.getName());
			lgr.log(Level.SEVERE, ex.getMessage(), ex);

			final String fileError;

			if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				fileError = "Error uploading the file. Check the format and try again.";
			} else {
				fileError = "Error al subir el archivo. Compruebe el formato y vuelva a intentarlo.";
			}

			redirect.addFlashAttribute("fileError", fileError);

			error = true;

		} catch (Exception e) {
			e.printStackTrace();

			final String fileError;

			if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				fileError = "Error uploading the file. Check the format and try again.";
			} else {
				fileError = "Error al subir el archivo. Compruebe el formato y vuelva a intentarlo.";
			}

			redirect.addFlashAttribute("fileError", fileError);

			error = true;

		} finally {
			try {
				if (st != null) {
					st.close();
				}
				if (con != null) {
					con.close();
				}

				if (error == false) {

					final String fileSuccess;

					long finalTime = TimeUnit.NANOSECONDS.toMinutes(nanoTime);

					if (totalcount > 0) {
						if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
							fileSuccess = "File successfully imported.<br/><b>Rows: " + totalcount + "<br/>Total time: "
									+ finalTime + " minutes</b>";
						} else {
							fileSuccess = "Archivo importado.<br/><b>Filas: " + totalcount + "<br/>Tiempo total: "
									+ finalTime + " minutos</b>";
						}
					} else {
						if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
							fileSuccess = "Updated data.";
						} else {
							fileSuccess = "Datos ya actualizados.";
						}
					}

					redirect.addFlashAttribute("fileSuccess", fileSuccess);

				}

			} catch (SQLException ex) {
				Logger lgr = Logger.getLogger(DeviceResearcherController.class.getName());
				lgr.log(Level.WARNING, ex.getMessage(), ex);

				final String fileError;

				if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
					fileError = "Error uploading the file. Check the format and try again.";
				} else {
					fileError = "Error al subir el archivo. Compruebe el formato y vuelva a intentarlo.";
				}

				redirect.addFlashAttribute("fileError", fileError);

				error = true;

			}
		}

	}

	public static void storeWeightsLisimeter(CsvParser parser, File stationPath, final File file, final String filename,
			Researcher author, Device device, HistoryService historyService, final RedirectAttributes redirect,
			final HttpServletRequest request, String url, String user, String password) {

		File devicePath = new File(stationPath, "Lisimetro/WEIGHTS/OUTPUT/" + file.getName());
		File fullPath = new File(devicePath.getAbsolutePath());

		// Interceptor de cambio de idioma
		final LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(request);

		Connection con = null;
		Statement st = null;
		ResultSet rs = null;

		String query = "";

		int count = 0;
		int contLines = 1;
		int totalcount = 0;
		long nanoTime = 0;

		boolean error = false;

		try {
			// LECTURA DEL CSV-----------------
			parser.beginParsing(getReader(fullPath.getAbsolutePath()));
			int linecount = getLineCount(fullPath.getAbsolutePath());

			String[] nextLine;

			// SALTARSE LAS CABECERAS
			String[] firstLine = parser.parseNext();

			// CONFIGURACIONES DE LA BD
			con = DriverManager.getConnection(url, user, password);
			st = con.createStatement();

			int rowid = 0;

			rs = st.executeQuery("SELECT MAX(RID) FROM `Rohigmese`.`WeightsLisimeter`");

			while (rs.next()) {
				rowid = rs.getInt("MAX(RID)");
				rowid++;
			}

			History history = new History();

			String values = "";

			long startNanos = System.nanoTime();
			double batchSize = 0;

			if (linecount > 500) {
				batchSize = linecount * 0.02;
			} else {
				batchSize = linecount - 1;
			}

			contLines = 1;
			boolean exit = false;
			boolean first = true;

			while ((nextLine = parser.parseNext()) != null) {

				if (contLines >= rowid) {
					if (exit == false) {
						history.setIdStart(rowid);
						exit = true;
					}

					if (first == true) {
						values += "('" + rowid + "','" + device.getId() + "','" + nextLine[0] + "','" + nextLine[1]
								+ "','" + nextLine[2] + "')";
						first = false;
					} else {
						values += ",('" + rowid + "','" + device.getId() + "','" + nextLine[0] + "','" + nextLine[1]
								+ "','" + nextLine[2] + "')";
					}

					count++;
					totalcount++;
					rowid++;

					if (count >= batchSize || contLines >= linecount - 1) {
						query = "INSERT INTO  `Rohigmese`.`WeightsLisimeter` (" + "`RID`, " + "`DEVICE_ID` ,"
								+ "`MOMENT` ," + "`WPOT` ," + "`WLISI` " + ")" + " VALUES " + values + ";";

						st.executeUpdate(query);
						count = 0;
						values = "";
						first = true;
						System.out.println("DATOS INTRODUCIDOS: " + totalcount);
						System.out.println("ELAPSED TIME: " + (System.nanoTime() - startNanos));
					}

				}

				contLines++;

			}

			nanoTime = (System.nanoTime() - startNanos);

			if (exit) {
				history.setIdEnd(rowid);
				history.setAuthor(author.getName() + " " + author.getSurname());
				history.setDate(new Date(System.currentTimeMillis() - 1000));
				history.setDevice("LISIMETRO > " + device.getName() + " [" + device.getBrand() + "]");
				historyService.save(history);
			}

		} catch (SQLException ex) {
			Logger lgr = Logger.getLogger(DeviceResearcherController.class.getName());
			lgr.log(Level.SEVERE, ex.getMessage(), ex);

			final String fileError;

			if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				fileError = "Error uploading the file. Check the format and try again.";
			} else {
				fileError = "Error al subir el archivo. Compruebe el formato y vuelva a intentarlo.";
			}

			redirect.addFlashAttribute("fileError", fileError);

			error = true;

		} catch (Exception e) {
			e.printStackTrace();

			final String fileError;

			if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				fileError = "Error uploading the file. Check the format and try again.";
			} else {
				fileError = "Error al subir el archivo. Compruebe el formato y vuelva a intentarlo.";
			}

			redirect.addFlashAttribute("fileError", fileError);

			error = true;

		} finally {
			try {
				if (st != null) {
					st.close();
				}
				if (con != null) {
					con.close();
				}

				if (error == false) {

					final String fileSuccess;

					long finalTime = TimeUnit.NANOSECONDS.toMinutes(nanoTime);

					if (totalcount > 0) {
						if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
							fileSuccess = "File successfully imported.<br/><b>Rows: " + totalcount + "<br/>Total time: "
									+ finalTime + " minutes</b>";
						} else {
							fileSuccess = "Archivo importado.<br/><b>Filas: " + totalcount + "<br/>Tiempo total: "
									+ finalTime + " minutos</b>";
						}
					} else {
						if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
							fileSuccess = "Updated data.";
						} else {
							fileSuccess = "Datos ya actualizados.";
						}
					}

					redirect.addFlashAttribute("fileSuccess", fileSuccess);

				}

			} catch (SQLException ex) {
				Logger lgr = Logger.getLogger(DeviceResearcherController.class.getName());
				lgr.log(Level.WARNING, ex.getMessage(), ex);

				final String fileError;

				if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
					fileError = "Error uploading the file. Check the format and try again.";
				} else {
					fileError = "Error al subir el archivo. Compruebe el formato y vuelva a intentarlo.";
				}

				redirect.addFlashAttribute("fileError", fileError);

				error = true;

			}
		}

	}

	public static void storeCampbellLisimeter(CsvParser parser, File stationPath, final File file,
			final String filename, Researcher author, Device device, HistoryService historyService,
			final RedirectAttributes redirect, final HttpServletRequest request, String url, String user,
			String password) {

		File devicePath = new File(stationPath, "Campbell/OUTPUT/" + file.getName());
		File fullPath = new File(devicePath.getAbsolutePath());

		// Interceptor de cambio de idioma
		final LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(request);

		Connection con = null;
		Statement st = null;
		ResultSet rs = null;

		String query = "";

		int count = 0;
		int contLines = 1;
		int totalcount = 0;
		long nanoTime = 0;

		boolean error = false;

		try {
			// LECTURA DEL CSV-----------------
			parser.beginParsing(getReader(fullPath.getAbsolutePath()));
			int linecount = getLineCount(fullPath.getAbsolutePath());

			String[] nextLine;

			// SALTARSE LAS CABECERAS
			String[] firstLine = parser.parseNext();

			// CONFIGURACIONES DE LA BD
			con = DriverManager.getConnection(url, user, password);
			st = con.createStatement();

			int rowid = 0;

			rs = st.executeQuery("SELECT MAX(RID) FROM `Rohigmese`.`CampbellLisimeter`");

			while (rs.next()) {
				rowid = rs.getInt("MAX(RID)");
				rowid++;
			}

			History history = new History();

			String values = "";

			long startNanos = System.nanoTime();
			double batchSize = 0;

			if (linecount > 500) {
				batchSize = linecount * 0.02;
			} else {
				batchSize = linecount - 1;
			}

			contLines = 1;
			boolean exit = false;
			boolean first = true;

			while ((nextLine = parser.parseNext()) != null) {

				if (contLines >= rowid) {
					if (exit == false) {
						history.setIdStart(rowid);
						exit = true;
					}

					if (first == true) {
						values += "('" + rowid + "','" + device.getId() + "','" + nextLine[0] + "','" + nextLine[2]
								+ "','" + nextLine[3] + "','" + nextLine[4] + "','" + nextLine[5] + "','" + nextLine[6]
								+ "','" + nextLine[7] + "','" + nextLine[8] + "','" + nextLine[9] + "','" + nextLine[10]
								+ "','" + nextLine[11] + "','" + nextLine[12] + "','" + nextLine[13] + "','"
								+ nextLine[14] + "','" + nextLine[15] + "','" + nextLine[16] + "','" + nextLine[17]
								+ "','" + nextLine[18] + "','" + nextLine[19] + "')";
						first = false;
					} else {
						values += ",('" + rowid + "','" + device.getId() + "','" + nextLine[0] + "','" + nextLine[2]
								+ "','" + nextLine[3] + "','" + nextLine[4] + "','" + nextLine[5] + "','" + nextLine[6]
								+ "','" + nextLine[7] + "','" + nextLine[8] + "','" + nextLine[9] + "','" + nextLine[10]
								+ "','" + nextLine[11] + "','" + nextLine[12] + "','" + nextLine[13] + "','"
								+ nextLine[14] + "','" + nextLine[15] + "','" + nextLine[16] + "','" + nextLine[17]
								+ "','" + nextLine[18] + "','" + nextLine[19] + "')";
					}

					count++;
					totalcount++;
					rowid++;

					if (count >= batchSize || contLines >= linecount - 1) {
						query = "INSERT INTO  `Rohigmese`.`CampbellLisimeter` (" + "`RID`, " + "`DEVICE_ID` ,"
								+ "`MOMENT` ," + "`VWC_P1` ," + "`EC_P1` ," + "`TEMP_P1` ," + "`VWC_P2` ," + "`EC_P2` ,"
								+ "`TEMP_P2` ," + "`VWC_P3` ," + "`EC_P3` ," + "`TEMP_P3` ," + "`VWC_P4` ,"
								+ "`EC_P4` ," + "`TEMP_P4` ," + "`VWC_P5` ," + "`EC_P5` ," + "`TEMP_P5` ,"
								+ "`VWC_P6` ," + "`EC_P6` ," + "`TEMP_P6`" + ")" + " VALUES " + values + ";";

						st.executeUpdate(query);
						count = 0;
						values = "";
						first = true;
						System.out.println("DATOS INTRODUCIDOS: " + totalcount);
						System.out.println("ELAPSED TIME: " + (System.nanoTime() - startNanos));
					}

				}

				contLines++;

			}

			nanoTime = (System.nanoTime() - startNanos);

			if (exit) {
				history.setIdEnd(rowid);
				history.setAuthor(author.getName() + " " + author.getSurname());
				history.setDate(new Date(System.currentTimeMillis() - 1000));
				history.setDevice("LISIMETRO > " + device.getName() + " [" + device.getBrand() + "]");
				historyService.save(history);
			}

		} catch (SQLException ex) {
			Logger lgr = Logger.getLogger(DeviceResearcherController.class.getName());
			lgr.log(Level.SEVERE, ex.getMessage(), ex);

			final String fileError;

			if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				fileError = "Error uploading the file. Check the format and try again.";
			} else {
				fileError = "Error al subir el archivo. Compruebe el formato y vuelva a intentarlo.";
			}

			redirect.addFlashAttribute("fileError", fileError);

			error = true;

		} catch (Exception e) {
			e.printStackTrace();

			final String fileError;

			if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				fileError = "Error uploading the file. Check the format and try again.";
			} else {
				fileError = "Error al subir el archivo. Compruebe el formato y vuelva a intentarlo.";
			}

			redirect.addFlashAttribute("fileError", fileError);

			error = true;

		} finally {
			try {
				if (st != null) {
					st.close();
				}
				if (con != null) {
					con.close();
				}

				if (error == false) {

					final String fileSuccess;

					long finalTime = TimeUnit.NANOSECONDS.toMinutes(nanoTime);

					if (totalcount > 0) {
						if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
							fileSuccess = "File successfully imported.<br/><b>Rows: " + totalcount + "<br/>Total time: "
									+ finalTime + " minutes</b>";
						} else {
							fileSuccess = "Archivo importado.<br/><b>Filas: " + totalcount + "<br/>Tiempo total: "
									+ finalTime + " minutos</b>";
						}
					} else {
						if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
							fileSuccess = "Updated data.";
						} else {
							fileSuccess = "Datos ya actualizados.";
						}
					}

					redirect.addFlashAttribute("fileSuccess", fileSuccess);

				}

			} catch (SQLException ex) {
				Logger lgr = Logger.getLogger(DeviceResearcherController.class.getName());
				lgr.log(Level.WARNING, ex.getMessage(), ex);

				final String fileError;

				if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
					fileError = "Error uploading the file. Check the format and try again.";
				} else {
					fileError = "Error al subir el archivo. Compruebe el formato y vuelva a intentarlo.";
				}

				redirect.addFlashAttribute("fileError", fileError);

				error = true;

			}
		}

	}

	public static void storeDavisLisimeter(CsvParser parser, File stationPath, final File file, final String filename,
			Researcher author, Device device, HistoryService historyService, final RedirectAttributes redirect,
			final HttpServletRequest request, String url, String user, String password) {

		File devicePath = new File(stationPath, "Davis/OUTPUT/" + file.getName());
		File fullPath = new File(devicePath.getAbsolutePath());

		// Interceptor de cambio de idioma
		final LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(request);

		Connection con = null;
		Statement st = null;
		ResultSet rs = null;

		String query = "";

		int count = 0;
		int contLines = 1;
		int totalcount = 0;
		long nanoTime = 0;

		boolean error = false;

		try {
			// LECTURA DEL CSV-----------------
			parser.beginParsing(getReader(fullPath.getAbsolutePath()));
			int linecount = getLineCount(fullPath.getAbsolutePath());

			String[] nextLine;

			// SALTARSE LAS CABECERAS
			String[] firstLine = parser.parseNext();

			// CONFIGURACIONES DE LA BD
			con = DriverManager.getConnection(url, user, password);
			st = con.createStatement();

			int rowid = 0;

			rs = st.executeQuery("SELECT MAX(RID) FROM `Rohigmese`.`DavisLisimeter`");

			while (rs.next()) {
				rowid = rs.getInt("MAX(RID)");
				rowid++;
			}

			History history = new History();

			String values = "";

			long startNanos = System.nanoTime();
			double batchSize = 0;

			if (linecount > 500) {
				batchSize = linecount * 0.02;
			} else {
				batchSize = linecount - 1;
			}

			contLines = 1;
			boolean exit = false;
			boolean first = true;

			while ((nextLine = parser.parseNext()) != null) {

				if (contLines >= rowid) {
					if (exit == false) {
						history.setIdStart(rowid);
						exit = true;
					}

					if (first == true) {
						values += "('" + rowid + "','" + device.getId() + "','" + nextLine[0] + "','" + nextLine[1]
								+ "','" + nextLine[2] + "','" + nextLine[3] + "','" + nextLine[4] + "','" + nextLine[5]
								+ "')";
						first = false;
					} else {
						values += ",('" + rowid + "','" + device.getId() + "','" + nextLine[0] + "','" + nextLine[1]
								+ "','" + nextLine[2] + "','" + nextLine[3] + "','" + nextLine[4] + "','" + nextLine[5]
								+ "')";
					}

					count++;
					totalcount++;
					rowid++;

					if (count >= batchSize || contLines >= linecount - 1) {
						query = "INSERT INTO  `Rohigmese`.`DavisLisimeter` (" + "`RID`, " + "`DEVICE_ID` ,"
								+ "`MOMENT` ," + "`TEMP` ," + "`HUMIDITY` ," + "`WINDSPEED` ," + "`BAR` ," + "`RAIN`"
								+ ")" + " VALUES " + values + ";";

						st.executeUpdate(query);
						count = 0;
						values = "";
						first = true;
						System.out.println("DATOS INTRODUCIDOS: " + totalcount);
						System.out.println("ELAPSED TIME: " + (System.nanoTime() - startNanos));
					}

				}

				contLines++;

			}

			nanoTime = (System.nanoTime() - startNanos);

			if (exit) {
				history.setIdEnd(rowid);
				history.setAuthor(author.getName() + " " + author.getSurname());
				history.setDate(new Date(System.currentTimeMillis() - 1000));
				history.setDevice("LISIMETRO > " + device.getName() + " [" + device.getBrand() + "]");
				historyService.save(history);
			}

		} catch (SQLException ex) {
			Logger lgr = Logger.getLogger(DeviceResearcherController.class.getName());
			lgr.log(Level.SEVERE, ex.getMessage(), ex);

			final String fileError;

			if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				fileError = "Error uploading the file. Check the format and try again.";
			} else {
				fileError = "Error al subir el archivo. Compruebe el formato y vuelva a intentarlo.";
			}

			redirect.addFlashAttribute("fileError", fileError);

			error = true;

		} catch (Exception e) {
			e.printStackTrace();

			final String fileError;

			if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				fileError = "Error uploading the file. Check the format and try again.";
			} else {
				fileError = "Error al subir el archivo. Compruebe el formato y vuelva a intentarlo.";
			}

			redirect.addFlashAttribute("fileError", fileError);

			error = true;

		} finally {
			try {
				if (st != null) {
					st.close();
				}
				if (con != null) {
					con.close();
				}

				if (error == false) {

					final String fileSuccess;

					long finalTime = TimeUnit.NANOSECONDS.toMinutes(nanoTime);

					if (totalcount > 0) {
						if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
							fileSuccess = "File successfully imported.<br/><b>Rows: " + totalcount + "<br/>Total time: "
									+ finalTime + " minutes</b>";
						} else {
							fileSuccess = "Archivo importado.<br/><b>Filas: " + totalcount + "<br/>Tiempo total: "
									+ finalTime + " minutos</b>";
						}
					} else {
						if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
							fileSuccess = "Updated data.";
						} else {
							fileSuccess = "Datos ya actualizados.";
						}
					}

					redirect.addFlashAttribute("fileSuccess", fileSuccess);

				}

			} catch (SQLException ex) {
				Logger lgr = Logger.getLogger(DeviceResearcherController.class.getName());
				lgr.log(Level.WARNING, ex.getMessage(), ex);

				final String fileError;

				if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
					fileError = "Error uploading the file. Check the format and try again.";
				} else {
					fileError = "Error al subir el archivo. Compruebe el formato y vuelva a intentarlo.";
				}

				redirect.addFlashAttribute("fileError", fileError);

				error = true;

			}
		}

	}

	public static void storeHelmanLisimeter(CsvParser parser, File stationPath, final File file, final String filename,
			Researcher author, Device device, HistoryService historyService, final RedirectAttributes redirect,
			final HttpServletRequest request, String url, String user, String password) {

		File devicePath = new File(stationPath, "Helman/OUTPUT/" + file.getName());
		File fullPath = new File(devicePath.getAbsolutePath());

		// Interceptor de cambio de idioma
		final LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(request);

		Connection con = null;
		Statement st = null;
		ResultSet rs = null;

		String query = "";

		int count = 0;
		int contLines = 1;
		int totalcount = 0;
		long nanoTime = 0;

		boolean error = false;

		try {
			// LECTURA DEL CSV-----------------
			parser.beginParsing(getReader(fullPath.getAbsolutePath()));
			int linecount = getLineCount(fullPath.getAbsolutePath());

			String[] nextLine;

			// SALTARSE LAS CABECERAS
			String[] firstLine = parser.parseNext();

			// CONFIGURACIONES DE LA BD
			con = DriverManager.getConnection(url, user, password);
			st = con.createStatement();

			int rowid = 0;

			rs = st.executeQuery("SELECT MAX(RID) FROM `Rohigmese`.`HelmanLisimeter`");

			while (rs.next()) {
				rowid = rs.getInt("MAX(RID)");
				rowid++;
			}

			History history = new History();

			String values = "";

			long startNanos = System.nanoTime();
			double batchSize = 0;

			if (linecount > 500) {
				batchSize = linecount * 0.02;
			} else {
				batchSize = linecount - 1;
			}

			contLines = 1;
			boolean exit = false;
			boolean first = true;

			while ((nextLine = parser.parseNext()) != null) {

				if (contLines >= rowid) {
					if (exit == false) {
						history.setIdStart(rowid);
						exit = true;
					}

					if (first == true) {
						values += "('" + rowid + "','" + device.getId() + "','" + nextLine[0] + "','" + nextLine[1]
								+ "')";
						first = false;
					} else {
						values += ",('" + rowid + "','" + device.getId() + "','" + nextLine[0] + "','" + nextLine[1]
								+ "')";
					}

					count++;
					totalcount++;
					rowid++;

					if (count >= batchSize || contLines >= linecount - 1) {
						query = "INSERT INTO  `Rohigmese`.`HelmanLisimeter` (" + "`RID`, " + "`DEVICE_ID` ,"
								+ "`MOMENT` ," + "`RAIN`)" + " VALUES " + values + ";";

						st.executeUpdate(query);
						count = 0;
						values = "";
						first = true;
						System.out.println("DATOS INTRODUCIDOS: " + totalcount);
						System.out.println("ELAPSED TIME: " + (System.nanoTime() - startNanos));
					}

				}

				contLines++;

			}

			nanoTime = (System.nanoTime() - startNanos);

			if (exit) {
				history.setIdEnd(rowid);
				history.setAuthor(author.getName() + " " + author.getSurname());
				history.setDate(new Date(System.currentTimeMillis() - 1000));
				history.setDevice("LISIMETRO > " + device.getName() + " [" + device.getBrand() + "]");
				historyService.save(history);
			}

		} catch (SQLException ex) {
			Logger lgr = Logger.getLogger(DeviceResearcherController.class.getName());
			lgr.log(Level.SEVERE, ex.getMessage(), ex);

			final String fileError;

			if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				fileError = "Error uploading the file. Check the format and try again.";
			} else {
				fileError = "Error al subir el archivo. Compruebe el formato y vuelva a intentarlo.";
			}

			redirect.addFlashAttribute("fileError", fileError);

			error = true;

		} catch (Exception e) {
			e.printStackTrace();

			final String fileError;

			if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				fileError = "Error uploading the file. Check the format and try again.";
			} else {
				fileError = "Error al subir el archivo. Compruebe el formato y vuelva a intentarlo.";
			}

			redirect.addFlashAttribute("fileError", fileError);

			error = true;

		} finally {
			try {
				if (st != null) {
					st.close();
				}
				if (con != null) {
					con.close();
				}

				if (error == false) {

					final String fileSuccess;

					long finalTime = TimeUnit.NANOSECONDS.toMinutes(nanoTime);

					if (totalcount > 0) {
						if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
							fileSuccess = "File successfully imported.<br/><b>Rows: " + totalcount + "<br/>Total time: "
									+ finalTime + " minutes</b>";
						} else {
							fileSuccess = "Archivo importado.<br/><b>Filas: " + totalcount + "<br/>Tiempo total: "
									+ finalTime + " minutos</b>";
						}
					} else {
						if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
							fileSuccess = "Updated data.";
						} else {
							fileSuccess = "Datos ya actualizados.";
						}
					}

					redirect.addFlashAttribute("fileSuccess", fileSuccess);

				}

			} catch (SQLException ex) {
				Logger lgr = Logger.getLogger(DeviceResearcherController.class.getName());
				lgr.log(Level.WARNING, ex.getMessage(), ex);

				final String fileError;

				if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
					fileError = "Error uploading the file. Check the format and try again.";
				} else {
					fileError = "Error al subir el archivo. Compruebe el formato y vuelva a intentarlo.";
				}

				redirect.addFlashAttribute("fileError", fileError);

				error = true;

			}
		}

	}

	public static void storeCampbellDT1C7(CsvParser parser, File stationPath, final File file, final String filename,
			Researcher author, Device device, HistoryService historyService, final RedirectAttributes redirect,
			final HttpServletRequest request, String url, String user, String password) {

		File devicePath = new File(stationPath, "Campbell/C7 DT1/OUTPUT/" + file.getName());
		File fullPath = new File(devicePath.getAbsolutePath());

		// Interceptor de cambio de idioma
		final LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(request);

		Connection con = null;
		Statement st = null;
		ResultSet rs = null;

		String query = "";

		int count = 0;
		int contLines = 1;
		int totalcount = 0;
		long nanoTime = 0;

		boolean error = false;

		try {
			// LECTURA DEL CSV-----------------
			parser.beginParsing(getReader(fullPath.getAbsolutePath()));
			int linecount = getLineCount(fullPath.getAbsolutePath());

			String[] nextLine;

			// SALTARSE LAS CABECERAS
			String[] firstLine = parser.parseNext();

			// CONFIGURACIONES DE LA BD
			con = DriverManager.getConnection(url, user, password);
			st = con.createStatement();

			int rowid = 0;

			rs = st.executeQuery("SELECT MAX(RID) FROM `Rohigmese`.`CampbellC7DT1`");

			while (rs.next()) {
				rowid = rs.getInt("MAX(RID)");
				rowid++;
			}

			History history = new History();

			String values = "";

			long startNanos = System.nanoTime();
			double batchSize = 0;

			if (linecount > 500) {
				batchSize = linecount * 0.02;
			} else {
				batchSize = linecount - 1;
			}

			contLines = 1;
			boolean exit = false;
			boolean first = true;

			while ((nextLine = parser.parseNext()) != null) {

				if (contLines >= rowid) {
					if (exit == false) {
						history.setIdStart(rowid);
						exit = true;
					}

					if (first == true) {
						values += "('" + rowid + "','" + device.getId() + "','" + nextLine[0] + "','" + nextLine[2]
								+ "','" + nextLine[3] + "','" + nextLine[4] + "','" + nextLine[5] + "','" + nextLine[6]
								+ "','" + nextLine[7] + "','" + nextLine[8] + "','" + nextLine[9] + "','" + nextLine[10]
								+ "','" + nextLine[11] + "','" + nextLine[12] + "','" + nextLine[13] + "','"
								+ nextLine[14] + "','" + nextLine[15] + "','" + nextLine[16] + "','" + nextLine[17]
								+ "','" + nextLine[18] + "','" + nextLine[19] + "')";
						first = false;
					} else {
						values += ",('" + rowid + "','" + device.getId() + "','" + nextLine[0] + "','" + nextLine[2]
								+ "','" + nextLine[3] + "','" + nextLine[4] + "','" + nextLine[5] + "','" + nextLine[6]
								+ "','" + nextLine[7] + "','" + nextLine[8] + "','" + nextLine[9] + "','" + nextLine[10]
								+ "','" + nextLine[11] + "','" + nextLine[12] + "','" + nextLine[13] + "','"
								+ nextLine[14] + "','" + nextLine[15] + "','" + nextLine[16] + "','" + nextLine[17]
								+ "','" + nextLine[18] + "','" + nextLine[19] + "')";
					}

					count++;
					totalcount++;
					rowid++;

					if (count >= batchSize || contLines >= linecount - 1) {
						query = "INSERT INTO  `Rohigmese`.`CampbellC7DT1` (" + "`RID`, " + "`DEVICE_ID` ,"
								+ "`MOMENT` ," + "`VWC_P1` ," + "`EC_P1` ," + "`TEMP_P1` ," + "`VWC_P2` ," + "`EC_P2` ,"
								+ "`TEMP_P2` ," + "`VWC_P3` ," + "`EC_P3` ," + "`TEMP_P3` ," + "`VWC_P4` ,"
								+ "`EC_P4` ," + "`TEMP_P4` ," + "`VWC_P5` ," + "`EC_P5` ," + "`TEMP_P5` ,"
								+ "`VWC_P6` ," + "`EC_P6` ," + "`TEMP_P6`" + ")" + " VALUES " + values + ";";
						st.executeUpdate(query);
						count = 0;
						values = "";
						first = true;
						System.out.println("DATOS INTRODUCIDOS: " + totalcount);
						System.out.println("ELAPSED TIME: " + (System.nanoTime() - startNanos));
					}

				}

				contLines++;

			}

			nanoTime = (System.nanoTime() - startNanos);

			if (exit) {
				history.setIdEnd(rowid);
				history.setAuthor(author.getName() + " " + author.getSurname());
				history.setDate(new Date(System.currentTimeMillis() - 1000));
				history.setDevice("MARISMILLAS-CORTAFUEGOS 7 > " + device.getName() + " [" + device.getBrand() + "]");
				historyService.save(history);
			}

		} catch (SQLException ex) {
			Logger lgr = Logger.getLogger(DeviceResearcherController.class.getName());
			lgr.log(Level.SEVERE, ex.getMessage(), ex);

			final String fileError;

			if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				fileError = "Error uploading the file. Check the format and try again.";
			} else {
				fileError = "Error al subir el archivo. Compruebe el formato y vuelva a intentarlo.";
			}

			redirect.addFlashAttribute("fileError", fileError);

			error = true;

		} catch (Exception e) {
			e.printStackTrace();

			final String fileError;

			if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				fileError = "Error uploading the file. Check the format and try again.";
			} else {
				fileError = "Error al subir el archivo. Compruebe el formato y vuelva a intentarlo.";
			}

			redirect.addFlashAttribute("fileError", fileError);

			error = true;

		} finally {
			try {
				if (st != null) {
					st.close();
				}
				if (con != null) {
					con.close();
				}

				if (error == false) {

					final String fileSuccess;

					long finalTime = TimeUnit.NANOSECONDS.toMinutes(nanoTime);

					if (totalcount > 0) {
						if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
							fileSuccess = "File successfully imported.<br/><b>Rows: " + totalcount + "<br/>Total time: "
									+ finalTime + " minutes</b>";
						} else {
							fileSuccess = "Archivo importado.<br/><b>Filas: " + totalcount + "<br/>Tiempo total: "
									+ finalTime + " minutos</b>";
						}
					} else {
						if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
							fileSuccess = "Updated data.";
						} else {
							fileSuccess = "Datos ya actualizados.";
						}
					}

					redirect.addFlashAttribute("fileSuccess", fileSuccess);

				}

			} catch (SQLException ex) {
				Logger lgr = Logger.getLogger(DeviceResearcherController.class.getName());
				lgr.log(Level.WARNING, ex.getMessage(), ex);

				final String fileError;

				if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
					fileError = "Error uploading the file. Check the format and try again.";
				} else {
					fileError = "Error al subir el archivo. Compruebe el formato y vuelva a intentarlo.";
				}

				redirect.addFlashAttribute("fileError", fileError);

				error = true;

			}
		}

	}

	public static void storeCampbellDT2C7(CsvParser parser, File stationPath, final File file, final String filename,
			Researcher author, Device device, HistoryService historyService, final RedirectAttributes redirect,
			final HttpServletRequest request, String url, String user, String password) {

		File devicePath = new File(stationPath, "Campbell/C7 DT2/OUTPUT/" + file.getName());
		File fullPath = new File(devicePath.getAbsolutePath());

		// Interceptor de cambio de idioma
		final LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(request);

		Connection con = null;
		Statement st = null;
		ResultSet rs = null;

		String query = "";

		int count = 0;
		int contLines = 1;
		int totalcount = 0;
		long nanoTime = 0;

		boolean error = false;

		try {
			// LECTURA DEL CSV-----------------
			parser.beginParsing(getReader(fullPath.getAbsolutePath()));
			int linecount = getLineCount(fullPath.getAbsolutePath());

			String[] nextLine;

			// SALTARSE LAS CABECERAS
			String[] firstLine = parser.parseNext();

			// CONFIGURACIONES DE LA BD
			con = DriverManager.getConnection(url, user, password);
			st = con.createStatement();

			int rowid = 0;

			rs = st.executeQuery("SELECT MAX(RID) FROM `Rohigmese`.`CampbellC7DT2`");

			while (rs.next()) {
				rowid = rs.getInt("MAX(RID)");
				rowid++;
			}

			History history = new History();

			String values = "";

			long startNanos = System.nanoTime();
			double batchSize = 0;

			if (linecount > 500) {
				batchSize = linecount * 0.02;
			} else {
				batchSize = linecount - 1;
			}

			contLines = 1;
			boolean exit = false;
			boolean first = true;

			while ((nextLine = parser.parseNext()) != null) {

				if (contLines >= rowid) {
					if (exit == false) {
						history.setIdStart(rowid);
						exit = true;
					}

					if (first == true) {
						values += "('" + rowid + "','" + device.getId() + "','" + nextLine[0] + "','" + nextLine[2]
								+ "','" + nextLine[3] + "','" + nextLine[4] + "','" + nextLine[5] + "','" + nextLine[6]
								+ "','" + nextLine[7] + "','" + nextLine[8] + "','" + nextLine[9] + "','" + nextLine[10]
								+ "','" + nextLine[11] + "','" + nextLine[12] + "','" + nextLine[13] + "','"
								+ nextLine[14] + "','" + nextLine[15] + "','" + nextLine[16] + "','" + nextLine[17]
								+ "','" + nextLine[18] + "','" + nextLine[19] + "')";
						first = false;
					} else {
						values += ",('" + rowid + "','" + device.getId() + "','" + nextLine[0] + "','" + nextLine[2]
								+ "','" + nextLine[3] + "','" + nextLine[4] + "','" + nextLine[5] + "','" + nextLine[6]
								+ "','" + nextLine[7] + "','" + nextLine[8] + "','" + nextLine[9] + "','" + nextLine[10]
								+ "','" + nextLine[11] + "','" + nextLine[12] + "','" + nextLine[13] + "','"
								+ nextLine[14] + "','" + nextLine[15] + "','" + nextLine[16] + "','" + nextLine[17]
								+ "','" + nextLine[18] + "','" + nextLine[19] + "')";
					}

					count++;
					totalcount++;
					rowid++;

					if (count >= batchSize || contLines >= linecount - 1) {
						query = "INSERT INTO  `Rohigmese`.`CampbellC7DT2` (" + "`RID`, " + "`DEVICE_ID` ,"
								+ "`MOMENT` ," + "`VWC_P1` ," + "`EC_P1` ," + "`TEMP_P1` ," + "`VWC_P2` ," + "`EC_P2` ,"
								+ "`TEMP_P2` ," + "`VWC_P3` ," + "`EC_P3` ," + "`TEMP_P3` ," + "`VWC_P4` ,"
								+ "`EC_P4` ," + "`TEMP_P4` ," + "`VWC_P5` ," + "`EC_P5` ," + "`TEMP_P5` ,"
								+ "`VWC_P6` ," + "`EC_P6` ," + "`TEMP_P6`" + ")" + " VALUES " + values + ";";
						st.executeUpdate(query);
						count = 0;
						values = "";
						first = true;
						System.out.println("DATOS INTRODUCIDOS: " + totalcount);
						System.out.println("ELAPSED TIME: " + (System.nanoTime() - startNanos));
					}

				}

				contLines++;

			}

			nanoTime = (System.nanoTime() - startNanos);

			if (exit) {
				history.setIdEnd(rowid);
				history.setAuthor(author.getName() + " " + author.getSurname());
				history.setDate(new Date(System.currentTimeMillis() - 1000));
				history.setDevice("MARISMILLAS-CORTAFUEGOS 7 > " + device.getName() + " [" + device.getBrand() + "]");
				historyService.save(history);
			}

		} catch (SQLException ex) {
			Logger lgr = Logger.getLogger(DeviceResearcherController.class.getName());
			lgr.log(Level.SEVERE, ex.getMessage(), ex);

			final String fileError;

			if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				fileError = "Error uploading the file. Check the format and try again.";
			} else {
				fileError = "Error al subir el archivo. Compruebe el formato y vuelva a intentarlo.";
			}

			redirect.addFlashAttribute("fileError", fileError);

			error = true;

		} catch (Exception e) {
			e.printStackTrace();

			final String fileError;

			if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				fileError = "Error uploading the file. Check the format and try again.";
			} else {
				fileError = "Error al subir el archivo. Compruebe el formato y vuelva a intentarlo.";
			}

			redirect.addFlashAttribute("fileError", fileError);

			error = true;

		} finally {
			try {
				if (st != null) {
					st.close();
				}
				if (con != null) {
					con.close();
				}

				if (error == false) {

					final String fileSuccess;

					long finalTime = TimeUnit.NANOSECONDS.toMinutes(nanoTime);

					if (totalcount > 0) {
						if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
							fileSuccess = "File successfully imported.<br/><b>Rows: " + totalcount + "<br/>Total time: "
									+ finalTime + " minutes</b>";
						} else {
							fileSuccess = "Archivo importado.<br/><b>Filas: " + totalcount + "<br/>Tiempo total: "
									+ finalTime + " minutos</b>";
						}
					} else {
						if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
							fileSuccess = "Updated data.";
						} else {
							fileSuccess = "Datos ya actualizados.";
						}
					}

					redirect.addFlashAttribute("fileSuccess", fileSuccess);

				}

			} catch (SQLException ex) {
				Logger lgr = Logger.getLogger(DeviceResearcherController.class.getName());
				lgr.log(Level.WARNING, ex.getMessage(), ex);

				final String fileError;

				if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
					fileError = "Error uploading the file. Check the format and try again.";
				} else {
					fileError = "Error al subir el archivo. Compruebe el formato y vuelva a intentarlo.";
				}

				redirect.addFlashAttribute("fileError", fileError);

				error = true;

			}
		}

	}

	public static void storeDavisC7(CsvParser parser, File stationPath, final File file, final String filename,
			Researcher author, Device device, HistoryService historyService, final RedirectAttributes redirect,
			final HttpServletRequest request, String url, String user, String password) {

		File devicePath = new File(stationPath, "Davis/C7/OUTPUT/" + file.getName());
		File fullPath = new File(devicePath.getAbsolutePath());

		// Interceptor de cambio de idioma
		final LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(request);

		Connection con = null;
		Statement st = null;
		ResultSet rs = null;

		String query = "";

		int count = 0;
		int contLines = 1;
		int totalcount = 0;
		long nanoTime = 0;

		boolean error = false;

		try {
			// LECTURA DEL CSV-----------------
			parser.beginParsing(getReader(fullPath.getAbsolutePath()));
			int linecount = getLineCount(fullPath.getAbsolutePath());

			String[] nextLine;

			// SALTARSE LAS CABECERAS
			String[] firstLine = parser.parseNext();

			// CONFIGURACIONES DE LA BD
			con = DriverManager.getConnection(url, user, password);
			st = con.createStatement();

			int rowid = 0;

			rs = st.executeQuery("SELECT MAX(RID) FROM `Rohigmese`.`DavisC7`");

			while (rs.next()) {
				rowid = rs.getInt("MAX(RID)");
				rowid++;
			}

			History history = new History();

			String values = "";

			long startNanos = System.nanoTime();
			double batchSize = 0;

			if (linecount > 500) {
				batchSize = linecount * 0.02;
			} else {
				batchSize = linecount - 1;
			}

			contLines = 1;
			boolean exit = false;
			boolean first = true;

			while ((nextLine = parser.parseNext()) != null) {

				if (contLines >= rowid) {
					if (exit == false) {
						history.setIdStart(rowid);
						exit = true;
					}

					if (first == true) {
						values += "('" + rowid + "','" + device.getId() + "','" + nextLine[0] + "','" + nextLine[1]
								+ "','" + nextLine[2] + "','" + nextLine[3] + "','" + nextLine[4] + "','" + nextLine[5]
								+ "')";
						first = false;
					} else {
						values += ",('" + rowid + "','" + device.getId() + "','" + nextLine[0] + "','" + nextLine[1]
								+ "','" + nextLine[2] + "','" + nextLine[3] + "','" + nextLine[4] + "','" + nextLine[5]
								+ "')";
					}

					count++;
					totalcount++;
					rowid++;

					if (count >= batchSize || contLines >= linecount - 1) {
						query = "INSERT INTO  `Rohigmese`.`DavisC7` (" + "`RID`, " + "`DEVICE_ID` ," + "`MOMENT` ,"
								+ "`TEMP` ," + "`HUMIDITY` ," + "`WINDSPEED` ," + "`BAR` ," + "`RAIN`" + ")"
								+ " VALUES " + values + ";";
						st.executeUpdate(query);
						count = 0;
						values = "";
						first = true;
						System.out.println("DATOS INTRODUCIDOS: " + totalcount);
						System.out.println("ELAPSED TIME: " + (System.nanoTime() - startNanos));
					}

				}

				contLines++;

			}

			nanoTime = (System.nanoTime() - startNanos);

			if (exit) {
				history.setIdEnd(rowid);
				history.setAuthor(author.getName() + " " + author.getSurname());
				history.setDate(new Date(System.currentTimeMillis() - 1000));
				history.setDevice("MARISMILLAS-CORTAFUEGOS 7 > " + device.getName() + " [" + device.getBrand() + "]");
				historyService.save(history);
			}

		} catch (SQLException ex) {
			Logger lgr = Logger.getLogger(DeviceResearcherController.class.getName());
			lgr.log(Level.SEVERE, ex.getMessage(), ex);

			final String fileError;

			if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				fileError = "Error uploading the file. Check the format and try again.";
			} else {
				fileError = "Error al subir el archivo. Compruebe el formato y vuelva a intentarlo.";
			}

			redirect.addFlashAttribute("fileError", fileError);

			error = true;

		} catch (Exception e) {
			e.printStackTrace();

			final String fileError;

			if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				fileError = "Error uploading the file. Check the format and try again.";
			} else {
				fileError = "Error al subir el archivo. Compruebe el formato y vuelva a intentarlo.";
			}

			redirect.addFlashAttribute("fileError", fileError);

			error = true;

		} finally {
			try {
				if (st != null) {
					st.close();
				}
				if (con != null) {
					con.close();
				}

				if (error == false) {

					final String fileSuccess;

					long finalTime = TimeUnit.NANOSECONDS.toMinutes(nanoTime);

					if (totalcount > 0) {
						if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
							fileSuccess = "File successfully imported.<br/><b>Rows: " + totalcount + "<br/>Total time: "
									+ finalTime + " minutes</b>";
						} else {
							fileSuccess = "Archivo importado.<br/><b>Filas: " + totalcount + "<br/>Tiempo total: "
									+ finalTime + " minutos</b>";
						}
					} else {
						if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
							fileSuccess = "Updated data.";
						} else {
							fileSuccess = "Datos ya actualizados.";
						}
					}

					redirect.addFlashAttribute("fileSuccess", fileSuccess);

				}

			} catch (SQLException ex) {
				Logger lgr = Logger.getLogger(DeviceResearcherController.class.getName());
				lgr.log(Level.WARNING, ex.getMessage(), ex);

				final String fileError;

				if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
					fileError = "Error uploading the file. Check the format and try again.";
				} else {
					fileError = "Error al subir el archivo. Compruebe el formato y vuelva a intentarlo.";
				}

				redirect.addFlashAttribute("fileError", fileError);

				error = true;

			}
		}

	}

	public static void storeDavisLlanos(CsvParser parser, File stationPath, final File file, final String filename,
			Researcher author, Device device, HistoryService historyService, final RedirectAttributes redirect,
			final HttpServletRequest request, String url, String user, String password) {

		File devicePath = new File(stationPath, "Davis/LLANOS/OUTPUT/" + file.getName());
		File fullPath = new File(devicePath.getAbsolutePath());

		// Interceptor de cambio de idioma
		final LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(request);

		Connection con = null;
		Statement st = null;
		ResultSet rs = null;

		String query = "";

		int count = 0;
		int contLines = 1;
		int totalcount = 0;
		long nanoTime = 0;

		boolean error = false;

		try {
			// LECTURA DEL CSV-----------------
			parser.beginParsing(getReader(fullPath.getAbsolutePath()));
			int linecount = getLineCount(fullPath.getAbsolutePath());

			String[] nextLine;

			// SALTARSE LAS CABECERAS
			String[] firstLine = parser.parseNext();

			// CONFIGURACIONES DE LA BD
			con = DriverManager.getConnection(url, user, password);
			st = con.createStatement();

			int rowid = 0;

			rs = st.executeQuery("SELECT MAX(RID) FROM `Rohigmese`.`DavisLlanos`");

			while (rs.next()) {
				rowid = rs.getInt("MAX(RID)");
				rowid++;
			}

			History history = new History();

			String values = "";

			long startNanos = System.nanoTime();
			double batchSize = 0;

			if (linecount > 500) {
				batchSize = linecount * 0.02;
			} else {
				batchSize = linecount - 1;
			}

			contLines = 1;
			boolean exit = false;
			boolean first = true;

			while ((nextLine = parser.parseNext()) != null) {

				if (contLines >= rowid) {
					if (exit == false) {
						history.setIdStart(rowid);
						exit = true;
					}

					if (first == true) {
						values += "('" + rowid + "','" + device.getId() + "','" + nextLine[0] + "','" + nextLine[1]
								+ "','" + nextLine[2] + "','" + nextLine[3] + "','" + nextLine[4] + "','" + nextLine[5]
								+ "')";
						first = false;
					} else {
						values += ",('" + rowid + "','" + device.getId() + "','" + nextLine[0] + "','" + nextLine[1]
								+ "','" + nextLine[2] + "','" + nextLine[3] + "','" + nextLine[4] + "','" + nextLine[5]
								+ "')";
					}

					count++;
					totalcount++;
					rowid++;

					if (count >= batchSize || contLines >= linecount - 1) {
						query = "INSERT INTO  `Rohigmese`.`DavisLlanos` (" + "`RID`, " + "`DEVICE_ID` ," + "`MOMENT` ,"
								+ "`TEMP` ," + "`HUMIDITY` ," + "`WINDSPEED` ," + "`BAR` ," + "`RAIN`" + ")"
								+ " VALUES " + values + ";";
						st.executeUpdate(query);
						count = 0;
						values = "";
						first = true;
						System.out.println("DATOS INTRODUCIDOS: " + totalcount);
						System.out.println("ELAPSED TIME: " + (System.nanoTime() - startNanos));
					}

				}

				contLines++;

			}

			nanoTime = (System.nanoTime() - startNanos);

			if (exit) {
				history.setIdEnd(rowid);
				history.setAuthor(author.getName() + " " + author.getSurname());
				history.setDate(new Date(System.currentTimeMillis() - 1000));
				history.setDevice("MARISMILLAS-LLANOS > " + device.getName() + " [" + device.getBrand() + "]");
				historyService.save(history);
			}

		} catch (SQLException ex) {
			Logger lgr = Logger.getLogger(DeviceResearcherController.class.getName());
			lgr.log(Level.SEVERE, ex.getMessage(), ex);

			final String fileError;

			if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				fileError = "Error uploading the file. Check the format and try again.";
			} else {
				fileError = "Error al subir el archivo. Compruebe el formato y vuelva a intentarlo.";
			}

			redirect.addFlashAttribute("fileError", fileError);

			error = true;

		} catch (Exception e) {
			e.printStackTrace();

			final String fileError;

			if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				fileError = "Error uploading the file. Check the format and try again.";
			} else {
				fileError = "Error al subir el archivo. Compruebe el formato y vuelva a intentarlo.";
			}

			redirect.addFlashAttribute("fileError", fileError);

			error = true;

		} finally {
			try {
				if (st != null) {
					st.close();
				}
				if (con != null) {
					con.close();
				}

				if (error == false) {

					final String fileSuccess;

					long finalTime = TimeUnit.NANOSECONDS.toMinutes(nanoTime);

					if (totalcount > 0) {
						if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
							fileSuccess = "File successfully imported.<br/><b>Rows: " + totalcount + "<br/>Total time: "
									+ finalTime + " minutes</b>";
						} else {
							fileSuccess = "Archivo importado.<br/><b>Filas: " + totalcount + "<br/>Tiempo total: "
									+ finalTime + " minutos</b>";
						}
					} else {
						if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
							fileSuccess = "Updated data.";
						} else {
							fileSuccess = "Datos ya actualizados.";
						}
					}

					redirect.addFlashAttribute("fileSuccess", fileSuccess);

				}

			} catch (SQLException ex) {
				Logger lgr = Logger.getLogger(DeviceResearcherController.class.getName());
				lgr.log(Level.WARNING, ex.getMessage(), ex);

				final String fileError;

				if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
					fileError = "Error uploading the file. Check the format and try again.";
				} else {
					fileError = "Error al subir el archivo. Compruebe el formato y vuelva a intentarlo.";
				}

				redirect.addFlashAttribute("fileError", fileError);

				error = true;

			}
		}

	}

	public static void storeDecagonLlanos(CsvParser parser, File stationPath, final File file, final String filename,
			Researcher author, Device device, HistoryService historyService, final RedirectAttributes redirect,
			final HttpServletRequest request, String url, String user, String password) {

		File devicePath = new File(stationPath, "Decagon/LLANOS/OUTPUT/" + file.getName());
		File fullPath = new File(devicePath.getAbsolutePath());

		// Interceptor de cambio de idioma
		final LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(request);

		Connection con = null;
		Statement st = null;
		ResultSet rs = null;

		String query = "";

		int count = 0;
		int contLines = 1;
		int totalcount = 0;
		long nanoTime = 0;

		boolean error = false;

		try {
			// LECTURA DEL CSV-----------------
			parser.beginParsing(getReader(fullPath.getAbsolutePath()));
			int linecount = getLineCount(fullPath.getAbsolutePath());

			String[] nextLine;

			// SALTARSE LAS CABECERAS
			String[] firstLine = parser.parseNext();

			// CONFIGURACIONES DE LA BD
			con = DriverManager.getConnection(url, user, password);
			st = con.createStatement();

			int rowid = 0;

			rs = st.executeQuery("SELECT MAX(RID) FROM `Rohigmese`.`DecagonLlanos`");

			while (rs.next()) {
				rowid = rs.getInt("MAX(RID)");
				rowid++;
			}

			History history = new History();

			String values = "";

			long startNanos = System.nanoTime();
			double batchSize = 0;

			if (linecount > 500) {
				batchSize = linecount * 0.02;
			} else {
				batchSize = linecount - 1;
			}

			contLines = 1;
			boolean exit = false;
			boolean first = true;

			while ((nextLine = parser.parseNext()) != null) {

				if (contLines >= rowid) {
					if (exit == false) {
						history.setIdStart(rowid);
						exit = true;
					}

					if (first == true) {
						values += "('" + rowid + "','" + device.getId() + "','" + nextLine[0] + "','" + nextLine[1]
								+ "','" + nextLine[2] + "','" + nextLine[3] + "','" + nextLine[4] + "','" + nextLine[5]
								+ "','" + nextLine[6] + "','" + nextLine[7] + "','" + nextLine[8] + "','" + nextLine[9]
								+ "')";
						first = false;
					} else {
						values += ",('" + rowid + "','" + device.getId() + "','" + nextLine[0] + "','" + nextLine[1]
								+ "','" + nextLine[2] + "','" + nextLine[3] + "','" + nextLine[4] + "','" + nextLine[5]
								+ "','" + nextLine[6] + "','" + nextLine[7] + "','" + nextLine[8] + "','" + nextLine[9]
								+ "')";
					}

					count++;
					totalcount++;
					rowid++;

					if (count >= batchSize || contLines >= linecount - 1) {
						query = "INSERT INTO  `Rohigmese`.`DecagonLlanos` (" + "`RID`, " + "`DEVICE_ID` ,"
								+ "`MOMENT` ," + "`VWC_P1` ," + "`TEMP_P1` ," + "`EC_P1` ," + "`VWC_P2` ,"
								+ "`TEMP_P2` ," + "`EC_P2` ," + "`VWC_P3` ," + "`TEMP_P3` ," + "`EC_P3`" + ")"
								+ " VALUES " + values + ";";
						st.executeUpdate(query);
						count = 0;
						values = "";
						first = true;
						System.out.println("DATOS INTRODUCIDOS: " + totalcount);
						System.out.println("ELAPSED TIME: " + (System.nanoTime() - startNanos));
					}

				}

				contLines++;

			}

			nanoTime = (System.nanoTime() - startNanos);

			if (exit) {
				history.setIdEnd(rowid);
				history.setAuthor(author.getName() + " " + author.getSurname());
				history.setDate(new Date(System.currentTimeMillis() - 1000));
				history.setDevice("MARISMILLAS-LLANOS > " + device.getName() + " [" + device.getBrand() + "]");
				historyService.save(history);
			}

		} catch (SQLException ex) {
			Logger lgr = Logger.getLogger(DeviceResearcherController.class.getName());
			lgr.log(Level.SEVERE, ex.getMessage(), ex);

			final String fileError;

			if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				fileError = "Error uploading the file. Check the format and try again.";
			} else {
				fileError = "Error al subir el archivo. Compruebe el formato y vuelva a intentarlo.";
			}

			redirect.addFlashAttribute("fileError", fileError);

			error = true;

		} catch (Exception e) {
			e.printStackTrace();

			final String fileError;

			if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				fileError = "Error uploading the file. Check the format and try again.";
			} else {
				fileError = "Error al subir el archivo. Compruebe el formato y vuelva a intentarlo.";
			}

			redirect.addFlashAttribute("fileError", fileError);

			error = true;

		} finally {
			try {
				if (st != null) {
					st.close();
				}
				if (con != null) {
					con.close();
				}

				if (error == false) {

					final String fileSuccess;

					long finalTime = TimeUnit.NANOSECONDS.toMinutes(nanoTime);

					if (totalcount > 0) {
						if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
							fileSuccess = "File successfully imported.<br/><b>Rows: " + totalcount + "<br/>Total time: "
									+ finalTime + " minutes</b>";
						} else {
							fileSuccess = "Archivo importado.<br/><b>Filas: " + totalcount + "<br/>Tiempo total: "
									+ finalTime + " minutos</b>";
						}
					} else {
						if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
							fileSuccess = "Updated data.";
						} else {
							fileSuccess = "Datos ya actualizados.";
						}
					}

					redirect.addFlashAttribute("fileSuccess", fileSuccess);

				}

			} catch (SQLException ex) {
				Logger lgr = Logger.getLogger(DeviceResearcherController.class.getName());
				lgr.log(Level.WARNING, ex.getMessage(), ex);

				final String fileError;

				if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
					fileError = "Error uploading the file. Check the format and try again.";
				} else {
					fileError = "Error al subir el archivo. Compruebe el formato y vuelva a intentarlo.";
				}

				redirect.addFlashAttribute("fileError", fileError);

				error = true;

			}
		}

	}

	public static void storeDecagonLaguna(CsvParser parser, File stationPath, final File file, final String filename,
			Researcher author, Device device, HistoryService historyService, final RedirectAttributes redirect,
			final HttpServletRequest request, String url, String user, String password) {

		File devicePath = new File(stationPath, "Decagon/LAGUNA/OUTPUT/" + file.getName());
		File fullPath = new File(devicePath.getAbsolutePath());

		// Interceptor de cambio de idioma
		final LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(request);

		Connection con = null;
		Statement st = null;
		ResultSet rs = null;

		String query = "";

		int count = 0;
		int contLines = 1;
		int totalcount = 0;
		long nanoTime = 0;

		boolean error = false;

		try {
			// LECTURA DEL CSV-----------------
			parser.beginParsing(getReader(fullPath.getAbsolutePath()));
			int linecount = getLineCount(fullPath.getAbsolutePath());

			String[] nextLine;

			// SALTARSE LAS CABECERAS
			String[] firstLine = parser.parseNext();

			// CONFIGURACIONES DE LA BD
			con = DriverManager.getConnection(url, user, password);
			st = con.createStatement();

			int rowid = 0;

			rs = st.executeQuery("SELECT MAX(RID) FROM `Rohigmese`.`DecagonLaguna`");

			while (rs.next()) {
				rowid = rs.getInt("MAX(RID)");
				rowid++;
			}

			History history = new History();

			String values = "";

			long startNanos = System.nanoTime();
			double batchSize = 0;

			if (linecount > 500) {
				batchSize = linecount * 0.02;
			} else {
				batchSize = linecount - 1;
			}

			contLines = 1;
			boolean exit = false;
			boolean first = true;

			while ((nextLine = parser.parseNext()) != null) {

				if (contLines >= rowid) {
					if (exit == false) {
						history.setIdStart(rowid);
						exit = true;
					}

					if (first == true) {
						values += "('" + rowid + "','" + device.getId() + "','" + nextLine[0] + "','" + nextLine[1]
								+ "','" + nextLine[2] + "','" + nextLine[3] + "','" + nextLine[4] + "','" + nextLine[5]
								+ "','" + nextLine[6] + "')";
						first = false;
					} else {
						values += ",('" + rowid + "','" + device.getId() + "','" + nextLine[0] + "','" + nextLine[1]
								+ "','" + nextLine[2] + "','" + nextLine[3] + "','" + nextLine[4] + "','" + nextLine[5]
								+ "','" + nextLine[6] + "')";
					}

					count++;
					totalcount++;
					rowid++;

					if (count >= batchSize || contLines >= linecount - 1) {
						query = "INSERT INTO  `Rohigmese`.`DecagonLaguna` (" + "`RID`, " + "`DEVICE_ID` ,"
								+ "`MOMENT` ," + "`VWC_P1` ," + "`TEMP_P1` ," + "`EC_P1` ," + "`VWC_P2` ,"
								+ "`TEMP_P2` ," + "`EC_P2`" + ")" + " VALUES " + values + ";";
						st.executeUpdate(query);
						count = 0;
						values = "";
						first = true;
						System.out.println("DATOS INTRODUCIDOS: " + totalcount);
						System.out.println("ELAPSED TIME: " + (System.nanoTime() - startNanos));
					}

				}

				contLines++;

			}

			nanoTime = (System.nanoTime() - startNanos);

			if (exit) {
				history.setIdEnd(rowid);
				history.setAuthor(author.getName() + " " + author.getSurname());
				history.setDate(new Date(System.currentTimeMillis() - 1000));
				history.setDevice("MARISMILLAS-LAGUNA > " + device.getName() + " [" + device.getBrand() + "]");
				historyService.save(history);
			}

		} catch (SQLException ex) {
			Logger lgr = Logger.getLogger(DeviceResearcherController.class.getName());
			lgr.log(Level.SEVERE, ex.getMessage(), ex);

			final String fileError;

			if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				fileError = "Error uploading the file. Check the format and try again.";
			} else {
				fileError = "Error al subir el archivo. Compruebe el formato y vuelva a intentarlo.";
			}

			redirect.addFlashAttribute("fileError", fileError);

			error = true;

		} catch (Exception e) {
			e.printStackTrace();

			final String fileError;

			if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				fileError = "Error uploading the file. Check the format and try again.";
			} else {
				fileError = "Error al subir el archivo. Compruebe el formato y vuelva a intentarlo.";
			}

			redirect.addFlashAttribute("fileError", fileError);

			error = true;

		} finally {
			try {
				if (st != null) {
					st.close();
				}
				if (con != null) {
					con.close();
				}

				if (error == false) {

					final String fileSuccess;

					long finalTime = TimeUnit.NANOSECONDS.toMinutes(nanoTime);

					if (totalcount > 0) {
						if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
							fileSuccess = "File successfully imported.<br/><b>Rows: " + totalcount + "<br/>Total time: "
									+ finalTime + " minutes</b>";
						} else {
							fileSuccess = "Archivo importado.<br/><b>Filas: " + totalcount + "<br/>Tiempo total: "
									+ finalTime + " minutos</b>";
						}
					} else {
						if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
							fileSuccess = "Updated data.";
						} else {
							fileSuccess = "Datos ya actualizados.";
						}
					}

					redirect.addFlashAttribute("fileSuccess", fileSuccess);

				}

			} catch (SQLException ex) {
				Logger lgr = Logger.getLogger(DeviceResearcherController.class.getName());
				lgr.log(Level.WARNING, ex.getMessage(), ex);

				final String fileError;

				if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
					fileError = "Error uploading the file. Check the format and try again.";
				} else {
					fileError = "Error al subir el archivo. Compruebe el formato y vuelva a intentarlo.";
				}

				redirect.addFlashAttribute("fileError", fileError);

				error = true;

			}
		}

	}

	public static void storeDecagonPalacio(CsvParser parser, File stationPath, final File file, final String filename,
			Researcher author, Device device, HistoryService historyService, final RedirectAttributes redirect,
			final HttpServletRequest request, String url, String user, String password) {

		File devicePath = new File(stationPath, "Decagon/PALACIO/OUTPUT/" + file.getName());
		File fullPath = new File(devicePath.getAbsolutePath());

		// Interceptor de cambio de idioma
		final LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(request);

		Connection con = null;
		Statement st = null;
		ResultSet rs = null;

		String query = "";

		int count = 0;
		int contLines = 1;
		int totalcount = 0;
		long nanoTime = 0;

		boolean error = false;

		try {
			// LECTURA DEL CSV-----------------
			parser.beginParsing(getReader(fullPath.getAbsolutePath()));
			int linecount = getLineCount(fullPath.getAbsolutePath());

			String[] nextLine;

			// SALTARSE LAS CABECERAS
			String[] firstLine = parser.parseNext();

			// CONFIGURACIONES DE LA BD
			con = DriverManager.getConnection(url, user, password);
			st = con.createStatement();

			int rowid = 0;

			rs = st.executeQuery("SELECT MAX(RID) FROM `Rohigmese`.`DecagonPalacio`");

			while (rs.next()) {
				rowid = rs.getInt("MAX(RID)");
				rowid++;
			}

			History history = new History();

			String values = "";

			long startNanos = System.nanoTime();
			double batchSize = 0;

			if (linecount > 500) {
				batchSize = linecount * 0.02;
			} else {
				batchSize = linecount - 1;
			}

			contLines = 1;
			boolean exit = false;
			boolean first = true;

			while ((nextLine = parser.parseNext()) != null) {

				if (contLines >= rowid) {
					if (exit == false) {
						history.setIdStart(rowid);
						exit = true;
					}

					if (first == true) {
						values += "('" + rowid + "','" + device.getId() + "','" + nextLine[0] + "','" + nextLine[1]
								+ "','" + nextLine[2] + "','" + nextLine[3] + "','" + nextLine[4] + "','" + nextLine[5]
								+ "','" + nextLine[6] + "','" + nextLine[7] + "','" + nextLine[8] + "','" + nextLine[9]
								+ "','" + nextLine[10] + "','" + nextLine[11] + "','" + nextLine[12] + "')";
						first = false;
					} else {
						values += ",('" + rowid + "','" + device.getId() + "','" + nextLine[0] + "','" + nextLine[1]
								+ "','" + nextLine[2] + "','" + nextLine[3] + "','" + nextLine[4] + "','" + nextLine[5]
								+ "','" + nextLine[6] + "','" + nextLine[7] + "','" + nextLine[8] + "','" + nextLine[9]
								+ "','" + nextLine[10] + "','" + nextLine[11] + "','" + nextLine[12] + "')";
					}

					count++;
					totalcount++;
					rowid++;

					if (count >= batchSize || contLines >= linecount - 1) {
						query = "INSERT INTO  `Rohigmese`.`DecagonPalacio` (" + "`RID`, " + "`DEVICE_ID` ,"
								+ "`MOMENT` ," + "`VWC_P1` ," + "`TEMP_P1` ," + "`EC_P1` ," + "`VWC_P2` ,"
								+ "`TEMP_P2` ," + "`EC_P2` ," + "`VWC_P3` ," + "`TEMP_P3` ," + "`EC_P3` ,"
								+ "`VWC_P4` ," + "`TEMP_P4` ," + "`EC_P4`" + ")" + " VALUES " + values + ";";
						st.executeUpdate(query);
						count = 0;
						values = "";
						first = true;
						System.out.println("DATOS INTRODUCIDOS: " + totalcount);
						System.out.println("ELAPSED TIME: " + (System.nanoTime() - startNanos));
					}

				}

				contLines++;

			}

			nanoTime = (System.nanoTime() - startNanos);

			if (exit) {
				history.setIdEnd(rowid);
				history.setAuthor(author.getName() + " " + author.getSurname());
				history.setDate(new Date(System.currentTimeMillis() - 1000));
				history.setDevice("MARISMILLAS-PALACIO > " + device.getName() + " [" + device.getBrand() + "]");
				historyService.save(history);
			}

		} catch (SQLException ex) {
			Logger lgr = Logger.getLogger(DeviceResearcherController.class.getName());
			lgr.log(Level.SEVERE, ex.getMessage(), ex);

			final String fileError;

			if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				fileError = "Error uploading the file. Check the format and try again.";
			} else {
				fileError = "Error al subir el archivo. Compruebe el formato y vuelva a intentarlo.";
			}

			redirect.addFlashAttribute("fileError", fileError);

			error = true;

		} catch (Exception e) {
			e.printStackTrace();

			final String fileError;

			if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				fileError = "Error uploading the file. Check the format and try again.";
			} else {
				fileError = "Error al subir el archivo. Compruebe el formato y vuelva a intentarlo.";
			}

			redirect.addFlashAttribute("fileError", fileError);

			error = true;

		} finally {
			try {
				if (st != null) {
					st.close();
				}
				if (con != null) {
					con.close();
				}

				if (error == false) {

					final String fileSuccess;

					long finalTime = TimeUnit.NANOSECONDS.toMinutes(nanoTime);

					if (totalcount > 0) {
						if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
							fileSuccess = "File successfully imported.<br/><b>Rows: " + totalcount + "<br/>Total time: "
									+ finalTime + " minutes</b>";
						} else {
							fileSuccess = "Archivo importado.<br/><b>Filas: " + totalcount + "<br/>Tiempo total: "
									+ finalTime + " minutos</b>";
						}
					} else {
						if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
							fileSuccess = "Updated data.";
						} else {
							fileSuccess = "Datos ya actualizados.";
						}
					}

					redirect.addFlashAttribute("fileSuccess", fileSuccess);

				}

			} catch (SQLException ex) {
				Logger lgr = Logger.getLogger(DeviceResearcherController.class.getName());
				lgr.log(Level.WARNING, ex.getMessage(), ex);

				final String fileError;

				if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
					fileError = "Error uploading the file. Check the format and try again.";
				} else {
					fileError = "Error al subir el archivo. Compruebe el formato y vuelva a intentarlo.";
				}

				redirect.addFlashAttribute("fileError", fileError);

				error = true;

			}
		}

	}

	public static void storeDavisSO(CsvParser parser, File stationPath, final File file, final String filename,
			Researcher author, Device device, HistoryService historyService, final RedirectAttributes redirect,
			final HttpServletRequest request, String url, String user, String password) {

		File devicePath = new File(stationPath, "Davis/OUTPUT/" + file.getName());
		File fullPath = new File(devicePath.getAbsolutePath());

		// Interceptor de cambio de idioma
		final LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(request);

		Connection con = null;
		Statement st = null;
		ResultSet rs = null;

		String query = "";

		int count = 0;
		int contLines = 1;
		int totalcount = 0;
		long nanoTime = 0;

		boolean error = false;

		try {
			// LECTURA DEL CSV-----------------
			parser.beginParsing(getReader(fullPath.getAbsolutePath()));
			int linecount = getLineCount(fullPath.getAbsolutePath());

			String[] nextLine;

			// SALTARSE LAS CABECERAS
			String[] firstLine = parser.parseNext();

			// CONFIGURACIONES DE LA BD
			con = DriverManager.getConnection(url, user, password);
			st = con.createStatement();

			int rowid = 0;

			rs = st.executeQuery("SELECT MAX(RID) FROM `Rohigmese`.`DavisSO`");

			while (rs.next()) {
				rowid = rs.getInt("MAX(RID)");
				rowid++;
			}

			History history = new History();

			String values = "";

			long startNanos = System.nanoTime();
			double batchSize = 0;

			if (linecount > 500) {
				batchSize = linecount * 0.02;
			} else {
				batchSize = linecount - 1;
			}

			contLines = 1;
			boolean exit = false;
			boolean first = true;

			while ((nextLine = parser.parseNext()) != null) {

				if (contLines >= rowid) {
					if (exit == false) {
						history.setIdStart(rowid);
						exit = true;
					}

					if (first == true) {
						values += "('" + rowid + "','" + device.getId() + "','" + nextLine[0] + "','" + nextLine[1]
								+ "','" + nextLine[2] + "','" + nextLine[3] + "','" + nextLine[4] + "','" + nextLine[5]
								+ "')";
						first = false;
					} else {
						values += ",('" + rowid + "','" + device.getId() + "','" + nextLine[0] + "','" + nextLine[1]
								+ "','" + nextLine[2] + "','" + nextLine[3] + "','" + nextLine[4] + "','" + nextLine[5]
								+ "')";
					}

					count++;
					totalcount++;
					rowid++;

					if (count >= batchSize || contLines >= linecount - 1) {
						query = "INSERT INTO  `Rohigmese`.`DavisSO` (" + "`RID`, " + "`DEVICE_ID` ," + "`MOMENT` ,"
								+ "`TEMP` ," + "`HUMIDITY` ," + "`WINDSPEED` ," + "`BAR` ," + "`RAIN`" + ")"
								+ " VALUES " + values + ";";
						st.executeUpdate(query);
						count = 0;
						values = "";
						first = true;
						System.out.println("DATOS INTRODUCIDOS: " + totalcount);
						System.out.println("ELAPSED TIME: " + (System.nanoTime() - startNanos));
					}

				}

				contLines++;

			}

			nanoTime = (System.nanoTime() - startNanos);

			if (exit) {
				history.setIdEnd(rowid);
				history.setAuthor(author.getName() + " " + author.getSurname());
				history.setDate(new Date(System.currentTimeMillis() - 1000));
				history.setDevice("SANTA OLALLA-SO IGME > " + device.getName() + " [" + device.getBrand() + "]");
				historyService.save(history);
			}

		} catch (SQLException ex) {
			Logger lgr = Logger.getLogger(DeviceResearcherController.class.getName());
			lgr.log(Level.SEVERE, ex.getMessage(), ex);

			final String fileError;

			if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				fileError = "Error uploading the file. Check the format and try again.";
			} else {
				fileError = "Error al subir el archivo. Compruebe el formato y vuelva a intentarlo.";
			}

			redirect.addFlashAttribute("fileError", fileError);

			error = true;

		} catch (Exception e) {
			e.printStackTrace();

			final String fileError;

			if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				fileError = "Error uploading the file. Check the format and try again.";
			} else {
				fileError = "Error al subir el archivo. Compruebe el formato y vuelva a intentarlo.";
			}

			redirect.addFlashAttribute("fileError", fileError);

			error = true;

		} finally {
			try {
				if (st != null) {
					st.close();
				}
				if (con != null) {
					con.close();
				}

				if (error == false) {

					final String fileSuccess;

					long finalTime = TimeUnit.NANOSECONDS.toMinutes(nanoTime);

					if (totalcount > 0) {
						if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
							fileSuccess = "File successfully imported.<br/><b>Rows: " + totalcount + "<br/>Total time: "
									+ finalTime + " minutes</b>";
						} else {
							fileSuccess = "Archivo importado.<br/><b>Filas: " + totalcount + "<br/>Tiempo total: "
									+ finalTime + " minutos</b>";
						}
					} else {
						if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
							fileSuccess = "Updated data.";
						} else {
							fileSuccess = "Datos ya actualizados.";
						}
					}

					redirect.addFlashAttribute("fileSuccess", fileSuccess);

				}

			} catch (SQLException ex) {
				Logger lgr = Logger.getLogger(DeviceResearcherController.class.getName());
				lgr.log(Level.WARNING, ex.getMessage(), ex);

				final String fileError;

				if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
					fileError = "Error uploading the file. Check the format and try again.";
				} else {
					fileError = "Error al subir el archivo. Compruebe el formato y vuelva a intentarlo.";
				}

				redirect.addFlashAttribute("fileError", fileError);

				error = true;

			}
		}

	}

	public static void storeCampbellSOP2(CsvParser parser, File stationPath, final File file, final String filename,
			Researcher author, Device device, HistoryService historyService, final RedirectAttributes redirect,
			final HttpServletRequest request, String url, String user, String password) {

		File devicePath = new File(stationPath, "Campbell/P2/OUTPUT/" + file.getName());
		File fullPath = new File(devicePath.getAbsolutePath());

		// Interceptor de cambio de idioma
		final LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(request);

		Connection con = null;
		Statement st = null;
		ResultSet rs = null;

		String query = "";

		int count = 0;
		int contLines = 1;
		int totalcount = 0;
		long nanoTime = 0;

		boolean error = false;

		try {
			// LECTURA DEL CSV-----------------
			parser.beginParsing(getReader(fullPath.getAbsolutePath()));
			int linecount = getLineCount(fullPath.getAbsolutePath());

			String[] nextLine;

			// SALTARSE LAS CABECERAS
			String[] firstLine = parser.parseNext();

			// CONFIGURACIONES DE LA BD
			con = DriverManager.getConnection(url, user, password);
			st = con.createStatement();

			int rowid = 0;

			rs = st.executeQuery("SELECT MAX(RID) FROM `Rohigmese`.`CampbellSOP2`");

			while (rs.next()) {
				rowid = rs.getInt("MAX(RID)");
				rowid++;
			}

			History history = new History();

			String values = "";

			long startNanos = System.nanoTime();
			double batchSize = 0;

			if (linecount > 500) {
				batchSize = linecount * 0.02;
			} else {
				batchSize = linecount - 1;
			}

			contLines = 1;
			boolean exit = false;
			boolean first = true;

			while ((nextLine = parser.parseNext()) != null) {

				if (contLines >= rowid) {
					if (exit == false) {
						history.setIdStart(rowid);
						exit = true;
					}

					if (first == true) {
						values += "('" + rowid + "','" + device.getId() + "','" + nextLine[0] + "','" + nextLine[2]
								+ "','" + nextLine[3] + "','" + nextLine[4] + "','" + nextLine[5] + "','" + nextLine[6]
								+ "','" + nextLine[7] + "','" + nextLine[8] + "','" + nextLine[9] + "','" + nextLine[10]
								+ "','" + nextLine[11] + "','" + nextLine[12] + "','" + nextLine[13] + "')";
						first = false;
					} else {
						values += ",('" + rowid + "','" + device.getId() + "','" + nextLine[0] + "','" + nextLine[2]
								+ "','" + nextLine[3] + "','" + nextLine[4] + "','" + nextLine[5] + "','" + nextLine[6]
								+ "','" + nextLine[7] + "','" + nextLine[8] + "','" + nextLine[9] + "','" + nextLine[10]
								+ "','" + nextLine[11] + "','" + nextLine[12] + "','" + nextLine[13] + "')";
					}

					count++;
					totalcount++;
					rowid++;

					if (count >= batchSize || contLines >= linecount - 1) {
						query = "INSERT INTO  `Rohigmese`.`CampbellSOP2` (" + "`RID`, " + "`DEVICE_ID` ," + "`MOMENT` ,"
								+ "`VWC_P1` ," + "`EC_P1` ," + "`TEMP_P1` ," + "`VWC_P2` ," + "`EC_P2` ,"
								+ "`TEMP_P2` ," + "`VWC_P3` ," + "`EC_P3` ," + "`TEMP_P3` ," + "`VWC_P4` ,"
								+ "`EC_P4` ," + "`TEMP_P4`" + ")" + " VALUES " + values + ";";
						st.executeUpdate(query);
						count = 0;
						values = "";
						first = true;
						System.out.println("DATOS INTRODUCIDOS: " + totalcount);
						System.out.println("ELAPSED TIME: " + (System.nanoTime() - startNanos));
					}

				}

				contLines++;

			}

			nanoTime = (System.nanoTime() - startNanos);

			if (exit) {
				history.setIdEnd(rowid);
				history.setAuthor(author.getName() + " " + author.getSurname());
				history.setDate(new Date(System.currentTimeMillis() - 1000));
				history.setDevice("SANTA OLALLA-SO P2 > " + device.getName() + " [" + device.getBrand() + "]");
				historyService.save(history);
			}

		} catch (SQLException ex) {
			Logger lgr = Logger.getLogger(DeviceResearcherController.class.getName());
			lgr.log(Level.SEVERE, ex.getMessage(), ex);

			final String fileError;

			if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				fileError = "Error uploading the file. Check the format and try again.";
			} else {
				fileError = "Error al subir el archivo. Compruebe el formato y vuelva a intentarlo.";
			}

			redirect.addFlashAttribute("fileError", fileError);

			error = true;

		} catch (Exception e) {
			e.printStackTrace();

			final String fileError;

			if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				fileError = "Error uploading the file. Check the format and try again.";
			} else {
				fileError = "Error al subir el archivo. Compruebe el formato y vuelva a intentarlo.";
			}

			redirect.addFlashAttribute("fileError", fileError);

			error = true;

		} finally {
			try {
				if (st != null) {
					st.close();
				}
				if (con != null) {
					con.close();
				}

				if (error == false) {

					final String fileSuccess;

					long finalTime = TimeUnit.NANOSECONDS.toMinutes(nanoTime);

					if (totalcount > 0) {
						if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
							fileSuccess = "File successfully imported.<br/><b>Rows: " + totalcount + "<br/>Total time: "
									+ finalTime + " minutes</b>";
						} else {
							fileSuccess = "Archivo importado.<br/><b>Filas: " + totalcount + "<br/>Tiempo total: "
									+ finalTime + " minutos</b>";
						}
					} else {
						if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
							fileSuccess = "Updated data.";
						} else {
							fileSuccess = "Datos ya actualizados.";
						}
					}

					redirect.addFlashAttribute("fileSuccess", fileSuccess);

				}

			} catch (SQLException ex) {
				Logger lgr = Logger.getLogger(DeviceResearcherController.class.getName());
				lgr.log(Level.WARNING, ex.getMessage(), ex);

				final String fileError;

				if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
					fileError = "Error uploading the file. Check the format and try again.";
				} else {
					fileError = "Error al subir el archivo. Compruebe el formato y vuelva a intentarlo.";
				}

				redirect.addFlashAttribute("fileError", fileError);

				error = true;

			}
		}

	}

	public static void storeCampbellSOP3(CsvParser parser, File stationPath, final File file, final String filename,
			Researcher author, Device device, HistoryService historyService, final RedirectAttributes redirect,
			final HttpServletRequest request, String url, String user, String password) {

		File devicePath = new File(stationPath, "Campbell/P3/OUTPUT/" + file.getName());
		File fullPath = new File(devicePath.getAbsolutePath());

		// Interceptor de cambio de idioma
		final LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(request);

		Connection con = null;
		Statement st = null;
		ResultSet rs = null;

		String query = "";

		int count = 0;
		int contLines = 1;
		int totalcount = 0;
		long nanoTime = 0;

		boolean error = false;

		try {
			// LECTURA DEL CSV-----------------
			parser.beginParsing(getReader(fullPath.getAbsolutePath()));
			int linecount = getLineCount(fullPath.getAbsolutePath());

			String[] nextLine;

			// SALTARSE LAS CABECERAS
			String[] firstLine = parser.parseNext();

			// CONFIGURACIONES DE LA BD
			con = DriverManager.getConnection(url, user, password);
			st = con.createStatement();

			int rowid = 0;

			rs = st.executeQuery("SELECT MAX(RID) FROM `Rohigmese`.`CampbellSOP3`");

			while (rs.next()) {
				rowid = rs.getInt("MAX(RID)");
				rowid++;
			}

			History history = new History();

			String values = "";

			long startNanos = System.nanoTime();
			double batchSize = 0;

			if (linecount > 500) {
				batchSize = linecount * 0.02;
			} else {
				batchSize = linecount - 1;
			}

			contLines = 1;
			boolean exit = false;
			boolean first = true;

			while ((nextLine = parser.parseNext()) != null) {

				if (contLines >= rowid) {
					if (exit == false) {
						history.setIdStart(rowid);
						exit = true;
					}

					if (first == true) {
						values += "('" + rowid + "','" + device.getId() + "','" + nextLine[0] + "','" + nextLine[1]
								+ "','" + nextLine[2] + "','" + nextLine[3] + "','" + nextLine[4] + "','" + nextLine[5]
								+ "','" + nextLine[6] + "','" + nextLine[7] + "','" + nextLine[8] + "','" + nextLine[9]
								+ "')";
						first = false;
					} else {
						values += ",('" + rowid + "','" + device.getId() + "','" + nextLine[0] + "','" + nextLine[1]
								+ "','" + nextLine[2] + "','" + nextLine[3] + "','" + nextLine[4] + "','" + nextLine[5]
								+ "','" + nextLine[6] + "','" + nextLine[7] + "','" + nextLine[8] + "','" + nextLine[9]
								+ "')";
					}

					count++;
					totalcount++;
					rowid++;

					if (count >= batchSize || contLines >= linecount - 1) {
						query = "INSERT INTO  `Rohigmese`.`CampbellSOP3` (" + "`RID`, " + "`DEVICE_ID` ," + "`MOMENT` ,"
								+ "`VWC_P1` ," + "`EC_P1` ," + "`TEMP_P1` ," + "`VWC_P2` ," + "`EC_P2` ,"
								+ "`TEMP_P2` ," + "`VWC_P3` ," + "`EC_P3` ," + "`TEMP_P3`" + ")" + " VALUES " + values
								+ ";";
						st.executeUpdate(query);
						count = 0;
						values = "";
						first = true;
						System.out.println("DATOS INTRODUCIDOS: " + totalcount);
						System.out.println("ELAPSED TIME: " + (System.nanoTime() - startNanos));
					}

				}

				contLines++;

			}

			nanoTime = (System.nanoTime() - startNanos);

			if (exit) {
				history.setIdEnd(rowid);
				history.setAuthor(author.getName() + " " + author.getSurname());
				history.setDate(new Date(System.currentTimeMillis() - 1000));
				history.setDevice("SANTA OLALLA-SO P3 > " + device.getName() + " [" + device.getBrand() + "]");
				historyService.save(history);
			}

		} catch (SQLException ex) {
			Logger lgr = Logger.getLogger(DeviceResearcherController.class.getName());
			lgr.log(Level.SEVERE, ex.getMessage(), ex);

			final String fileError;

			if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				fileError = "Error uploading the file. Check the format and try again.";
			} else {
				fileError = "Error al subir el archivo. Compruebe el formato y vuelva a intentarlo.";
			}

			redirect.addFlashAttribute("fileError", fileError);

			error = true;

		} catch (Exception e) {
			e.printStackTrace();

			final String fileError;

			if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				fileError = "Error uploading the file. Check the format and try again.";
			} else {
				fileError = "Error al subir el archivo. Compruebe el formato y vuelva a intentarlo.";
			}

			redirect.addFlashAttribute("fileError", fileError);

			error = true;

		} finally {
			try {
				if (st != null) {
					st.close();
				}
				if (con != null) {
					con.close();
				}

				if (error == false) {

					final String fileSuccess;

					long finalTime = TimeUnit.NANOSECONDS.toMinutes(nanoTime);

					if (totalcount > 0) {
						if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
							fileSuccess = "File successfully imported.<br/><b>Rows: " + totalcount + "<br/>Total time: "
									+ finalTime + " minutes</b>";
						} else {
							fileSuccess = "Archivo importado.<br/><b>Filas: " + totalcount + "<br/>Tiempo total: "
									+ finalTime + " minutos</b>";
						}
					} else {
						if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
							fileSuccess = "Updated data.";
						} else {
							fileSuccess = "Datos ya actualizados.";
						}
					}

					redirect.addFlashAttribute("fileSuccess", fileSuccess);

				}

			} catch (SQLException ex) {
				Logger lgr = Logger.getLogger(DeviceResearcherController.class.getName());
				lgr.log(Level.WARNING, ex.getMessage(), ex);

				final String fileError;

				if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
					fileError = "Error uploading the file. Check the format and try again.";
				} else {
					fileError = "Error al subir el archivo. Compruebe el formato y vuelva a intentarlo.";
				}

				redirect.addFlashAttribute("fileError", fileError);

				error = true;

			}
		}

	}

	public static void storeManualSOP1(CsvParser parser, File stationPath, final File file, final String filename,
			Researcher author, Device device, HistoryService historyService, final RedirectAttributes redirect,
			final HttpServletRequest request, String url, String user, String password) {

		File devicePath = new File(stationPath, "P1/OUTPUT/" + file.getName());
		File fullPath = new File(devicePath.getAbsolutePath());

		// Interceptor de cambio de idioma
		final LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(request);

		Connection con = null;
		Statement st = null;
		ResultSet rs = null;

		String query = "";

		int count = 0;
		int contLines = 1;
		int totalcount = 0;
		long nanoTime = 0;

		boolean error = false;

		try {
			// LECTURA DEL CSV-----------------
			parser.beginParsing(getReader(fullPath.getAbsolutePath()));
			int linecount = getLineCount(fullPath.getAbsolutePath());

			String[] nextLine;

			// SALTARSE LAS CABECERAS
			String[] firstLine = parser.parseNext();

			// CONFIGURACIONES DE LA BD
			con = DriverManager.getConnection(url, user, password);
			st = con.createStatement();

			int rowid = 0;

			rs = st.executeQuery("SELECT MAX(RID) FROM `Rohigmese`.`ManualSOP1`");

			while (rs.next()) {
				rowid = rs.getInt("MAX(RID)");
				rowid++;
			}

			History history = new History();

			String values = "";

			long startNanos = System.nanoTime();
			double batchSize = 0;

			if (linecount > 500) {
				batchSize = linecount * 0.02;
			} else {
				batchSize = linecount - 1;
			}

			contLines = 1;
			boolean exit = false;
			boolean first = true;

			while ((nextLine = parser.parseNext()) != null) {

				if (contLines >= rowid) {
					if (exit == false) {
						history.setIdStart(rowid);
						exit = true;
					}

					if (first == true) {
						values += "('" + rowid + "','" + device.getId() + "','" + nextLine[0] + "','" + nextLine[1]
								+ "')";
						first = false;
					} else {
						values += ",('" + rowid + "','" + device.getId() + "','" + nextLine[0] + "','" + nextLine[1]
								+ "')";
					}

					count++;
					totalcount++;
					rowid++;

					if (count >= batchSize || contLines >= linecount - 1) {
						query = "INSERT INTO  `Rohigmese`.`ManualSOP1` (" + "`RID`, " + "`DEVICE_ID` ," + "`MOMENT` ,"
								+ "`LEVEL`)" + " VALUES " + values + ";";

						st.executeUpdate(query);
						count = 0;
						values = "";
						first = true;
						System.out.println("DATOS INTRODUCIDOS: " + totalcount);
						System.out.println("ELAPSED TIME: " + (System.nanoTime() - startNanos));
					}

				}

				contLines++;

			}

			nanoTime = (System.nanoTime() - startNanos);

			if (exit) {
				history.setIdEnd(rowid);
				history.setAuthor(author.getName() + " " + author.getSurname());
				history.setDate(new Date(System.currentTimeMillis() - 1000));
				history.setDevice("SANTA OLALLA-SO P1 > " + device.getName());
				historyService.save(history);
			}

		} catch (SQLException ex) {
			Logger lgr = Logger.getLogger(DeviceResearcherController.class.getName());
			lgr.log(Level.SEVERE, ex.getMessage(), ex);

			final String fileError;

			if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				fileError = "Error uploading the file. Check the format and try again.";
			} else {
				fileError = "Error al subir el archivo. Compruebe el formato y vuelva a intentarlo.";
			}

			redirect.addFlashAttribute("fileError", fileError);

			error = true;

		} catch (Exception e) {
			e.printStackTrace();

			final String fileError;

			if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				fileError = "Error uploading the file. Check the format and try again.";
			} else {
				fileError = "Error al subir el archivo. Compruebe el formato y vuelva a intentarlo.";
			}

			redirect.addFlashAttribute("fileError", fileError);

			error = true;

		} finally {
			try {
				if (st != null) {
					st.close();
				}
				if (con != null) {
					con.close();
				}

				if (error == false) {

					final String fileSuccess;

					long finalTime = TimeUnit.NANOSECONDS.toMinutes(nanoTime);

					if (totalcount > 0) {
						if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
							fileSuccess = "File successfully imported.<br/><b>Rows: " + totalcount + "<br/>Total time: "
									+ finalTime + " minutes</b>";
						} else {
							fileSuccess = "Archivo importado.<br/><b>Filas: " + totalcount + "<br/>Tiempo total: "
									+ finalTime + " minutos</b>";
						}
					} else {
						if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
							fileSuccess = "Updated data.";
						} else {
							fileSuccess = "Datos ya actualizados.";
						}
					}

					redirect.addFlashAttribute("fileSuccess", fileSuccess);

				}

			} catch (SQLException ex) {
				Logger lgr = Logger.getLogger(DeviceResearcherController.class.getName());
				lgr.log(Level.WARNING, ex.getMessage(), ex);

				final String fileError;

				if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
					fileError = "Error uploading the file. Check the format and try again.";
				} else {
					fileError = "Error al subir el archivo. Compruebe el formato y vuelva a intentarlo.";
				}

				redirect.addFlashAttribute("fileError", fileError);

				error = true;

			}
		}

	}

}
