/*
 * ResearcherController.java
 * 
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.station;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContextUtils;

import controllers.AbstractController;
import domain.Device;
import domain.Station;
import domain.Zone;
import services.DeviceService;
import services.StationService;
import services.ZoneService;

@Controller
@RequestMapping("/device/station")
public class DeviceStationController extends AbstractController {
	
	//Services--------------------------------------------------------------------------
	@Autowired
	private DeviceService	deviceService;
	@Autowired
	private StationService	stationService;
	@Autowired
	private ZoneService	zoneService;

	// Constructors -----------------------------------------------------------

	public DeviceStationController() {
		super();
	}
	
	//List zones------------------------------------------------------------------
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView listDevices(final int stationId,@ModelAttribute("notFoundError") String notFoundError, final HttpServletRequest request) {
		
		final ModelAndView modelAndView;
		
		final Station station = this.stationService.findOne(stationId);
		final Zone zone = this.zoneService.findOne(station.getZone().getId());

		final Collection<Device> devices = new ArrayList<Device>(station.getDevices());

		modelAndView = new ModelAndView("device/list");
		
		modelAndView.addObject("devices", devices);
		modelAndView.addObject("station", station);
		
		if (notFoundError != "") {
			modelAndView.addObject("notFoundError", notFoundError);
		}
		
		modelAndView.addObject("requestURI", "/device/station/list.do?stationId="+station.getId());
		
		this.updateBreadCrumb(modelAndView, "Zone: "+zone.getName()+", Station: "+station.getName(), "Zona: "+zone.getName()+", Estaci�n: "+station.getName(),"Equipment","Equipamiento",request);
		
		return modelAndView;
		
	}

	// Ancillary methods --------------------------------------------------------------
	
	protected void updateBreadCrumb(ModelAndView mv,String parentEN,String parentES,String currentPageEN,String currentPageES,final HttpServletRequest request) {
		
		final LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(request);
		
		if(parentEN!=null && parentES!=null && currentPageEN!=null && currentPageES!=null) {
			if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				mv.addObject("parent", parentEN);
				mv.addObject("currentPage", currentPageEN);
			}else {
				mv.addObject("parent", parentES);
				mv.addObject("currentPage", currentPageES);
			}
		}else if(parentEN==null || parentES==null ) {
			
			if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				mv.addObject("currentPage", currentPageEN);
			}else {
				mv.addObject("currentPage", currentPageES);
			}
			
		}else {
			
			mv.addObject("currentPage", "Index");
			
		}
		
		
		
	}

}
