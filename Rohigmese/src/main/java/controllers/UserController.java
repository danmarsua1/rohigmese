/*
 * UserController.java
 * 
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import domain.User;
import services.UserService;

@Controller
@RequestMapping("/user")
public class UserController extends AbstractController {
	
	//Services--------------------------------------------------------------------------
	@Autowired
	private UserService	userService;

	// Constructors -----------------------------------------------------------

	public UserController() {
		super();
	}
	
	//Creating--------------------------------------------------------------------------
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create(final HttpServletRequest request) {
		ModelAndView modelAndView;

		final User user = this.userService.create();
		modelAndView = this.createRegisterModelAndView(user);
		
		this.updateBreadCrumb(modelAndView, "User","Usuario","Sign Up","Registro",request);

		return modelAndView;
	}
	
	//Editing---------------------------------------------------------------------------
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(final HttpServletRequest request) {
		ModelAndView modelAndView;

		final User user = this.userService.findByPrincipal();
		Assert.notNull(user);

		modelAndView = this.createEditModelAndView(user);
		
		this.updateBreadCrumb(modelAndView, null,null,"My information","Mis datos",request);
		
		return modelAndView;
	}
	
	//Register-------------------------------------------------------------------------
	@RequestMapping(value = "/register", method = RequestMethod.POST, params = "save")
	public ModelAndView register(@Valid final User user, final BindingResult bindingResult,final HttpServletRequest request, final RedirectAttributes redirect) {
		ModelAndView modelAndView;
		
		//Interceptor de cambio de idioma
		final LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(request);

		if (bindingResult.hasErrors()) {
			modelAndView = this.createRegisterModelAndView(user);
			
			final String errorsAlert;
			
			if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				errorsAlert = "(*)  Obligatory fields";
			}else {
				errorsAlert = "(*)  Campos obligatorios";
			}
			
			modelAndView.addObject("errorsAlert", errorsAlert);
			
			this.updateBreadCrumb(modelAndView, "User","Usuario","Sign Up","Registro",request);
			
		}else
			try {
				
				if(userService.uniqueUser(user.getUserAccount().getUsername())) {
					this.userService.register(user);
					
					final String registerSuccess;
					
			        if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
			        	registerSuccess = "Has been successfully registered";
			        }else {
			        	registerSuccess = "Se ha registrado con �xito";
			        }
			         
			        redirect.addFlashAttribute("registerSuccess", registerSuccess);
					
					modelAndView = new ModelAndView("redirect:/security/login.do");
					
					this.updateBreadCrumb(modelAndView, null,null,null,null,request);
					
				}else {
					
					modelAndView = this.createRegisterModelAndView(user);
					
					final String verifyAlert;
					
					if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
						verifyAlert = "There's already an user registered with that username";
					}else {
						verifyAlert = "Ya hay un usuario registrado con ese username";
					}
					
					modelAndView.addObject("verifyAlert", verifyAlert);
					
					this.updateBreadCrumb(modelAndView, "User","Usuario","Sign Up","Registro",request);
					
				}

			} catch (final Throwable throwable) {
				modelAndView = this.createRegisterModelAndView(user, "user.commit.error");
				
				this.updateBreadCrumb(modelAndView, "User","Usuario","Sign Up","Registro",request);

			}

		return modelAndView;

	}

	//Update-------------------------------------------------------------------------
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final User user, final BindingResult bindingResult,final HttpServletRequest request) {
		ModelAndView modelAndView;
		
		//Interceptor de cambio de idioma
		final LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(request);

		if (bindingResult.hasErrors()) {
			modelAndView = this.createEditModelAndView(user);
			
			final String errorsAlert;
			
			if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				errorsAlert = "(*)  Obligatory fields";
			}else {
				errorsAlert = "(*)  Campos obligatorios";
			}
			
			modelAndView.addObject("errorsAlert", errorsAlert);
			
			this.updateBreadCrumb(modelAndView, null,null,"My information","Mis datos",request);
			
		}else
			try {
				this.userService.save(user);
				modelAndView = new ModelAndView("redirect:/welcome/index.do");
				
				this.updateBreadCrumb(modelAndView, null,null,null,null,request);

			} catch (final Throwable throwable) {
				modelAndView = this.createEditModelAndView(user, "user.commit.error");
				
				this.updateBreadCrumb(modelAndView, null,null,"My information","Mis datos",request);

			}

		return modelAndView;

	}
	
	//Delete-------------------------------------------------------------------------
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(@Valid final User user, final BindingResult bindingResult, final HttpServletRequest request) {

		ModelAndView modelAndView;
		
		try {
			this.userService.delete(user);
			modelAndView = new ModelAndView("redirect:../j_spring_security_logout");
			this.updateBreadCrumb(modelAndView, null,null,null,null,request);
		} catch (final Throwable throwable) {
			modelAndView = this.createEditModelAndView(user, "user.commit.error");
			this.updateBreadCrumb(modelAndView, null,null,"My information","Mis datos",request);
		}

		return modelAndView;

	}

	// Ancillary methods --------------------------------------------------------------
	protected ModelAndView createEditModelAndView(final User user) {
		ModelAndView result;
		result = this.createEditModelAndView(user, null);
		return result;
	}

	protected ModelAndView createEditModelAndView(final User user, final String message) {
		ModelAndView result;

		result = new ModelAndView("user/edit");

		result.addObject("user", user);
		result.addObject("message", message);


		return result;
	}
	
	protected ModelAndView createRegisterModelAndView(final User user) {
		ModelAndView result;
		result = this.createRegisterModelAndView(user, null);
		return result;
	}

	protected ModelAndView createRegisterModelAndView(final User user, final String message) {
		ModelAndView result;

		result = new ModelAndView("user/register");

		result.addObject("user", user);
		result.addObject("message", message);

		return result;
	}
	
	protected void updateBreadCrumb(ModelAndView mv,String parentEN,String parentES,String currentPageEN,String currentPageES,final HttpServletRequest request) {
		
		final LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(request);
		
		if(parentEN!=null && parentES!=null && currentPageEN!=null && currentPageES!=null) {
			if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				mv.addObject("parent", parentEN);
				mv.addObject("currentPage", currentPageEN);
			}else {
				mv.addObject("parent", parentES);
				mv.addObject("currentPage", currentPageES);
			}
		}else if(parentEN==null || parentES==null ) {
			
			if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				mv.addObject("currentPage", currentPageEN);
			}else {
				mv.addObject("currentPage", currentPageES);
			}
			
		}else {
			
			mv.addObject("currentPage", "Index");
			
		}
		
		
		
	}

}
