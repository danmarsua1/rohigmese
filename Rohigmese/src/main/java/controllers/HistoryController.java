/*
 * ResearcherController.java
 * 
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers;

import java.util.Collection;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContextUtils;

import domain.History;
import services.HistoryService;

@Controller
@RequestMapping("/history")
public class HistoryController extends AbstractController {

	// Services--------------------------------------------------------------------------
	@Autowired
	private HistoryService historyService;

	// Constructors -----------------------------------------------------------

	public HistoryController() {
		super();
	}

	// List zones------------------------------------------------------------------
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView listRecords(final HttpServletRequest request) {

		final ModelAndView modelAndView;

		final Collection<History> history = this.historyService.findAll();

		modelAndView = new ModelAndView("history/list");

		modelAndView.addObject("history", history);

		modelAndView.addObject("requestURI", "/history/list.do");

		this.updateBreadCrumb(modelAndView, null, null, "Data records", "Historial de datos", request);

		return modelAndView;

	}

	// Ancillary methods
	// --------------------------------------------------------------

	protected void updateBreadCrumb(ModelAndView mv, String parentEN, String parentES, String currentPageEN,
			String currentPageES, final HttpServletRequest request) {

		final LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(request);

		if (parentEN != null && parentES != null && currentPageEN != null && currentPageES != null) {
			if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				mv.addObject("parent", parentEN);
				mv.addObject("currentPage", currentPageEN);
			} else {
				mv.addObject("parent", parentES);
				mv.addObject("currentPage", currentPageES);
			}
		} else if (parentEN == null || parentES == null) {

			if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				mv.addObject("currentPage", currentPageEN);
			} else {
				mv.addObject("currentPage", currentPageES);
			}

		} else {

			mv.addObject("currentPage", "Index");

		}

	}

}
