/*
 * AdministratorController.java
 * 
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContextUtils;

import domain.Actor;
import domain.Administrator;
import domain.Invitation;
import domain.Researcher;
import domain.User;
import security.Authority;
import services.ActorService;
import services.AdministratorService;
import services.InvitationService;
import services.ResearcherService;
import services.UserService;

@Controller
@RequestMapping("/administrator")
public class AdministratorController extends AbstractController {
	
	//Services--------------------------------------------------------------------------
	@Autowired
	private AdministratorService	administratorService;
	@Autowired
	private InvitationService	invitationService;
	@Autowired
	private UserService	userService;
	@Autowired
	private ResearcherService	researcherService;

	// Constructors -----------------------------------------------------------

	public AdministratorController() {
		super();
	}
	
	//List system users--------------------------------------------------------
	@RequestMapping(value = "/user/list", method = RequestMethod.GET)
	public ModelAndView listUsers(final HttpServletRequest request) {
		
		final ModelAndView modelAndView;

		final Collection<Administrator> admins = this.administratorService.findAll();
		final Collection<Researcher> researchers = this.researcherService.findAll();
		final Collection<User> users = this.userService.findAll();
		
		Collection<Object> actors = new ArrayList<Object>();
		
		modelAndView = new ModelAndView("user/list");
		
		actors.addAll(admins);
		actors.addAll(researchers);
		actors.addAll(users);
		
		modelAndView.addObject("actors", actors);
		
		modelAndView.addObject("requestURI", "/administrator/user/list.do");
		
		this.updateBreadCrumb(modelAndView, "Administrator","Administrador","User registry","Registro de usuarios",request);
		
		return modelAndView;
		
	}
	
	//List invitations------------------------------------------------------------------
	@RequestMapping(value = "/invitation/list", method = RequestMethod.GET)
	public ModelAndView listInvitations(final HttpServletRequest request) {
		
		final ModelAndView modelAndView;

		final Collection<Invitation> invitations = this.invitationService.findAll();

		modelAndView = new ModelAndView("invitation/list");
		
		modelAndView.addObject("invitations", invitations);
		
		modelAndView.addObject("requestURI", "/administrator/invitation/list.do");
		
		this.updateBreadCrumb(modelAndView, "Administrator","Administrador","View invitations","Revisar invitaciones",request);
		
		return modelAndView;
		
	}

	//Editing---------------------------------------------------------------------------
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(final HttpServletRequest request) {
		ModelAndView modelAndView;

		final Administrator administrator = this.administratorService.findByPrincipal();
		Assert.notNull(administrator);

		modelAndView = this.createEditModelAndView(administrator);
		
		this.updateBreadCrumb(modelAndView, "Administrator","Administrador","My information","Mis datos",request);
		
		return modelAndView;
	}

	//Update-------------------------------------------------------------------------
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final Administrator administrator, final BindingResult bindingResult,final HttpServletRequest request) {
		ModelAndView modelAndView;
		System.out.println();
		
		//Interceptor de cambio de idioma
		final LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(request);

		if (bindingResult.hasErrors()) {
			modelAndView = this.createEditModelAndView(administrator);
			
			final String errorsAlert;
			
			if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				errorsAlert = "(*)  Obligatory fields";
			}else {
				errorsAlert = "(*)  Campos obligatorios";
			}
			
			modelAndView.addObject("errorsAlert", errorsAlert);
			
			this.updateBreadCrumb(modelAndView, "Administrator","Administrador","My information","Mis datos",request);
			
		}else
			try {
				this.administratorService.save(administrator);
				modelAndView = new ModelAndView("redirect:/welcome/index.do");
				
				this.updateBreadCrumb(modelAndView, null,null,null,null,request);

			} catch (final Throwable throwable) {
				modelAndView = this.createEditModelAndView(administrator, "administrator.commit.error");
				
				this.updateBreadCrumb(modelAndView, "Administrator","Administrador","My information","Mis datos",request);

			}

		return modelAndView;

	}

	// Ancillary methods --------------------------------------------------------------
	protected ModelAndView createEditModelAndView(final Administrator administrator) {
		ModelAndView result;
		result = this.createEditModelAndView(administrator, null);
		return result;
	}

	protected ModelAndView createEditModelAndView(final Administrator administrator, final String message) {
		ModelAndView result;

		result = new ModelAndView("administrator/edit");

		result.addObject("administrator", administrator);
		result.addObject("message", message);


		return result;
	}
	
	protected void updateBreadCrumb(ModelAndView mv,String parentEN,String parentES,String currentPageEN,String currentPageES,final HttpServletRequest request) {
		
		final LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(request);
		
		if(parentEN!=null && parentES!=null && currentPageEN!=null && currentPageES!=null) {
			if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				mv.addObject("parent", parentEN);
				mv.addObject("currentPage", currentPageEN);
			}else {
				mv.addObject("parent", parentES);
				mv.addObject("currentPage", currentPageES);
			}
		}else if(parentEN==null || parentES==null ) {
			
			if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				mv.addObject("currentPage", currentPageEN);
			}else {
				mv.addObject("currentPage", currentPageES);
			}
			
		}else {
			
			mv.addObject("currentPage", "Index");
			
		}
		
		
		
	}

}
