/*
 * ResearcherController.java
 * 
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import domain.Researcher;
import domain.User;
import services.ResearcherService;

@Controller
@RequestMapping("/researcher")
public class ResearcherController extends AbstractController {
	
	//Services--------------------------------------------------------------------------
	@Autowired
	private ResearcherService	researcherService;

	// Constructors -----------------------------------------------------------

	public ResearcherController() {
		super();
	}
	
	//Creating (Verify)--------------------------------------------------------------------------
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create(final HttpServletRequest request) {
		ModelAndView modelAndView;

		modelAndView = new ModelAndView("researcher/verify");
		
		this.updateBreadCrumb(modelAndView, "Researcher","Investigador","Sign Up","Registro",request);

		return modelAndView;
	}
		
	//Creating (Register)--------------------------------------------------------------------------
	@RequestMapping(value = "/verify", method = RequestMethod.POST, params = "save")
	public ModelAndView verify(@RequestParam final String username, @RequestParam final String password, final HttpServletRequest request) {
		ModelAndView modelAndView;
		
		//Interceptor de cambio de idioma
		final LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(request);
		
		if(username!="" && password!="") {
		
			if(researcherService.verifyCredentials(username,password)) {
				final Researcher researcher = this.researcherService.create(username);
				modelAndView = this.createRegisterModelAndView(researcher);
			
			}else {
				modelAndView = new ModelAndView("researcher/verify");
				
				final String verifyAlert;
				
				if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
					verifyAlert = "Incorrect username or password. If you believe this is due to an administrative error, please contact support. Thank you.";
				}else {
					verifyAlert = "Usuario o contrase�a incorrectos. Si cree que se debe a un error de administraci�n, por favor, p�ngase en contacto con soporte. Gracias.";
				}
				
				modelAndView.addObject("verifyAlert", verifyAlert);
			}
		
		}else {
			modelAndView = new ModelAndView("researcher/verify");
			
			final String errorsAlert;
			
			if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				errorsAlert = "(*)  Obligatory fields";
			}else {
				errorsAlert = "(*)  Campos obligatorios";
			}
			
			modelAndView.addObject("errorsAlert", errorsAlert);
		}
		
		this.updateBreadCrumb(modelAndView, "Researcher","Investigador","Sign Up","Registro",request);

		return modelAndView;
	}

	//Editing---------------------------------------------------------------------------
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(final HttpServletRequest request) {
		ModelAndView modelAndView;

		final Researcher researcher = this.researcherService.findByPrincipal();
		Assert.notNull(researcher);

		modelAndView = this.createEditModelAndView(researcher);
		
		this.updateBreadCrumb(modelAndView, "Researcher","Investigador","My information","Mis datos",request);
		
		return modelAndView;
	}
	
	//Register-------------------------------------------------------------------------
	@RequestMapping(value = "/register", method = RequestMethod.POST, params = "save")
	public ModelAndView register(@Valid final Researcher researcher, final BindingResult bindingResult,final HttpServletRequest request, final RedirectAttributes redirect) {
		ModelAndView modelAndView;
		
		//Interceptor de cambio de idioma
		final LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(request);

		if (bindingResult.hasErrors()) {
			modelAndView = this.createRegisterModelAndView(researcher);
			
			final String errorsAlert;
			
			if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				errorsAlert = "(*)  Obligatory fields";
			}else {
				errorsAlert = "(*)  Campos obligatorios";
			}
			
			modelAndView.addObject("errorsAlert", errorsAlert);
			
			this.updateBreadCrumb(modelAndView, "Researcher","Investigador","Sign Up","Registro",request);
			
		}else
			try {
				
				if(researcherService.uniqueResearcher(researcher.getUserAccount().getUsername())) {
					
					this.researcherService.register(researcher);
					
					final String registerSuccess;
					
			        if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
			        	registerSuccess = "Has been successfully registered";
			        }else {
			        	registerSuccess = "Se ha registrado con �xito";
			        }
			         
			        redirect.addFlashAttribute("registerSuccess", registerSuccess);
					
					modelAndView = new ModelAndView("redirect:/security/login.do");
					
					this.updateBreadCrumb(modelAndView, null,null,null,null,request);
					
				}else {
					
					modelAndView = this.createRegisterModelAndView(researcher);
					
					final String verifyAlert;
					
					if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
						verifyAlert = "There's already an investigator registered with that username";
					}else {
						verifyAlert = "Ya hay un investigador registrado con ese username";
					}
					
					modelAndView.addObject("verifyAlert", verifyAlert);
					
					this.updateBreadCrumb(modelAndView, "Researcher","Investigador","Sign Up","Registro",request);
					
				}

			} catch (final Throwable throwable) {
				modelAndView = this.createRegisterModelAndView(researcher, "researcher.commit.error");
				
				this.updateBreadCrumb(modelAndView, "Researcher","Investigador","Sign Up","Registro",request);

			}

		return modelAndView;

	}

	//Update-------------------------------------------------------------------------
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final Researcher researcher, final BindingResult bindingResult,final HttpServletRequest request) {
		ModelAndView modelAndView;
		
		//Interceptor de cambio de idioma
		final LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(request);

		if (bindingResult.hasErrors()) {
			modelAndView = this.createEditModelAndView(researcher);
			
			final String errorsAlert;
			
			if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				errorsAlert = "(*)  Obligatory fields";
			}else {
				errorsAlert = "(*)  Campos obligatorios";
			}
			
			modelAndView.addObject("errorsAlert", errorsAlert);
			
			this.updateBreadCrumb(modelAndView, "Researcher","Investigador","My information","Mis datos",request);
			
		}else
			try {
				this.researcherService.save(researcher);
				modelAndView = new ModelAndView("redirect:/welcome/index.do");
				
				this.updateBreadCrumb(modelAndView, null,null,null,null,request);

			} catch (final Throwable throwable) {
				modelAndView = this.createEditModelAndView(researcher, "researcher.commit.error");
				
				this.updateBreadCrumb(modelAndView, "Researcher","Investigador","My information","Mis datos",request);

			}

		return modelAndView;

	}

	// Ancillary methods --------------------------------------------------------------
	
	protected ModelAndView createRegisterModelAndView(final Researcher researcher) {
		ModelAndView result;
		result = this.createRegisterModelAndView(researcher, null);
		return result;
	}

	protected ModelAndView createRegisterModelAndView(final Researcher researcher, final String message) {
		ModelAndView result;

		result = new ModelAndView("researcher/register");

		result.addObject("researcher", researcher);
		result.addObject("message", message);

		return result;
	}
	
	protected ModelAndView createEditModelAndView(final Researcher researcher) {
		ModelAndView result;
		result = this.createEditModelAndView(researcher, null);
		return result;
	}

	protected ModelAndView createEditModelAndView(final Researcher researcher, final String message) {
		ModelAndView result;

		result = new ModelAndView("researcher/edit");

		result.addObject("researcher", researcher);
		result.addObject("message", message);


		return result;
	}
	
	protected void updateBreadCrumb(ModelAndView mv,String parentEN,String parentES,String currentPageEN,String currentPageES,final HttpServletRequest request) {
		
		final LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(request);
		
		if(parentEN!=null && parentES!=null && currentPageEN!=null && currentPageES!=null) {
			if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				mv.addObject("parent", parentEN);
				mv.addObject("currentPage", currentPageEN);
			}else {
				mv.addObject("parent", parentES);
				mv.addObject("currentPage", currentPageES);
			}
		}else if(parentEN==null || parentES==null ) {
			
			if (localeResolver.resolveLocale(request).getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				mv.addObject("currentPage", currentPageEN);
			}else {
				mv.addObject("currentPage", currentPageES);
			}
			
		}else {
			
			mv.addObject("currentPage", "Index");
			
		}
		
		
		
	}

}
