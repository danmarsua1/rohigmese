
package services;

import java.util.ArrayList;
import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Localization;
import domain.Researcher;
import domain.Zone;
import repositories.ZoneRepository;

@Service
@Transactional
public class ZoneService {

	//Repository--------------------------------------------------------------------------------
	@Autowired
	private ZoneRepository	zoneRepository;
	
	//Services----------------------------------------------------------------------------------
	@Autowired
	private ResearcherService researcherService;


	//Constructor-------------------------------------------------------------------------------
	public ZoneService() {
		super();
	}

	//CRUDS Methods-----------------------------------------------------------------------------
	public Zone create() {
		final Researcher researcher = this.researcherService.findByPrincipal();
		Assert.notNull(researcher);

		final Zone zone = new Zone();

		return zone;
	}
	
	public Collection<Zone> findAll() {
		return this.zoneRepository.findAll();
	}

	public Zone findOne(final Integer arg0) {
		return this.zoneRepository.findOne(arg0);
	}

	public Zone save(final Zone zone) {
		Assert.notNull(zone);

		Zone saved;
		
		zone.setName(zone.getName().toUpperCase());
		
		saved = this.zoneRepository.save(zone);

		return saved;
	}

	//Other Methods-----------------------------------------------------------------------------
	
	public boolean uniqueZone(String name) {
		Boolean result = true;
		
		Collection<Zone> zones = new ArrayList<Zone>(this.zoneRepository.findAll());
		
		final ArrayList<String> names = new ArrayList<>();
		
		for (final Zone i : zones)
			names.add(i.getName().toLowerCase());
		
		if (names.contains(name.toLowerCase())) {
			result = false;
		}
		
		return result;
	}

	public boolean verifyLocalization(Double longitude, Double latitude) {
		Boolean result = true;
		
		final String LONGITUDE_PATTERN="^(\\+|-)?(?:180(?:(?:\\.0{1,7})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\\.[0-9]{1,7})?))$";
		final String LATITUDE_PATTERN="^(\\+|-)?(?:90(?:(?:\\.0{1,7})?)|(?:[0-9]|[1-8][0-9])(?:(?:\\.[0-9]{1,7})?))$";
		//final String PATTERN="^-?[\\d]*\\.[\\d]*$";
		
		if (!(String.valueOf(longitude).matches(LONGITUDE_PATTERN) && String.valueOf(latitude).matches(LATITUDE_PATTERN)))
			result = false;
		
		return result;
		
	}

}
