
package services;

import java.util.Collection;
import java.util.Date;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import repositories.AdministratorRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import domain.Administrator;

@Service
@Transactional
public class AdministratorService {

	//Repository--------------------------------------------------------------------------------
	@Autowired
	private AdministratorRepository	administratorRepository;


	//Constructor-------------------------------------------------------------------------------
	public AdministratorService() {
		super();
	}

	//CRUDS Methods-----------------------------------------------------------------------------
	public Collection<Administrator> findAll() {
		return this.administratorRepository.findAll();
	}

	public Administrator findOne(final Integer arg0) {
		return this.administratorRepository.findOne(arg0);
	}

	public Administrator save(final Administrator administrator) {
		Assert.notNull(administrator);

		Administrator saved;

		saved = this.administratorRepository.save(administrator);

		return saved;
	}

	//Other Methods-----------------------------------------------------------------------------
	public Administrator findByPrincipal() {
		final UserAccount userAccount = LoginService.getPrincipal();

		final Administrator administrator = this.administratorRepository.findByPrincipal(userAccount.getId());

		Assert.isTrue(administrator.getUserAccount().equals(userAccount));

		return administrator;
	}

	public Boolean isAdministrator() {
		Boolean res = false;

		try {
			final Authority aut = new Authority();

			aut.setAuthority(Authority.ADMINISTRATOR);

			res = LoginService.getPrincipal().getAuthorities().contains(aut);
		} catch (final Exception e) {
			res = false;
		}

		return res;
	}
	
	public void sendEmail(Session session, String toEmail, String subject, String body){
		
		final UserAccount userAccount = LoginService.getPrincipal();

		final Administrator administrator = this.administratorRepository.findByPrincipal(userAccount.getId());
		
		try{
			
	      MimeMessage msg = new MimeMessage(session);
	      //set message headers
	      msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
	      msg.addHeader("format", "flowed");
	      msg.addHeader("Content-Transfer-Encoding", "8bit");

	      msg.setFrom(new InternetAddress(administrator.getEmail(), "ROHIGMESE Support"));

	      msg.setReplyTo(InternetAddress.parse(administrator.getEmail(), false));

	      msg.setSubject(subject, "UTF-8");

	      msg.setText(body, "UTF-8");

	      msg.setSentDate(new Date(System.currentTimeMillis()));

	      msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail, false));
	      
	      System.out.println("Message is ready");
    	  Transport.send(msg);  

	      System.out.println("Email Sent Successfully!!");
	    }
	    catch (Exception e) {
	      e.printStackTrace();
	    }
	}

}
