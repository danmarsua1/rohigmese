
package services;

import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.History;
import domain.Researcher;
import repositories.HistoryRepository;

@Service
@Transactional
public class HistoryService {

	//Repository--------------------------------------------------------------------------------
	@Autowired
	private HistoryRepository	historyRepository;
	
	//Services----------------------------------------------------------------------------------
	@Autowired
	private ResearcherService researcherService;


	//Constructor-------------------------------------------------------------------------------
	public HistoryService() {
		super();
	}

	//CRUDS Methods-----------------------------------------------------------------------------
	public History create() {
		final Researcher researcher = this.researcherService.findByPrincipal();
		Assert.notNull(researcher);

		final History history = new History();

		return history;
	}
	
	public Collection<History> findAll() {
		return this.historyRepository.findAll();
	}

	public History findOne(final Integer arg0) {
		return this.historyRepository.findOne(arg0);
	}

	public History save(final History history) {
		Assert.notNull(history);

		History saved;
		
		saved = this.historyRepository.save(history);

		return saved;
	}

}
