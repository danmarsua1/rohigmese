
package services;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Invitation;
import domain.Researcher;
import repositories.ResearcherRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import security.UserAccountRepository;

@Service
@Transactional
public class ResearcherService {

	//Repository--------------------------------------------------------------------------------
	@Autowired
	private ResearcherRepository	researcherRepository;
	
	@Autowired
	private UserAccountRepository userAccountRepository;
	
	//Services----------------------------------------------------------------------------------
	@Autowired
	private SecurityService security;
	
	@Autowired
	private InvitationService invitationService;
	
	


	//Constructor-------------------------------------------------------------------------------
	public ResearcherService() {
		super();
	}

	//CRUDS Methods-----------------------------------------------------------------------------
	public Researcher create(String username) {

		final UserAccount userAccount = new UserAccount();
		
		userAccount.setUsername(username);

		final Authority authority = new Authority();

		final Collection<Authority> authorities = userAccount.getAuthorities();

		authority.setAuthority(Authority.RESEARCHER);
		authorities.add(authority);
		userAccount.setAuthorities(authorities);

		final Researcher researcher = new Researcher();
		researcher.setUserAccount(userAccount);

		return researcher;
	}
	
	public Collection<Researcher> findAll() {
		return this.researcherRepository.findAll();
	}

	public Researcher findOne(final Integer arg0) {
		return this.researcherRepository.findOne(arg0);
	}

	public Researcher save(final Researcher researcher) {
		Assert.notNull(researcher);

		Researcher saved;

		saved = this.researcherRepository.save(researcher);

		return saved;
	}
	
	public void delete(final Researcher researcher) {
		this.researcherRepository.delete(researcher);
	}

	//Other Methods-----------------------------------------------------------------------------
	public void register(final Researcher researcher) throws NoSuchAlgorithmException {
		Assert.notNull(researcher);
		UserAccount userAccount;

		userAccount = researcher.getUserAccount();
		
		userAccount.setPassword(security.getHash(userAccount.getPassword()));
		researcher.setUserAccount(userAccount);
		
		Collection<Invitation> invitations = new ArrayList<Invitation>(this.invitationService.findAll());
		
		for (final Invitation inv : invitations)
			if(inv.getUsername().equals(researcher.getUserAccount().getUsername())) {
				researcher.setInvitation(inv);
				inv.setAccepted(true);
				this.invitationService.save(inv);
				break;
			}
	
		this.save(researcher);
	}
	
	public Researcher findByPrincipal() {
		final UserAccount userAccount = LoginService.getPrincipal();

		final Researcher researcher = this.researcherRepository.findByPrincipal(userAccount.getId());

		Assert.isTrue(researcher.getUserAccount().equals(userAccount));

		return researcher;
	}

	public Boolean isResearcher() {
		Boolean res = false;

		try {
			final Authority aut = new Authority();

			aut.setAuthority(Authority.RESEARCHER);

			res = LoginService.getPrincipal().getAuthorities().contains(aut);
		} catch (final Exception e) {
			res = false;
		}

		return res;
	}

	public boolean verifyCredentials(final String username, final String tmpPassword) {
		
		Boolean result = false;
		
		Collection<Invitation> invitations = new ArrayList<Invitation>(this.invitationService.findAll());
		
		final ArrayList<String> usernames = new ArrayList<>();
		final ArrayList<String> tmpPasswords = new ArrayList<>();
		
		for (final Invitation i : invitations)
			usernames.add(i.getUsername());
		
		for (final Invitation i : invitations)
			tmpPasswords.add(i.getTempPassword());
		
		if (usernames.contains(username) && tmpPasswords.contains(tmpPassword)) {
			result = true;
		}
		
		return result;
		
	}

	public boolean uniqueResearcher(final String username) {
		
		Boolean result = true;
		
		Collection<UserAccount> userAccounts = new ArrayList<UserAccount>(this.userAccountRepository.findAll());
		
		final ArrayList<String> usernames = new ArrayList<>();
		
		for (final UserAccount i : userAccounts)
			usernames.add(i.getUsername());
		
		if (usernames.contains(username)) {
			result = false;
		}
		
		return result;
		
	}

}
