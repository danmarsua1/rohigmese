
package services;

import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Researcher;
import domain.dataTables.Davis.Davis;
import domain.dataTables.Davis.DavisC7;
import domain.dataTables.Davis.DavisLisimeter;
import domain.dataTables.Davis.DavisLlanos;
import domain.dataTables.Davis.DavisSO;
import repositories.DavisRepository;

@Service
@Transactional
public class DavisService {

	//Repository--------------------------------------------------------------------------------
	@Autowired
	private DavisRepository	davisRepository;
	
	//Services----------------------------------------------------------------------------------
	@Autowired
	private ResearcherService researcherService;


	//Constructor-------------------------------------------------------------------------------
	public DavisService() {
		super();
	}

	//CRUDS Methods-----------------------------------------------------------------------------
	public Davis create() {
		final Researcher researcher = this.researcherService.findByPrincipal();
		Assert.notNull(researcher);

		final Davis davis = new Davis();

		return davis;
	}
	
	public Collection<Davis> findAll() {
		return this.davisRepository.findAll();
	}

	public Davis findOne(final Integer arg0) {
		return this.davisRepository.findOne(arg0);
	}

	public Davis save(final Davis davis) {
		Assert.notNull(davis);

		Davis saved;
		
		saved = this.davisRepository.save(davis);

		return saved;
	}

	//Other Methods-----------------------------------------------------------------------------
	public Collection<DavisLisimeter> getLisimeterData() {
		return this.davisRepository.getLisimeterData();
	}
	
	public Collection<DavisC7> getC7Data() {
		return this.davisRepository.getC7Data();
	}
	
	public Collection<DavisLlanos> getLlanosData() {
		return this.davisRepository.getLlanosData();
	}
	
	public Collection<DavisSO> getSOData() {
		return this.davisRepository.getSOData();
	}

}
