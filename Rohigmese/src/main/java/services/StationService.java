
package services;

import java.util.ArrayList;
import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Researcher;
import domain.Station;
import repositories.StationRepository;

@Service
@Transactional
public class StationService {

	//Repository--------------------------------------------------------------------------------
	@Autowired
	private StationRepository	stationRepository;
	
	//Services----------------------------------------------------------------------------------
	@Autowired
	private ResearcherService researcherService;


	//Constructor-------------------------------------------------------------------------------
	public StationService() {
		super();
	}

	//CRUDS Methods-----------------------------------------------------------------------------
	public Station create() {
		final Researcher researcher = this.researcherService.findByPrincipal();
		Assert.notNull(researcher);

		final Station station = new Station();

		return station;
	}
	
	public Collection<Station> findAll() {
		return this.stationRepository.findAll();
	}

	public Station findOne(final Integer arg0) {
		return this.stationRepository.findOne(arg0);
	}

	public Station save(final Station station) {
		Assert.notNull(station);

		Station saved;
		
		station.setName(station.getName().toUpperCase());
		
		saved = this.stationRepository.save(station);

		return saved;
	}

	//Other Methods-----------------------------------------------------------------------------
	
	public boolean uniqueStation(String name) {
		Boolean result = true;
		
		Collection<Station> stations = new ArrayList<Station>(this.stationRepository.findAll());
		
		final ArrayList<String> names = new ArrayList<>();
		
		for (final Station i : stations)
			names.add(i.getName().toLowerCase());
		
		if (names.contains(name.toLowerCase())) {
			result = false;
		}
		
		return result;
	}

	public boolean verifyLocalization(Double longitude, Double latitude) {
		Boolean result = true;
		
		final String LONGITUDE_PATTERN="^(\\+|-)?(?:180(?:(?:\\.0{1,7})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\\.[0-9]{1,7})?))$";
		final String LATITUDE_PATTERN="^(\\+|-)?(?:90(?:(?:\\.0{1,7})?)|(?:[0-9]|[1-8][0-9])(?:(?:\\.[0-9]{1,7})?))$";
		//final String PATTERN="^-?[\\d]*\\.[\\d]*$";
		
		if (!(String.valueOf(longitude).matches(LONGITUDE_PATTERN) && String.valueOf(latitude).matches(LATITUDE_PATTERN)))
			result = false;
		
		return result;
		
	}

}
