
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Invitation;
import repositories.InvitationRepository;
import security.UserAccount;
import security.UserAccountRepository;

@Service
@Transactional
public class InvitationService {

	//Repository--------------------------------------------------------------------------------
	@Autowired
	private InvitationRepository	invitationRepository;
	
	@Autowired
	private UserAccountRepository	userAccountRepository;
	
	//Services----------------------------------------------------------------------------------
	@Autowired
	private AdministratorService administratorService;


	//Constructor-------------------------------------------------------------------------------
	public InvitationService() {
		super();
	}

	//CRUDS Methods-----------------------------------------------------------------------------
	public Invitation create() {
		Assert.isTrue(this.administratorService.isAdministrator());

		final Invitation invitation = new Invitation();
		
		invitation.setMoment(new Date(System.currentTimeMillis() - 1000));
		
		//Contraseņa temporal
		String tmpPassword = "";

		do
			tmpPassword = this.tempPasswordGenerator();
		while (!this.isTempPasswordUnique(tmpPassword));
		
		invitation.setTempPassword(tmpPassword);
		invitation.setAccepted(false);

		return invitation;
	}
	
	public Collection<Invitation> findAll() {
		return this.invitationRepository.findAll();
	}

	public Invitation findOne(final Integer arg0) {
		return this.invitationRepository.findOne(arg0);
	}

	public Invitation save(final Invitation invitation) {
		Assert.notNull(invitation);

		Invitation saved;

		saved = this.invitationRepository.save(invitation);

		return saved;
	}
	
	//Bussiness Methods----------
	public String tempPasswordGenerator() {
		
		final char[] elems = {
				'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
				't', 'u', 'v', 'w', 'x', 'y', 'z',
				'A', 'B', 'C', 'D', 'E', 'F', 'G' ,'H' ,'I' ,'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S',
				'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
				'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
			};
		
		final char[] group = new char[10];

		final String secuency;

		for (int i = 0; i < 10; i++) {
			final int el = (int) (Math.random() * elems.length);
			group[i] = elems[el];
		}

		secuency = new String(group);
		return secuency;
		
	}
	
	public boolean isTempPasswordUnique(final String password) {

		Boolean result = true;

		final ArrayList<Invitation> invitations = new ArrayList<>(this.invitationRepository.findAll());

		final ArrayList<String> passwords = new ArrayList<>();

		for (final Invitation i : invitations)
			passwords.add(i.getTempPassword());
		
		if (passwords.contains(password)) {
			result = false;
			this.tempPasswordGenerator();
		}
		
		return result;

	}

	public boolean isResearcherUnique(final String username, final String email) {
		Boolean result = true;

		final Collection<UserAccount> userAccounts = new ArrayList<UserAccount>(this.userAccountRepository.findAll());
		final Collection<Invitation> invitations = new ArrayList<Invitation>(this.invitationRepository.findAll());

		final ArrayList<String> usernames = new ArrayList<>();
		final ArrayList<String> emails = new ArrayList<>();

		for (final UserAccount u : userAccounts)
			usernames.add(u.getUsername());
		
		for (final Invitation i : invitations)
			usernames.add(i.getUsername());
		
		for (final Invitation i : invitations)
			emails.add(i.getRecipient());
		
		if (usernames.contains(username) || emails.contains(email)) {
			result = false;
		}
		
		return result;
	}

}
