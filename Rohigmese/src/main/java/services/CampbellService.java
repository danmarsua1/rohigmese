
package services;

import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Researcher;
import domain.dataTables.Campbell.Campbell;
import domain.dataTables.Campbell.CampbellC7DT1;
import domain.dataTables.Campbell.CampbellC7DT2;
import domain.dataTables.Campbell.CampbellLisimeter;
import domain.dataTables.Campbell.CampbellSOP2;
import domain.dataTables.Campbell.CampbellSOP3;
import repositories.CampbellRepository;

@Service
@Transactional
public class CampbellService {

	//Repository--------------------------------------------------------------------------------
	@Autowired
	private CampbellRepository	campbellRepository;
	
	//Services----------------------------------------------------------------------------------
	@Autowired
	private ResearcherService researcherService;


	//Constructor-------------------------------------------------------------------------------
	public CampbellService() {
		super();
	}

	//CRUDS Methods-----------------------------------------------------------------------------
	public Campbell create() {
		final Researcher researcher = this.researcherService.findByPrincipal();
		Assert.notNull(researcher);

		final Campbell campbell = new Campbell();

		return campbell;
	}
	
	public Collection<Campbell> findAll() {
		return this.campbellRepository.findAll();
	}

	public Campbell findOne(final Integer arg0) {
		return this.campbellRepository.findOne(arg0);
	}

	public Campbell save(final Campbell campbell) {
		Assert.notNull(campbell);

		Campbell saved;
		
		saved = this.campbellRepository.save(campbell);

		return saved;
	}

	//Other Methods-----------------------------------------------------------------------------
	public Collection<CampbellLisimeter> getLisimeterData() {
		return this.campbellRepository.getLisimeterData();
	}
	
	public Collection<CampbellC7DT1> getC7DT1Data() {
		return this.campbellRepository.getC7DT1Data();
	}
	
	public Collection<CampbellC7DT2> getC7DT2Data() {
		return this.campbellRepository.getC7DT2Data();
	}
	
	public Collection<CampbellSOP2> getSOP2Data() {
		return this.campbellRepository.getSOP2Data();
	}
	
	public Collection<CampbellSOP3> getSOP3Data() {
		return this.campbellRepository.getSOP3Data();
	}

}
