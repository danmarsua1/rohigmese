
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Device;
import domain.Researcher;
import domain.User;
import domain.dataTables.Helman.Helman;
import domain.dataTables.Helman.HelmanLisimeter;
import repositories.HelmanRepository;
import security.LoginService;
import security.UserAccount;

@Service
@Transactional
public class HelmanService {

	//Repository--------------------------------------------------------------------------------
	@Autowired
	private HelmanRepository	helmanRepository;
	
	//Services----------------------------------------------------------------------------------
	@Autowired
	private ResearcherService researcherService;
	@Autowired
	private DeviceService deviceService;


	//Constructor-------------------------------------------------------------------------------
	public HelmanService() {
		super();
	}

	//CRUDS Methods-----------------------------------------------------------------------------
	public Helman create() {
		final Researcher researcher = this.researcherService.findByPrincipal();
		Assert.notNull(researcher);

		final Helman helman = new Helman();

		return helman;
	}
	
	public Collection<Helman> findAll() {
		return this.helmanRepository.findAll();
	}

	public Helman findOne(final Integer arg0) {
		return this.helmanRepository.findOne(arg0);
	}

	public Helman save(final Helman helman) {
		Assert.notNull(helman);

		Helman saved;
		
		saved = this.helmanRepository.save(helman);

		return saved;
	}

	//Other Methods-----------------------------------------------------------------------------
	public Collection<HelmanLisimeter> getAllData() {
		return this.helmanRepository.getAllData();
	}

}
