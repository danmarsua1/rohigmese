
package services;

import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Researcher;
import domain.dataTables.Lisimeter.Lisimeter;
import domain.dataTables.Lisimeter.MeteoLisimeter;
import domain.dataTables.Lisimeter.WeightsLisimeter;
import repositories.LisimeterRepository;

@Service
@Transactional
public class LisimeterService {

	//Repository--------------------------------------------------------------------------------
	@Autowired
	private LisimeterRepository	lisimeterRepository;
	
	//Services----------------------------------------------------------------------------------
	@Autowired
	private ResearcherService researcherService;


	//Constructor-------------------------------------------------------------------------------
	public LisimeterService() {
		super();
	}

	//CRUDS Methods-----------------------------------------------------------------------------
	public Lisimeter create() {
		final Researcher researcher = this.researcherService.findByPrincipal();
		Assert.notNull(researcher);

		final Lisimeter lisimeter = new Lisimeter();

		return lisimeter;
	}

	public Lisimeter save(final Lisimeter lisimeter) {
		Assert.notNull(lisimeter);

		Lisimeter saved;
		
		saved = this.lisimeterRepository.save(lisimeter);

		return saved;
	}

	//Other Methods-----------------------------------------------------------------------------
	public Collection<MeteoLisimeter> getMeteoData() {
		return this.lisimeterRepository.getMeteoData();
	}
	
	public Collection<WeightsLisimeter> getWeightsData() {
		return this.lisimeterRepository.getWeightsData();
	}

}
