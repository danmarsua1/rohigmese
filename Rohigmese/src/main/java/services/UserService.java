
package services;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.User;
import repositories.UserRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import security.UserAccountRepository;

@Service
@Transactional
public class UserService {

	//Repository--------------------------------------------------------------------------------
	@Autowired
	private UserRepository	userRepository;
	@Autowired
	private UserAccountRepository userAccountRepository;
	
	//Services----------------------------------------------------------------------------------
	@Autowired
	private SecurityService security;


	//Constructor-------------------------------------------------------------------------------
	public UserService() {
		super();
	}

	//CRUDS Methods-----------------------------------------------------------------------------
	public User create() {

		final UserAccount userAccount = new UserAccount();

		final Authority authority = new Authority();

		final Collection<Authority> authorities = userAccount.getAuthorities();

		authority.setAuthority(Authority.USER);
		authorities.add(authority);
		userAccount.setAuthorities(authorities);

		final User user = new User();
		user.setUserAccount(userAccount);

		return user;
	}
	
	public Collection<User> findAll() {
		return this.userRepository.findAll();
	}

	public User findOne(final Integer arg0) {
		return this.userRepository.findOne(arg0);
	}

	public User save(final User user) {
		Assert.notNull(user);

		User saved;

		saved = this.userRepository.save(user);

		return saved;
	}
	
	public void delete(final User user) {
		this.userRepository.delete(user);
	}

	//Other Methods-----------------------------------------------------------------------------
	public void register(final User user) throws NoSuchAlgorithmException {
		Assert.notNull(user);
		UserAccount userAccount;

		userAccount = user.getUserAccount();

		userAccount.setPassword(security.getHash(userAccount.getPassword()));
		user.setUserAccount(userAccount);

		this.save(user);
	}
	
	public User findByPrincipal() {
		final UserAccount userAccount = LoginService.getPrincipal();

		final User user = this.userRepository.findByPrincipal(userAccount.getId());

		Assert.isTrue(user.getUserAccount().equals(userAccount));

		return user;
	}

	public Boolean isUser() {
		Boolean res = false;

		try {
			final Authority aut = new Authority();

			aut.setAuthority(Authority.USER);

			res = LoginService.getPrincipal().getAuthorities().contains(aut);
		} catch (final Exception e) {
			res = false;
		}

		return res;
	}

	public boolean uniqueUser(String username) {
		
		Boolean result = true;
		
		Collection<UserAccount> userAccounts = new ArrayList<UserAccount>(this.userAccountRepository.findAll());
		
		final ArrayList<String> usernames = new ArrayList<>();
		
		for (final UserAccount i : userAccounts)
			usernames.add(i.getUsername());
		
		if (usernames.contains(username)) {
			result = false;
		}
		
		return result;
	}

}
