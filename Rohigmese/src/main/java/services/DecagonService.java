
package services;

import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Researcher;
import domain.dataTables.Decagon.Decagon;
import domain.dataTables.Decagon.DecagonLaguna;
import domain.dataTables.Decagon.DecagonLlanos;
import domain.dataTables.Decagon.DecagonPalacio;
import repositories.DecagonRepository;

@Service
@Transactional
public class DecagonService {

	//Repository--------------------------------------------------------------------------------
	@Autowired
	private DecagonRepository	decagonRepository;
	
	//Services----------------------------------------------------------------------------------
	@Autowired
	private ResearcherService researcherService;


	//Constructor-------------------------------------------------------------------------------
	public DecagonService() {
		super();
	}

	//CRUDS Methods-----------------------------------------------------------------------------
	public Decagon create() {
		final Researcher researcher = this.researcherService.findByPrincipal();
		Assert.notNull(researcher);

		final Decagon decagon = new Decagon();

		return decagon;
	}
	
	public Collection<Decagon> findAll() {
		return this.decagonRepository.findAll();
	}

	public Decagon findOne(final Integer arg0) {
		return this.decagonRepository.findOne(arg0);
	}

	public Decagon save(final Decagon decagon) {
		Assert.notNull(decagon);

		Decagon saved;
		
		saved = this.decagonRepository.save(decagon);

		return saved;
	}

	//Other Methods-----------------------------------------------------------------------------
	public Collection<DecagonLaguna> getLagunaData() {
		return this.decagonRepository.getLagunaData();
	}
	
	public Collection<DecagonLlanos> getLlanosData() {
		return this.decagonRepository.getLlanosData();
	}
	
	public Collection<DecagonPalacio> getPalacioData() {
		return this.decagonRepository.getPalacioData();
	}

}
