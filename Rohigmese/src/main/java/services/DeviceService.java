
package services;

import java.util.ArrayList;
import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Researcher;
import domain.Device;
import repositories.DeviceRepository;

@Service
@Transactional
public class DeviceService {

	//Repository--------------------------------------------------------------------------------
	@Autowired
	private DeviceRepository	deviceRepository;
	
	//Services----------------------------------------------------------------------------------
	@Autowired
	private ResearcherService researcherService;


	//Constructor-------------------------------------------------------------------------------
	public DeviceService() {
		super();
	}

	//CRUDS Methods-----------------------------------------------------------------------------
	public Device create() {
		final Researcher researcher = this.researcherService.findByPrincipal();
		Assert.notNull(researcher);

		final Device device = new Device();

		return device;
	}
	
	public Collection<Device> findAll() {
		return this.deviceRepository.findAll();
	}

	public Device findOne(final Integer arg0) {
		return this.deviceRepository.findOne(arg0);
	}

	public Device save(final Device device) {
		Assert.notNull(device);

		Device saved;
		
		//device.setName(device.getName().toUpperCase());
		
		saved = this.deviceRepository.save(device);

		return saved;
	}

	//Other Methods-----------------------------------------------------------------------------

	public boolean verifyLocalization(Double longitude, Double latitude) {
		Boolean result = true;
		
		final String LONGITUDE_PATTERN="^(\\+|-)?(?:180(?:(?:\\.0{1,7})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\\.[0-9]{1,7})?))$";
		final String LATITUDE_PATTERN="^(\\+|-)?(?:90(?:(?:\\.0{1,7})?)|(?:[0-9]|[1-8][0-9])(?:(?:\\.[0-9]{1,7})?))$";
		//final String PATTERN="^-?[\\d]*\\.[\\d]*$";
		
		if (!(String.valueOf(longitude).matches(LONGITUDE_PATTERN) && String.valueOf(latitude).matches(LATITUDE_PATTERN)))
			result = false;
		
		return result;
		
	}

}
