
package services;

import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Researcher;
import domain.dataTables.Manual.Manual;
import domain.dataTables.Manual.ManualSOP1;
import repositories.ManualRepository;

@Service
@Transactional
public class ManualService {

	//Repository--------------------------------------------------------------------------------
	@Autowired
	private ManualRepository	manualRepository;
	
	//Services----------------------------------------------------------------------------------
	@Autowired
	private ResearcherService researcherService;


	//Constructor-------------------------------------------------------------------------------
	public ManualService() {
		super();
	}

	//CRUDS Methods-----------------------------------------------------------------------------
	public Manual create() {
		final Researcher researcher = this.researcherService.findByPrincipal();
		Assert.notNull(researcher);

		final Manual manual = new Manual();

		return manual;
	}
	
	public Collection<Manual> findAll() {
		return this.manualRepository.findAll();
	}

	public Manual findOne(final Integer arg0) {
		return this.manualRepository.findOne(arg0);
	}

	public Manual save(final Manual manual) {
		Assert.notNull(manual);

		Manual saved;
		
		saved = this.manualRepository.save(manual);

		return saved;
	}

	//Other Methods-----------------------------------------------------------------------------
	public Collection<ManualSOP1> getP1Data() {
		return this.manualRepository.getP1Data();
	}

}
