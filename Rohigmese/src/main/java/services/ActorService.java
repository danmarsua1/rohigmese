
package services;

import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import repositories.ActorRepository;
import security.LoginService;
import security.UserAccount;
import domain.Actor;

@Service
@Transactional
public class ActorService {

	//Repository--------------------------------------------------------------------------------
	@Autowired
	private ActorRepository	actorRepository;


	//Constructor-------------------------------------------------------------------------------
	public ActorService() {
		super();
	}

	//CRUDS Methods-----------------------------------------------------------------------------
	public Collection<Actor> findAll() {
		return this.actorRepository.findAll();
	}

	public Actor findOne(final Integer arg0) {
		return this.actorRepository.findOne(arg0);
	}

	public Actor save(final Actor actor) {
		Assert.notNull(actor);
		Actor saved;

		saved = this.actorRepository.save(actor);

		return saved;
	}

	//Other Methods-----------------------------------------------------------------------------
	public Actor findByPrincipal() {
		final UserAccount userAccount = LoginService.getPrincipal();
		final Actor actor = this.actorRepository.findByPrincipal(userAccount.getId());

		Assert.isTrue(actor.getUserAccount().equals(userAccount));

		return actor;
	}

}
