
package repositories;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Administrator;
import domain.dataTables.Helman.Helman;
import domain.dataTables.Helman.HelmanLisimeter;

@Repository
public interface HelmanRepository extends JpaRepository<Helman, Integer> {

	

	//Queries-----------------------------------------------------------------------------
	@Query("select h from HelmanLisimeter h order by h.moment DESC")
	public Collection<HelmanLisimeter> getAllData();

}
