
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.dataTables.Campbell.Campbell;
import domain.dataTables.Campbell.CampbellC7DT1;
import domain.dataTables.Campbell.CampbellC7DT2;
import domain.dataTables.Campbell.CampbellLisimeter;
import domain.dataTables.Campbell.CampbellSOP2;
import domain.dataTables.Campbell.CampbellSOP3;

@Repository
public interface CampbellRepository extends JpaRepository<Campbell, Integer> {

	//Queries-----------------------------------------------------------------------------
	@Query("select c from CampbellLisimeter c order by c.moment DESC")
	public Collection<CampbellLisimeter> getLisimeterData();
	
	@Query("select c from CampbellC7DT1 c order by c.moment DESC")
	public Collection<CampbellC7DT1> getC7DT1Data();
	
	@Query("select c from CampbellC7DT2 c order by c.moment DESC")
	public Collection<CampbellC7DT2> getC7DT2Data();
	
	@Query("select c from CampbellSOP2 c order by c.moment DESC")
	public Collection<CampbellSOP2> getSOP2Data();
	
	@Query("select c from CampbellSOP3 c order by c.moment DESC")
	public Collection<CampbellSOP3> getSOP3Data();

}
