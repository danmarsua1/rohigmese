
package repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Researcher;

@Repository
public interface ResearcherRepository extends JpaRepository<Researcher, Integer> {

	//Queries-----------------------------------------------------------------------------

	@Query("select r from Researcher r where r.userAccount.id=?1")
	public Researcher findByPrincipal(int id);

}
