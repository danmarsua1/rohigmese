
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.dataTables.Davis.Davis;
import domain.dataTables.Davis.DavisC7;
import domain.dataTables.Davis.DavisLisimeter;
import domain.dataTables.Davis.DavisLlanos;
import domain.dataTables.Davis.DavisSO;

@Repository
public interface DavisRepository extends JpaRepository<Davis, Integer> {

	//Queries-----------------------------------------------------------------------------
	@Query("select d from DavisLisimeter d order by d.moment DESC")
	public Collection<DavisLisimeter> getLisimeterData();
	
	@Query("select d from DavisC7 d order by d.moment DESC")
	public Collection<DavisC7> getC7Data();
	
	@Query("select d from DavisLlanos d order by d.moment DESC")
	public Collection<DavisLlanos> getLlanosData();
	
	@Query("select d from DavisSO d order by d.moment DESC")
	public Collection<DavisSO> getSOData();

}
