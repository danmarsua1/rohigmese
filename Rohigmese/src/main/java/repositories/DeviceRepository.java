
package repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import domain.Device;

@Repository
public interface DeviceRepository extends JpaRepository<Device, Integer> {

	//Queries-----------------------------------------------------------------------------


}
