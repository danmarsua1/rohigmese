
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.dataTables.Lisimeter.Lisimeter;
import domain.dataTables.Lisimeter.MeteoLisimeter;
import domain.dataTables.Lisimeter.WeightsLisimeter;

@Repository
public interface LisimeterRepository extends JpaRepository<Lisimeter, Integer> {

	

	//Queries-----------------------------------------------------------------------------
	@Query("select m from MeteoLisimeter m order by m.moment DESC")
	public Collection<MeteoLisimeter> getMeteoData();
	
	@Query("select w from WeightsLisimeter w order by w.moment DESC")
	public Collection<WeightsLisimeter> getWeightsData();

}
