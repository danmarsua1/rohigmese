
package repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import domain.Station;

@Repository
public interface StationRepository extends JpaRepository<Station, Integer> {

	//Queries-----------------------------------------------------------------------------


}
