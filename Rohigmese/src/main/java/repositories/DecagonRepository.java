
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.dataTables.Campbell.Campbell;
import domain.dataTables.Campbell.CampbellLisimeter;
import domain.dataTables.Decagon.Decagon;
import domain.dataTables.Decagon.DecagonLaguna;
import domain.dataTables.Decagon.DecagonLlanos;
import domain.dataTables.Decagon.DecagonPalacio;

@Repository
public interface DecagonRepository extends JpaRepository<Decagon, Integer> {

	//Queries-----------------------------------------------------------------------------
	@Query("select d from DecagonLaguna d order by d.moment DESC")
	public Collection<DecagonLaguna> getLagunaData();
	
	@Query("select d from DecagonLlanos d order by d.moment DESC")
	public Collection<DecagonLlanos> getLlanosData();
	
	@Query("select d from DecagonPalacio d order by d.moment DESC")
	public Collection<DecagonPalacio> getPalacioData();

}
