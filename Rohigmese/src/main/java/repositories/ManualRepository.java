
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.dataTables.Manual.Manual;
import domain.dataTables.Manual.ManualSOP1;

@Repository
public interface ManualRepository extends JpaRepository<Manual, Integer> {

	//Queries-----------------------------------------------------------------------------
	@Query("select m from ManualSOP1 m order by m.moment DESC")
	public Collection<ManualSOP1> getP1Data();

}
