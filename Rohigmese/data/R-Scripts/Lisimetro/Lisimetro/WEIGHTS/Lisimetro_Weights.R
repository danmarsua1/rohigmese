
### FUNCIONES AUXILIARES ###
process_weights <- function(datos_weights){
  
  # Eliminar todas las filas con fecha == NA
  na_rows<-which(is.na(datos_weights[,1]))
  if (length(na_rows)>0)
    datos_weights<-datos_weights[-na_rows,]
  
  ### PESO OLLA (Kg) ###
  
  #---Parametros---#
  
  #Salto max. (en 1 minuto)
  threshold=1
  
  #---Procesado---#
  
  # L01_LCB_D_000
  indices<-which( abs(datos_weights[,2]-lag(datos_weights[,2]))>threshold | datos_weights[,2]<0 )
  datos_weights[indices,2]<-NA
  
  ##################
  
  ### PESO LISIMETRO (Kg) ###
  
  #---Parametros---#
  
  #Salto max. (en 1 minuto)
  threshold=1
  
  #---Procesado---#
  
  # L01_LCB_L_000
  indices<-which( abs(datos_weights[,3]-lag(datos_weights[,3]))>threshold | datos_weights[,3]<=0 )
  datos_weights[indices,3]<-NA
  
  ##################
  
  return(datos_weights)
  
}

#LOG
log_file <- "LOG.log"

#Archivos necesarios
file_weights = "INPUT/Lisimetro_Weights.csv"

#Numero de archivos procesados
num_files = 1


# Comprobar que estan todos los archivos que hacen falta
files<-list.files(path = "INPUT",pattern = "\\.(txt|csv|dat|xlsx)$",recursive = TRUE)

errores<-0

tryCatch({
  
  cat("\nCARGANDO SCRIPT...")
  
  if (!(files[1]==gsub("INPUT/", "", file_weights))){

    cat(sprintf(paste(format(Sys.time(), "%A %d %b. %Y, %T "),"[ERROR] Archivo de pesos del lisimetro: Falta el archivo '",file_weights,"'"," en el directorio INPUT!","\n",sep="")), file = log_file, append = TRUE)
    
    stop("Ver archivo LOG para mas informacion",call. = FALSE)
    
  }else{
    
    cat(sprintf(paste(format(Sys.time(), "%A %d %b. %Y, %T "),"[INFO] INICIO DEL PROGRAMA. ","\n",sep="")), file = log_file, append = TRUE)
    
    ##################################
    ######### FICHERO PESOS ##########
    ##################################
    
    # Leer fichero Pesos
    cat(sprintf(paste(format(Sys.time(), "%A %d %b. %Y, %T "),"[INFO] PESOS LISIMETRO: Importando datos... ","\n",sep="")), file = log_file, append = TRUE)
    
    Weights<-read.table("INPUT/Lisimetro_Weights.csv",header=FALSE,dec=".",sep=",",skip=1,stringsAsFactors=FALSE)
    
    # Convertir la primera columna a fecha (para serie temporal)
    Weights[,1]<- as.POSIXct(Weights[,1],format="%Y/%m/%d %H:%M:%OS")
    
    cat(sprintf(paste(format(Sys.time(), "%A %d %b. %Y, %T "),"[INFO] PESOS LISIMETRO: Datos importados al script (",length(Weights[,1])," filas).\n",sep="")), file = log_file, append = TRUE)
    
    
    ### PROCESAMIENTO FICHERO PESOS ###
    
    # Filas duplicadas
    cat(sprintf(paste(format(Sys.time(), "%A %d %b. %Y, %T "),"[INFO] PESOS LISIMETRO: Eliminando filas duplicadas...","\n",sep="")), file = log_file, append = TRUE)
    
    if ( length(which(duplicated(Weights[,1])))>0 ){
      
      duplicated_rows<-which(duplicated(Weights[,1]))
      
      # Eliminar filas duplicadas
      Weights<-Weights[-duplicated_rows,]
      
    }
    
    
    # Conversion de datos de las columnas a numero  y limpieza de datos nulos o no validos
    # (Todo lo que no sea numerico, se convierte a 'NA')
    cat(sprintf(paste(format(Sys.time(), "%A %d %b. %Y, %T "),"[INFO] PESOS LISIMETRO: Conversion de datos y eliminacion de valores no validos...","\n",sep="")), file = log_file, append = TRUE)
    
    Weights[,9]<-as.numeric(as.character(Weights[,9]))
    Weights[,10]<-as.numeric(as.character(Weights[,10]))
    
    cat(sprintf(paste(format(Sys.time(), "%A %d %b. %Y, %T "),"[INFO] PESOS LISIMETRO: Inicio del procesamiento de datos...","\n",sep="")), file = log_file, append = TRUE)
    
    
    #Pasar fichero pesos a tabla mas manejable (solo 3 parametros)
    Weights<-Weights[,c(1,9,10)]
    
    
    # Procesar fichero pesos
    Weights<-process_weights(Weights)
    
    cat(sprintf(paste(format(Sys.time(), "%A %d %b. %Y, %T "),"[INFO] PESOS LISIMETRO: Procesamiento terminado.","\n",sep="")), file = log_file, append = TRUE)
    
    
    # Escritura fichero pesos
    cat(sprintf(paste(format(Sys.time(), "%A %d %b. %Y, %T "),"[INFO] PESOS LISIMETRO: Exportando datos...","\n",sep="")), file = log_file, append = TRUE)
    write.table(Weights, "OUTPUT/EXPORTED_LISIMETRO_WEIGHTS.csv", sep=";", col.names = c("TIMESTAMP","PESO OLLA","PESO LISIMETRO"), row.names=FALSE, dec=".", quote=FALSE)
    
    cat(sprintf(paste(format(Sys.time(), "%A %d %b. %Y, %T "),"[INFO] PESOS LISIMETRO: DATOS EXPORTADOS.","\n",sep="")), file = log_file, append = TRUE)
    
  }
  
  
},
error = function(e){
  errores<-e$message
  cat(sprintf(paste(format(Sys.time(), "%A %d %b. %Y, %T "),"[ERROR] Se ha producido un error interno: ",e$message,".\n",sep="")), file = log_file, append = TRUE)
  stop("Ver archivo LOG para mas informacion",call. = FALSE)
},
finally = {
  
  cat(sprintf(paste(format(Sys.time(), "%A %d %b. %Y, %T "),"[INFO] FIN DEL PROGRAMA.","\n",sep="")), file = log_file, append = TRUE)
  cat("\nTERMINADO. FIN DEL PROGRAMA.\n")
  
}
)