
# library(openxlsx)
# library(tidyr)
# library(dplyr)
# library(DescTools)
# library(ggplot2)
# library(rstudioapi)
# library(lubridate)
# library(scales)
# library(png)
# library(cowplot)
# library(zoo)

#TIMESTAMP;VWC_PORT1;TEMP_PORT1;EC_PORT1;VWC_PORT2;TEMP_PORT2;EC_PORT2;VWC_PORT3;TEMP_PORT3;EC_PORT3;
#VWC_PORT4;TEMP_PORT4;EC_PORT4

datos<-read.csv2("OUTPUT/EXPORTED_MARISMILLAS_DECAGON_PALACIO.csv",header=TRUE,dec=".",sep=";",stringsAsFactors=FALSE,na.strings="---")

datos[,1]<- as.POSIXct(datos[,1],format="%Y-%m-%d %H:%M:%S")

datos=data.frame(fecha=datos[,1],wvc_p1=datos[,2],ec_p1=datos[,4],tmp_p1=datos[,3],
                 wvc_p2=datos[,5],ec_p2=datos[,7],tmp_p2=datos[,6],
                 wvc_p3=datos[,8],ec_p3=datos[,10],tmp_p3=datos[,9],
                 wvc_p4=datos[,11],ec_p4=datos[,13],tmp_p4=datos[,12])

vwc_plot<-ggplot() +
  geom_line(data=datos, aes(y=wvc_p1, x=fecha, colour = "P1: 3.0m"),size=1) +
  geom_line(data=datos, aes(y=wvc_p2, x=fecha, colour = "P2: 2.20m"),size=1) +
  geom_line(data=datos, aes(y=wvc_p3, x=fecha, colour = "P3: 1.60m"),size=1) +
  geom_line(data=datos, aes(y=wvc_p4, x=fecha, colour = "P4: 1.20m"),size=1) +
  scale_x_datetime(date_labels = "%b-%y",date_breaks = "1 month") +
  scale_y_continuous(labels = function(x) round(as.numeric(x), digits=3)) +
  scale_color_brewer(palette="Set1") +
  labs(y=expression(paste("VWC ",(m^3/m^3))),colour = "Depths") +
  theme_bw() +
  theme(legend.position = "top",
          legend.title = element_blank(),
          legend.text = element_text(size=16),
          plot.margin = margin(0.5, 1, 0.5, 1, "cm"),
          axis.text.x = element_blank(),
          axis.title.x = element_blank(),
          axis.text.y.left = element_text(size=16),
          axis.title.y.left = element_text(size=13),
          panel.grid.major.x = element_line(color = "#555555",linetype="dashed"),
          plot.title = element_text(size = 15,hjust = 0.5))

ec_plot<-ggplot() +
  geom_line(data=datos, aes(y=ec_p1, x=fecha, colour = "3.0m"),size=1) +
  geom_line(data=datos, aes(y=ec_p2, x=fecha, colour = "2.20m"),size=1) +
  geom_line(data=datos, aes(y=ec_p3, x=fecha, colour = "1.60m"),size=1) +
  geom_line(data=datos, aes(y=ec_p4, x=fecha, colour = "1.20m"),size=1) +
  scale_x_datetime(date_labels = "%b-%y",date_breaks = "1 month") +
  scale_y_continuous(labels = function(x) round(as.numeric(x), digits=3)) +
  scale_color_brewer(palette="Set1") +
  labs(y=expression(paste("EC ",(dS*m^{-1}))),colour = "Depths") +
  theme_bw() +
  theme(legend.position="none",
        plot.margin = margin(0.5, 1, 0.5, 1, "cm"),
        axis.text.x = element_blank(),
        axis.title.x = element_blank(),
        axis.text.y.left = element_text(size=16),
        axis.title.y.left = element_text(size=13),
        panel.grid.major.x = element_line(color = "#555555",linetype="dashed"),
        plot.title = element_text(size = 15,hjust = 0.5))

tmp_plot<-ggplot() +
  geom_line(data=datos, aes(y=tmp_p1, x=fecha, colour = "3.0m"),size=1) +
  geom_line(data=datos, aes(y=tmp_p2, x=fecha, colour = "2.20m"),size=1) +
  geom_line(data=datos, aes(y=tmp_p3, x=fecha, colour = "1.60m"),size=1) +
  geom_line(data=datos, aes(y=tmp_p4, x=fecha, colour = "1.20m"),size=1) +
  scale_x_datetime(date_labels = "%b-%y",date_breaks = "1 month") +
  scale_color_brewer(palette="Set1") +
  labs(y=expression(paste("Temperature (",degree,"C)")),colour = "Depths") +
  theme_bw() +
  theme(legend.position="none",
        plot.margin = margin(0.5, 1, 0.5, 2, "cm"),
        axis.text.x = element_text(size=14,angle = 25, vjust = 1.0, hjust = 1.0),
        axis.title.x = element_blank(),
        axis.text.y.left = element_text(size=16),
        axis.title.y.left = element_text(size=13),
        panel.grid.major.x = element_line(color = "#555555",linetype="dashed"),
        plot.title = element_text(size = 15,hjust = 0.5))

graph<-plot_grid(
  vwc_plot,ec_plot,tmp_plot,
nrow = 3,
ncol = 1
)
  
title <- ggdraw() + 
draw_label(
"Decagon device parameters (Different depths below ground)",
hjust = 0.5,
size=15
)
  
graph<-plot_grid(title,graph,ncol=1,rel_heights = c(0.05,1,1,1))

#plot(graph)

