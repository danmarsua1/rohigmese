
### FUNCIONES AUXILIARES ###
process_campbell <- function(datos_campbell){
  
  # Eliminar todas las filas con fecha == NA
  na_rows<-which(is.na(datos_campbell[,1]))
  if (length(na_rows)>0)
    datos_campbell<-datos_campbell[-na_rows,]
  
  ### HUMEDAD (m3/m3) ###
  
  #---Parametros---#
  
  # Rango
  min=0
  max=1
  
  #---Procesado---#
  
  # Humedad Port 1
  indices<-which(datos_campbell[,3]<min | datos_campbell[,3]>max)
  if (length(indices)>0)
    datos_campbell<-datos_campbell[-indices,]
  
  # Humedad Port 2
  indices<-which(datos_campbell[,6]<min | datos_campbell[,6]>max)
  if (length(indices)>0)
    datos_campbell<-datos_campbell[-indices,]
  
  # Humedad Port 3
  indices<-which(datos_campbell[,9]<min | datos_campbell[,9]>max)
  if (length(indices)>0)
    datos_campbell<-datos_campbell[-indices,]
  
  # Humedad Port 4
  indices<-which(datos_campbell[,12]<min | datos_campbell[,12]>max)
  if (length(indices)>0)
    datos_campbell<-datos_campbell[-indices,]
  
  ##################
  
  
  ### ELECTROCONDUCTIVIDAD (dS*m-1) ###
  
  #---Parametros---#
  
  # Rango
  min=0
  max=3
  
  #---Procesado---#
  
  # Electroconductividad Port 1
  indices<-which(datos_campbell[,4]<min | datos_campbell[,4]>max)
  if (length(indices)>0)
    datos_campbell<-datos_campbell[-indices,]
  
  # Electroconductividad Port 2
  indices<-which(datos_campbell[,7]<min | datos_campbell[,7]>max)
  if (length(indices)>0)
    datos_campbell<-datos_campbell[-indices,]
  
  # Electroconductividad Port 3
  indices<-which(datos_campbell[,10]<min | datos_campbell[,10]>max)
  if (length(indices)>0)
    datos_campbell<-datos_campbell[-indices,]
  
  # Electroconductividad Port 4
  indices<-which(datos_campbell[,13]<min | datos_campbell[,13]>max)
  if (length(indices)>0)
    datos_campbell<-datos_campbell[-indices,]
  
  ##################
  
  
  ### TEMPERATURA (C) ###
  
  #---Parametros---#
  
  # Rango
  min=0
  max=60
  
  #---Procesado---#
  
  # Temp Port 1
  indices<-which(datos_campbell[,5]<min | datos_campbell[,5]>max)
  if (length(indices)>0)
    datos_campbell<-datos_campbell[-indices,]
  
  # Temp Port 2
  indices<-which(datos_campbell[,8]<min | datos_campbell[,8]>max)
  if (length(indices)>0)
    datos_campbell<-datos_campbell[-indices,]
  
  # Temp Port 3
  indices<-which(datos_campbell[,11]<min | datos_campbell[,11]>max)
  if (length(indices)>0)
    datos_campbell<-datos_campbell[-indices,]
  
  # Temp Port 4
  indices<-which(datos_campbell[,14]<min | datos_campbell[,14]>max)
  if (length(indices)>0)
    datos_campbell<-datos_campbell[-indices,]
  
  ##################
  
  return(datos_campbell)
  
}


#LOG
log_file <- "LOG.log"

#Archivos necesarios
file_campbell_c7_dt2 = "INPUT/Marismillas_Campbell_C7_DT2.dat"

#Numero de archivos procesados
num_files = 1


# Comprobar que estan todos los archivos que hacen falta
files<-list.files(path = "INPUT",pattern = "\\.(txt|csv|dat|xlsx)$",recursive = TRUE)

errores<-0


tryCatch({
  
  cat("\nCARGANDO SCRIPT...")

  if (!(files[1]==gsub("INPUT/", "", file_campbell_c7_dt2))){

    cat(sprintf(paste(format(Sys.time(), "%A %d %b. %Y, %T "),"[ERROR] Archivo Campbell-Cortafuegos 7_DT2: Falta el archivo '",file_campbell_c7_dt2,"'"," en el directorio INPUT!","\n",sep="")), file = log_file, append = TRUE)
    
    stop("Ver archivo LOG para mas informacion",call. = FALSE)
    
  }else{
    
    
    cat(sprintf(paste(format(Sys.time(), "%A %d %b. %Y, %T "),"[INFO] INICIO DEL PROGRAMA. ","\n",sep="")), file = log_file, append = TRUE)

    ############################################
    ####### CAMPBELL - CORTAFUEGOS_DT2 #########
    ############################################
    
    # FICHERO CAMPBELL-Cortafuegos 7 DT2
    
    nomColumnas = c("TIMESTAMP","RECORD","VWC_PORT1","EC_PORT1","TEMP_PORT1",
                    "VWC_PORT2","EC_PORT2","TEMP_PORT2","VWC_PORT3","EC_PORT3",
                    "TEMP_PORT3","VWC_PORT4","EC_PORT4","TEMP_PORT4","VWC_PORT5",
                    "EC_PORT5","TEMP_PORT5","VWC_PORT6","EC_PORT6","TEMP_PORT6")
    
    cat(sprintf(paste(format(Sys.time(), "%A %d %b. %Y, %T "),"[INFO] CAMPBELL-Cortafuegos 7 DT2: Importando datos... ","\n",sep="")), file = log_file, append = TRUE)
    
    Campbell<-read.table("INPUT/Marismillas_Campbell_C7_DT2.dat",header=FALSE,dec=".",sep=",",skip=4,stringsAsFactors=FALSE,
                         col.names=nomColumnas)
    
    
    # Convertir la primera columna a fecha
    Campbell[,1]<- as.POSIXct(Campbell[,1],format="%Y-%m-%d %H:%M:%S")
    
    cat(sprintf(paste(format(Sys.time(), "%A %d %b. %Y, %T "),"[INFO] CAMPBELL-Cortafuegos 7 DT2: Datos importados al script (",length(Campbell[,1])," filas).\n",sep="")), file = log_file, append = TRUE)
    
    
    ### PROCESAMIENTO FICHERO CAMPBELL-Cortafuegos 7 DT2 ###
    
    # Filas duplicadas
    cat(sprintf(paste(format(Sys.time(), "%A %d %b. %Y, %T "),"[INFO] CAMPBELL-Cortafuegos 7 DT2: Eliminando filas duplicadas...","\n",sep="")), file = log_file, append = TRUE)
    
    if ( length(which(duplicated(Campbell[,1])))>0 ){
      
      duplicated_rows<-which(duplicated(Campbell[,1]))
      
      # Eliminar filas duplicadas
      Campbell<-Campbell[-duplicated_rows,]
      
    }
    
    
    # Conversion de datos de las columnas a numero y limpieza de datos nulos o no validos
    # (Todo lo que no sea numerico, se convierte a 'NA')
    cat(sprintf(paste(format(Sys.time(), "%A %d %b. %Y, %T "),"[INFO] CAMPBELL-Cortafuegos 7 DT2: Conversion de datos y eliminacion de valores no validos...","\n",sep="")), file = log_file, append = TRUE)
    
    Campbell[,3]<-as.numeric(as.character(Campbell[,3]))
    Campbell[,4]<-as.numeric(as.character(Campbell[,4]))
    Campbell[,5]<-as.numeric(as.character(Campbell[,5]))
    Campbell[,6]<-as.numeric(as.character(Campbell[,6]))
    Campbell[,7]<-as.numeric(as.character(Campbell[,7]))
    Campbell[,8]<-as.numeric(as.character(Campbell[,8]))
    Campbell[,9]<-as.numeric(as.character(Campbell[,9]))
    Campbell[,10]<-as.numeric(as.character(Campbell[,10]))
    Campbell[,11]<-as.numeric(as.character(Campbell[,11]))
    Campbell[,12]<-as.numeric(as.character(Campbell[,12]))
    Campbell[,13]<-as.numeric(as.character(Campbell[,13]))
    Campbell[,14]<-as.numeric(as.character(Campbell[,14]))
    Campbell[,15]<-as.numeric(as.character(Campbell[,15]))
    Campbell[,16]<-as.numeric(as.character(Campbell[,16]))
    Campbell[,17]<-as.numeric(as.character(Campbell[,17]))
    Campbell[,18]<-as.numeric(as.character(Campbell[,18]))
    Campbell[,19]<-as.numeric(as.character(Campbell[,19]))
    Campbell[,20]<-as.numeric(as.character(Campbell[,20]))
    
    cat(sprintf(paste(format(Sys.time(), "%A %d %b. %Y, %T "),"[INFO] CAMPBELL-Cortafuegos 7 DT2: Inicio del procesamiento de datos...","\n",sep="")), file = log_file, append = TRUE)
    
    
    # Procesar fichero Campbell-Cortafuegos 7 DT2
    Campbell<-process_campbell(Campbell)
    
    cat(sprintf(paste(format(Sys.time(), "%A %d %b. %Y, %T "),"[INFO] CAMPBELL-Cortafuegos 7 DT2: Procesamiento terminado.","\n",sep="")), file = log_file, append = TRUE)
    
    
    # Escritura fichero Campbell-Cortafuegos 7 DT2
    cat(sprintf(paste(format(Sys.time(), "%A %d %b. %Y, %T "),"[INFO] CAMPBELL-Cortafuegos 7 DT2: Exportando datos...","\n",sep="")), file = log_file, append = TRUE)
    write.table(Campbell, "OUTPUT/EXPORTED_MARISMILLAS_CAMPBELL_C7_DT2.csv", sep=";", row.names=FALSE, dec=".", quote=FALSE)
    
    
    # Campbell-Cortafuegos 7 DT2: visualizacion grafica
    cat(sprintf(paste(format(Sys.time(), "%A %d %b. %Y, %T "),"[INFO] CAMPBELL-Cortafuegos 7 DT2: DATOS EXPORTADOS.","\n",sep="")), file = log_file, append = TRUE)
    
  }
  
  
},
error = function(e){
  errores<-e$message
  cat(sprintf(paste(format(Sys.time(), "%A %d %b. %Y, %T "),"[ERROR] Se ha producido un error interno: ",e$message,".\n",sep="")), file = log_file, append = TRUE)
  stop("Ver archivo LOG para mas informacion",call. = FALSE)
},
finally = {
  
  cat(sprintf(paste(format(Sys.time(), "%A %d %b. %Y, %T "),"[INFO] FIN DEL PROGRAMA.","\n",sep="")), file = log_file, append = TRUE)
  cat("\nTERMINADO. FIN DEL PROGRAMA.\n")
  
}
)