use `Rohigmese`;

revoke all privileges on `Rohigmese`.* from 'acme-user'@'%';

revoke all privileges on `Rohigmese`.* from 'acme-manager'@'%';
